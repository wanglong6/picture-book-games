package com.jzmedia.pbm.activity.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzmedia.pbm.R;
import com.jzmedia.pbm.database.domain.PictureBook;
import com.jzmedia.pbm.vp.ShowcaseContract;

import java.util.ArrayList;
import java.util.List;
import androidx.annotation.RequiresApi;

/**
 * Created by ChristieIn on 2017/7/20.
 * showcase activity books adapter.
 */

public class ShowcaseActBooksAdapter extends RVCommonAdapter<ShowcaseContract.Presenter,
        PictureBook, ShowcaseActBooksAdapter.ViewHolder> {
    private ArrayList<Drawable> mCoverList = null;
    /** 打印标签. */
    private static final String TAG = "ShowcaseAdapter";//makeLogTag("ShowcaseActBooksAdapter");

    /**
     * <p>构造函数</p>
     * @param context context
     * @param data data
     * @param presenter presenter
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ShowcaseActBooksAdapter(Context context, List<PictureBook> data, ShowcaseContract.Presenter presenter) {
        super(context, data, presenter);
        if (mCoverList == null) {
            mCoverList = new ArrayList<>(10);
            mCoverList.add(mContext.getDrawable(R.mipmap.cover_bgzydan));
            mCoverList.add(mContext.getDrawable(R.mipmap.cover_cfl));
            mCoverList.add(mContext.getDrawable(R.mipmap.cover_cyf));
            mCoverList.add(mContext.getDrawable(R.mipmap.cover_dygw));
            mCoverList.add(mContext.getDrawable(R.mipmap.cover_kkbxj));
            mCoverList.add(mContext.getDrawable(R.mipmap.cover_pcxdzj));
            mCoverList.add(mContext.getDrawable(R.mipmap.cover_zpy));
            mCoverList.add(mContext.getDrawable(R.mipmap.cover_zjcr));
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.showcase_act_book_item, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        vh.mIcon = itemView.findViewById(R.id.showcase_act_book_item_icon);
        vh.mName = itemView.findViewById(R.id.showcase_act_book_item_name);
        return vh;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final ViewHolder vh, final int position) {
        final PictureBook book = getItem(position);
        vh.mIcon.setImageDrawable(mCoverList.get(position));
        //vh.mIcon.setImageURI(Uri.parse(book.getVIcon()));
        vh.mName.setText(book.getName());
    }

    /**
     * <p>books viewholder.</p>
     */
    class ViewHolder extends RVCommonAdapter.ViewHolder {
        /** icon imageview. */
        private ImageView mIcon;
        /** name imageview. */
        private TextView mName;

        ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
