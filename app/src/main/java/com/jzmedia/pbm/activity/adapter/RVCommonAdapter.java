package com.jzmedia.pbm.activity.adapter;

import android.content.Context;
import android.graphics.Rect;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import com.jzmedia.pbm.vp.BaseContract;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.jzmedia.common.util.LogUtils.makeLogTag;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Description.
 * Created by pc on 2017/9/21.
 * <p>recycler view common adapter.</p>
 */
public abstract class RVCommonAdapter<P extends BaseContract.Presenter, D,
        VH extends RVCommonAdapter.ViewHolder> extends RecyclerView.Adapter<VH> {
    /** 打印标签. */
    private static final String TAG = makeLogTag("RVCommonAdapter");
    /** 内容数据数据. */
    protected List<D> mData;
    /** Inflater对象,用于创建item. */
    protected LayoutInflater mInflater;
    /** presenter. */
    protected P mPresenter;
    /** 上下文对象. */
    protected Context mContext;
    /** item click listener. */
    private OnItemClickListener mOnItemClickListener;
    /** item long click listener. */
    private OnItemLongClickListener mOnItemLongClickListener;
    /** item selected listener. */
    private OnItemSelectedListener mOnItemSelectedListener;
    /** item key listener. */
    private OnKeyListener mOnKeyListener;

    RVCommonAdapter(@NonNull Context context, List<D> data, @NonNull P presenter) {
        mContext = checkNotNull(context, "context cannot be null");
        mPresenter =  checkNotNull(presenter, "presenter cannot be null");
        mInflater = LayoutInflater.from(context);
        mData = new ArrayList<>(10);
        setAdapterData(data);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    /**
     * <p>获取position的数据对象.</p>
     * @param position position
     * @return 该位置数据对象
     */
    public D getItem(int position) {
        return mData.get(position);
    }

    /**
     * <p>设置新的数据.</p>
     * @param data 新的数据
     */
    public void setAdapterData(List<D> data) {
        mData.clear();
        if (data != null) {
            mData.addAll(data);
        }
    }

    /**
     * <p>获取数据</p>
     * @return 数据
     */
    public List<D> getAdapterData() {
        return mData;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener, View.OnLongClickListener,
            View.OnFocusChangeListener, View.OnKeyListener{

        /*package*/ ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            itemView.setOnFocusChangeListener(this);
            itemView.setOnKeyListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(RVCommonAdapter.this, v, getLayoutPosition());
            }
        }

        @Override
        public boolean onLongClick(View v) {
            return mOnItemLongClickListener != null
                    && mOnItemLongClickListener.onItemLongClick(RVCommonAdapter.this, v, getLayoutPosition());
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (mOnItemSelectedListener != null) {
                if (hasFocus) {
                    mOnItemSelectedListener.onItemSelected(RVCommonAdapter.this, v, getLayoutPosition());
                } else {
                    mOnItemSelectedListener.onItemUnselected(RVCommonAdapter.this, v, getLayoutPosition());
                }
            }
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            return mOnKeyListener != null
                    && mOnKeyListener.onKey(RVCommonAdapter.this, v, getLayoutPosition(), keyCode, event);
        }
    }

    /**
     * <p>recycler view item has been clicked.</p>
     */
    public interface OnItemClickListener {

        /**
         * Callback method to be invoked when an item in this AdapterView has
         * been clicked.
         * @param adapter The adapter
         * @param view The view within the AdapterView that was clicked (this
         *            will be a view provided by the adapter)
         * @param position The position of the view in the adapter.
         */
        void onItemClick(RVCommonAdapter<?, ?, ?> adapter, View view, int position);
    }

    /**
     * Register a callback to be invoked when an item in this AdapterView has
     * been clicked.
     *
     * @param listener The callback that will be invoked.
     */
    public void setOnItemClickListener(@NonNull OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    /**
     * <p>Interface definition for a callback to be invoked when an item in this</p>
     * <p>view has been clicked and held.</p>
     */
    public interface OnItemLongClickListener {
        /**
         * Callback method to be invoked when an item in this view has been
         * clicked and held.
         *
         * Implementers can call getItemAtPosition(position) if they need to access
         * the data associated with the selected item.
         *
         * @param adapter The adapter
         * @param view The view within the AbsListView that was clicked
         * @param position The position of the view in the list
         *
         * @return true if the callback consumed the long click, false otherwise
         */
        boolean onItemLongClick(RVCommonAdapter<?, ?, ?> adapter, View view, int position);
    }


    /**
     * Register a callback to be invoked when an item in this AdapterView has
     * been clicked and held
     *
     * @param listener The callback that will run
     */
    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        mOnItemLongClickListener = listener;
    }

    /**
     * Interface definition for a callback to be invoked when
     * an item in this view has been selected.
     */
    public interface OnItemSelectedListener {
        /**
         * <p>Callback method to be invoked when an item in this view has been
         * selected. This callback is invoked only when the newly selected
         * position is different from the previously selected position or if
         * there was no selected item.</p>
         *
         * Impelmenters can call getItemAtPosition(position) if they need to access the
         * data associated with the selected item.
         *
         * @param adapter The adapter
         * @param view The view within the AdapterView that was clicked
         * @param position The position of the view in the adapter
         */
        void onItemSelected(RVCommonAdapter<?, ?, ?> adapter, View view, int position);

        /**
         * <p>Callback method to be invoked when an item in this view has been unselected.</p>
         * @param adapter The adapter
         * @param view The view within the AdapterView that was clicked
         * @param position The position of the view in the adapter
         */
        void onItemUnselected(RVCommonAdapter<?, ?, ?> adapter, View view, int position);

    }

    /**
     * Register a callback to be invoked when an item in this AdapterView has
     * been selected.
     *
     * @param listener The callback that will run
     */
    public void setOnItemSelectedListener(@NonNull OnItemSelectedListener listener) {
        mOnItemSelectedListener = listener;
    }

    /**
     * Interface definition for a callback to be invoked when a hardware key event is
     * dispatched to this view. The callback will be invoked before the key event is
     * given to the view. This is only useful for hardware keyboards; a software input
     * method has no obligation to trigger this listener.
     */
    public interface OnKeyListener {
        /**
         * @param adapter The adapter
         * @param v The view the key has been dispatched to.
         * @param keyCode The code for the physical key that was pressed
         * @param event The KeyEvent object containing full information about
         *        the event.
         * @return True if the listener has consumed the event, false otherwise.
         */
        boolean onKey(RVCommonAdapter<?, ?, ?> adapter, View v, int position, int keyCode, KeyEvent event);
    }

    public void setOnKeyListener(@NonNull OnKeyListener listener) {
        mOnKeyListener = listener;
    }

    /**
     * <p>space item decoration.</p>
     */
    public static class SpaceItemDecoration extends RecyclerView.ItemDecoration {
        /** left,right,bottom,top. */
        private int mLeft, mRight, mBottom, mTop;

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.left = mLeft;
            outRect.right = mRight;
            outRect.bottom = mBottom;
            outRect.top = mTop;
        }

        /**
         * @param l left
         * @param r right
         * @param t top
         * @param b bottom
         */
        public SpaceItemDecoration(int l, int r, int t, int b) {
            mLeft = l;
            mRight = r;
            mTop = t;
            mBottom = b;
        }
    }
}
