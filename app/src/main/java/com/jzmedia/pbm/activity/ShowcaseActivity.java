package com.jzmedia.pbm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.jzmedia.common.util.LogUtils;
import com.jzmedia.pbm.AppApplication;
import com.jzmedia.pbm.R;
import com.jzmedia.pbm.activity.adapter.RVCommonAdapter;
import com.jzmedia.pbm.activity.adapter.ShowcaseActBooksAdapter;
import com.jzmedia.pbm.activity.adapter.ShowcaseActTypesAdapter;
import com.jzmedia.pbm.contract.PBMContract;
import com.jzmedia.pbm.database.domain.BookType;
import com.jzmedia.pbm.database.domain.PictureBook;
import com.jzmedia.pbm.model.behavior.BehaviorAction;
import com.jzmedia.pbm.util.DensityUtils;
import com.jzmedia.pbm.vp.ShowcaseContract;
import com.jzmedia.pbm.vp.ShowcasePresenter;
import java.util.List;
import static com.jzmedia.common.util.LogUtils.LOGD;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

/**
 * Created by ChristieIn on 2018/3/16.
 * 绘本、动画、电子书展示界面。
 */
public class ShowcaseActivity extends BaseActivity<ShowcaseContract.Presenter> implements ShowcaseContract.View {
    /** 打印标签. */
    private static final String TAG = "ShowcaseActivity";//makeLogTag("ShowcaseActivity");
    /** 每页显示书本个数. */
    private static final int ITEMS_COUNT_OF_PAGE = 5;
    /** 绘本展示列表,类型展示列表. */
    private RecyclerView mBooksRv, mTypesRv;
    /** books adapter. */
    private ShowcaseActBooksAdapter mBooksAdapter;
    /** types adapter. */
    private ShowcaseActTypesAdapter mTypesAdapter;
    /** 按钮. */
    private Button mPreBtn, mNextBtn, mBackBtn;
    /** title. */
    private ImageView mTitleImgv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showcase);

        //适配分辨率
        View root = findViewById(R.id.showcase_act_root);
        DensityUtils.adjustScreen(this, root);

        mTitleImgv = findViewById(R.id.showcase_act_title_imgv);

        mBackBtn = findViewById(R.id.showcase_act_back_btn);
        mBackBtn.setOnClickListener(mBtnClickListener);

        //book
        LinearLayoutManager blm = new LinearLayoutManager(this);
        blm.setOrientation(LinearLayoutManager.HORIZONTAL);
        mBooksRv = findViewById(R.id.showcase_act_books_rv);
        mBooksRv.setLayoutManager(new StaggeredGridLayoutManager(4,StaggeredGridLayoutManager.VERTICAL));//可以表述一行显示item的数量，并且可以设置列表的方向
        //mBooksRv.setLayoutManager(blm);
        mBooksRv.setItemAnimator(null);
        mBooksRv.addItemDecoration(new RVCommonAdapter.SpaceItemDecoration(0, 0, 0, 0));
        mBooksAdapter = new ShowcaseActBooksAdapter(this, null, mPresenter);
        mBooksAdapter.setOnItemClickListener(mItemClickListener);
        mBooksRv.setAdapter(mBooksAdapter);
        mBooksRv.addOnScrollListener(mScrollListener);


        LinearLayoutManager tlm = new LinearLayoutManager(this);
        tlm.setOrientation(LinearLayoutManager.HORIZONTAL);
        mTypesRv = findViewById(R.id.showcase_act_types_rv);
        mTypesRv.setLayoutManager(tlm);
        mTypesRv.setItemAnimator(null);
        mTypesRv.addItemDecoration(new RVCommonAdapter.SpaceItemDecoration(0, 0, 0, 0));
        mTypesAdapter = new ShowcaseActTypesAdapter(this, null, mPresenter);
        mTypesAdapter.setOnItemClickListener(mItemClickListener);
        mTypesRv.setAdapter(mTypesAdapter);

        mPreBtn = findViewById(R.id.showcase_act_pre_btn);
        mNextBtn = findViewById(R.id.showcase_act_next_btn);

        mPreBtn.setOnClickListener(mBtnClickListener);
        mNextBtn.setOnClickListener(mBtnClickListener);

        //通过启动参数打开log
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(PBMContract.LOGGABLE_EXTRA_KEY)
                && intent.getBooleanExtra(PBMContract.LOGGABLE_EXTRA_KEY, false)) {
            LogUtils.enable();
        } else {
            LogUtils.disable();
        }

        //Intent serviceIntent = new Intent(this, UpgradeService.class);
        //startService(serviceIntent);
        mPresenter.reportAppAction(BehaviorAction.enter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.reportAppAction(BehaviorAction.quit);
        //Intent serviceIntent = new Intent(this, UpgradeService.class);
        //stopService(serviceIntent);

        AppApplication application = (AppApplication) getApplication();
        application.release();
    }

    @Override
    protected ShowcaseContract.Presenter initializePresenter() {
        return new ShowcasePresenter(this, this);
    }

    @Override
    public void showMainType(BookType type) {
        switch (type.getTypeId()) {
            case PBMContract.TYPE_PICTUREBOOK:
                mTitleImgv.setImageResource(R.mipmap.showcase_act_hbjd);
                break;
            case PBMContract.TYPE_EBOOK:
                mTitleImgv.setImageResource(R.mipmap.showcase_act_dzts);
                break;
            case PBMContract.TYPE_CARTOON:
                mTitleImgv.setImageResource(R.mipmap.showcase_act_hbdh);
                break;
            default:
                break;
        }
    }

    @Override
    public void showSubTypes(List<BookType> types) {
        mTypesAdapter.setAdapterData(types);
        mTypesAdapter.notifyDataSetChanged();
        mTypesRv.smoothScrollToPosition(0);
        if (types.size() > 0) {
            mTypesAdapter.setSelectedPosition(0);
            mPresenter.openSubType(types.get(0));
        }
    }

    @Override
    public void showBooks(BookType type, List<PictureBook> books) {
        mBooksAdapter.setAdapterData(books);
        mBooksAdapter.notifyDataSetChanged();
        mBooksRv.smoothScrollToPosition(0);
        mPreBtn.setSelected(false);
        if (mBooksAdapter.getItemCount() > ITEMS_COUNT_OF_PAGE) {
            mNextBtn.setSelected(true);
        } else {
            mNextBtn.setSelected(false);
        }
        for (PictureBook tmp : books) {
            Log.d(TAG, "-> " + tmp.toString());
        }
    }

    /**
     * <p>book or type item点击事件监听接口</p>
     */
    private RVCommonAdapter.OnItemClickListener mItemClickListener = new RVCommonAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(RVCommonAdapter<?, ?, ?> adapter, View view, int position) {
            LOGD(TAG, "onItemClick() called with: adapter = [" + adapter + "], view = [" + view + "], position = [" + position + "]");
            if (adapter == mBooksAdapter) {
                mPresenter.openBook(mBooksAdapter.getItem(position));
            } else if (adapter == mTypesAdapter) {
                if (position != mTypesAdapter.getSelectedPosition()) {
                    mTypesAdapter.setSelectedPosition(position);
                    mTypesAdapter.notifyDataSetChanged();
                    mPresenter.openSubType(mTypesAdapter.getItem(position));
                }
            }
        }
    };

    /**
     * <p>滚动事件</p>
     */
    private RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (!recyclerView.canScrollHorizontally(1)){
                mNextBtn.setSelected(false);
            } else {
                mNextBtn.setSelected(true);
            }
            if (!recyclerView.canScrollHorizontally(-1)) {
                mPreBtn.setSelected(false);
            } else {
                mPreBtn.setSelected(true);
            }
        }
    };

    /**
     * <p>按钮点击事件</p>
     */
    private View.OnClickListener mBtnClickListener = v -> {
        switch (v.getId()) {
            case R.id.showcase_act_pre_btn:
                scrollDirection(-1);
                break;
            case R.id.showcase_act_next_btn:
                scrollDirection(1);
                break;
            case R.id.showcase_act_back_btn:
                exitSelf();
                break;
            default:
                break;
        }
    };

    /**
     * <p>翻页</p>
     * @param direction 小于0时前一页,大于0时下一页,等于0时不翻页
     */
    private void scrollDirection(int direction) {
        if (direction == 0) {
            return;
        }
        LinearLayoutManager llm = (LinearLayoutManager) mBooksRv.getLayoutManager();
        int position;
        if (direction < 0) {
            int fvp = llm.findFirstCompletelyVisibleItemPosition();
            position = fvp - ITEMS_COUNT_OF_PAGE;
            if (position < 0) {
                position = 0;
            }
        } else {
            int lvp = llm.findLastCompletelyVisibleItemPosition();
            position = lvp + ITEMS_COUNT_OF_PAGE;
            if (position > mBooksAdapter.getItemCount() - 1) {
                position = mBooksAdapter.getItemCount() - 1;
            }
        }
        mBooksRv.smoothScrollToPosition(position);
    }

}
