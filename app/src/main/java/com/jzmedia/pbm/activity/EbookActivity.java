package com.jzmedia.pbm.activity;

import android.os.Bundle;
//import android.support.annotation.Nullable;

import com.jzmedia.pbm.contract.PBMContract;
import com.jzmedia.pbm.database.domain.BookType;

import static com.jzmedia.common.util.LogUtils.makeLogTag;

import androidx.annotation.Nullable;

/**
 * Created by ChristieIn on 2018/3/21.
 * 电子图书列表展示界面.
 */
public class EbookActivity extends ShowcaseActivity {
    /** 打印标签. */
    private static final String TAG = makeLogTag("EbookActivity");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.initWithTypeId(PBMContract.TYPE_EBOOK);
    }
}
