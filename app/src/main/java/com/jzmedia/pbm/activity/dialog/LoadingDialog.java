package com.jzmedia.pbm.activity.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
//import android.support.annotation.NonNull;

import androidx.annotation.NonNull;

import com.jzmedia.pbm.R;


/**
 * Created by ChristieIn on 2018/1/22.
 * loading dialog.
 */
public class LoadingDialog extends AlertDialog {

    /**
     * <p>loading dialog.</p>
     * @param context context
     */
    public LoadingDialog(@NonNull Context context) {
        super(context, R.style.AppDialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_loading);
    }
}
