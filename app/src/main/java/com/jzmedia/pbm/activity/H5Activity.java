package com.jzmedia.pbm.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.jzmedia.pbm.R;
import com.jzmedia.pbm.database.domain.BookModule;
import com.jzmedia.pbm.database.domain.PictureBook;
import com.jzmedia.pbm.model.behavior.BehaviorAction;
import com.jzmedia.pbm.vp.H5Contract;
import com.jzmedia.pbm.vp.H5Presenter;

import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.makeLogTag;

public class H5Activity extends BaseActivity<H5Contract.Presenter> implements H5Contract.View {
    /** 打印标签. */
    private static final String TAG = "H5Activity";//makeLogTag("H5Activity");
    /** screen width, screen height. */
    private int screenWidth, screenHeight;
    /** 是否正在拖动. */
    private boolean isMoving;
    /** webview. */
    private WebView mWebView;
    /** back btn. */
    private Button mBackBtn;
    /** book. */
    private PictureBook mCurrentBook;
    /** module. */
    private BookModule mCurrentModule;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h5);
        LOGD(TAG, "onCreate.");

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        if (wm != null) {
            wm.getDefaultDisplay().getMetrics(outMetrics);
            screenWidth = outMetrics.widthPixels;
            screenHeight = outMetrics.heightPixels;
        } else {
            screenWidth = 1024;
            screenHeight = 600;
        }

        //初始化webview
        initWebView();

        //back btn
        mBackBtn = findViewById(R.id.h5_act_back_btn);
        mBackBtn.setOnClickListener(mBtnClickListener);
        mBackBtn.setOnLongClickListener(mBtnLongClickListener);
        mBackBtn.setOnTouchListener(mBtnTouchListener);

        mCurrentBook = (PictureBook) getIntent().getSerializableExtra(EXTRA_BOOK_KEY);
        mCurrentModule = (BookModule) getIntent().getSerializableExtra(EXTRA_MODULE_KEY);
        loadUrl(mWebView,mCurrentModule.getUrl());

        mPresenter.reportPageEnter(mCurrentBook, mCurrentModule);
        mPresenter.reportContentAction(BehaviorAction.enter, mCurrentBook, mCurrentModule);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWebView.destroy();
        mPresenter.reportContentAction(BehaviorAction.quit, mCurrentBook, mCurrentModule);
    }

    @Override
    protected H5Contract.Presenter initializePresenter() {
        return new H5Presenter(this, this);
    }

    /**
     * <p>初始化WebView.</p>
     */
    private void initWebView() {
        mWebView = (WebView) findViewById(R.id.h5_act_webview);
        mWebView.clearCache(true);
        mWebView.clearHistory();
        WebSettings settings = mWebView.getSettings();
        settings.setSaveFormData(false);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setSupportZoom(true);
        settings.setNeedInitialFocus(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setDefaultTextEncodingName("GBK");
        settings.setMediaPlaybackRequiresUserGesture(false);
        settings.setDomStorageEnabled(true);

        settings.setAllowFileAccess(true);// 设置允许访问文件数据
        mWebView.getSettings().setAllowFileAccessFromFileURLs(true);



        //设置缓存
        mWebView.getSettings().setAppCacheEnabled(false);
        // 设置支持本地存储
        mWebView.getSettings().setDatabaseEnabled(false);
        //取得缓存路径
        String path = getApplicationContext().getDir("cache", Context.MODE_PRIVATE).getPath();
        //设置路径
        mWebView.getSettings().setDatabasePath(path);
        mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        mWebView.getSettings().setAllowContentAccess(true);

        mWebView.setWebViewClient(new WebViewClientVod());

        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebView.addJavascriptInterface(mJsObj, "ebookObj");
    }

    /**
     * 加载网页.
     * @param url 网页地址
     */
    private void loadUrl(WebView mWebView,String url) {
        if (url == null) return;
        Log.d(TAG, "url-> " + url);
        if (url.equals("file:///android_asset/hbyx/cfl/index.html") || url.equals("file:///android_asset/hbyx/cyf/index.html")){
//            Toast.makeText(H5Activity.this, "进来了啊", Toast.LENGTH_SHORT).show();
            mWebView.setInitialScale(25);
            int screenDensity = 160;
//        Logger.d(TAG, "screenDensity = " + screenDensity);
            WebSettings.ZoomDensity zoomDensity = WebSettings.ZoomDensity.MEDIUM;
            switch (screenDensity){
                case DisplayMetrics.DENSITY_HIGH://240dpi
                case DisplayMetrics.DENSITY_XHIGH://320dpi
                case DisplayMetrics.DENSITY_XXHIGH://480dpi
                default:
                    zoomDensity = WebSettings.ZoomDensity.FAR;
                    break;
            }
            mWebView.getSettings().setDefaultZoom(zoomDensity);
            mWebView.getSettings().setUseWideViewPort(true);
            mWebView.getSettings().setTextZoom(100);
            mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        }else {

        }
        if (TextUtils.equals(url, mWebView.getUrl())) {
            mWebView.reload();
        } else {
            mWebView.loadUrl(url);
        }
    }

    /**
     * <p>webview client.</p>
     */
    private class WebViewClientVod extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            LOGD(TAG, "request = " + request);
            super.shouldOverrideUrlLoading(view, request);
            return true;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }
    }

    /**
     * js调Java的接口
     * */
    private Object mJsObj = new Object() {

        @JavascriptInterface
        public void jsHandlerBack(String value) {
            LOGD(TAG, "jsHandlerBack, value = " + value);
        }

        @JavascriptInterface
        public void jsGoHomePage(final String code) {
            LOGD(TAG, "jsGoHomePage...code = " + code);
            exitSelf();
        }

        @JavascriptInterface
        public void closeLoadingAni() {
            LOGD(TAG, "closeLoadingAni()");
        }

        @JavascriptInterface
        public String decryptData(String data, int key) {
            if (!TextUtils.isEmpty(data)) {
                byte[] dataA = Base64.decode(data, Base64.DEFAULT);
                for (int i=0; i < dataA.length; i++) {
                    dataA[i] -= key;
                }
                byte[] dataB = Base64.decode(dataA, Base64.DEFAULT);
                String dataC = new String(dataB);
                return dataC;
            }
            return "";
        }

    };

    /**
     * <p>模块按钮点击事件监听器</p>
     */
    private View.OnClickListener mBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LOGD("TAG", "onClick");
            LOGD(TAG, "onClick() called with: v = [" + v + "]");
            switch (v.getId()) {
                case R.id.h5_act_back_btn:
                    exitSelf();
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * <p>长按事件.</p>
     */
    private View.OnLongClickListener mBtnLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            LOGD("TAG", "onLongClick");
            isMoving = true;
            return true;
        }
    };

    /**
     * <p>拖拽</p>
     */
    private View.OnTouchListener mBtnTouchListener = new View.OnTouchListener() {
        private int lastX, lastY;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int ea = event.getAction();
            switch (ea) {
                case MotionEvent.ACTION_DOWN:
                    isMoving = false;
                    lastX = (int) event.getRawX();// 获取触摸事件触摸位置的原始X坐标
                    lastY = (int) event.getRawY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (isMoving) {
                        int dx = (int) event.getRawX() - lastX;
                        int dy = (int) event.getRawY() - lastY;
                        int l = v.getLeft() + dx;
                        int b = v.getBottom() + dy;
                        int r = v.getRight() + dx;
                        int t = v.getTop() + dy;
                        // 下面判断移动是否超出屏幕
                        if (l < 0) {
                            l = 0;
                            r = l + v.getWidth();
                        }
                        if (t < 0) {
                            t = 0;
                            b = t + v.getHeight();
                        }
                        if (r > screenWidth) {
                            r = screenWidth;
                            l = r - v.getWidth();
                        }
                        if (b > screenHeight) {
                            b = screenHeight;
                            t = b - v.getHeight();
                        }
                        v.layout(l, t, r, b);
                        lastX = (int) event.getRawX();
                        lastY = (int) event.getRawY();
                        v.postInvalidate();
                        return true;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    isMoving = false;
                    break;
            }
            return false;
        }
    };

}
