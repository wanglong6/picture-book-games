package com.jzmedia.pbm.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
//import android.support.annotation.Nullable;
import android.widget.Toast;

import com.gyf.immersionbar.BarHide;
import com.gyf.immersionbar.ImmersionBar;
import com.jzmedia.pbm.R;
import com.jzmedia.pbm.activity.dialog.LoadingDialog;
import com.jzmedia.pbm.database.domain.BookModule;
import com.jzmedia.pbm.database.domain.PictureBook;
import com.jzmedia.pbm.vp.BaseContract;

import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.makeLogTag;

import androidx.annotation.Nullable;

/**
 * Created by ChristieIn on 2018/1/22.
 * base activity for preparation lesson client.
 */

public abstract class BaseActivity<P extends BaseContract.Presenter> extends Activity implements BaseContract.View<P> {
    /** 打印标签. */
    private static final String TAG = makeLogTag("BaseActivity");
    /** extra book key. */
    protected static final String EXTRA_BOOK_KEY = "book";
    /** extra module key. */
    protected static final String EXTRA_MODULE_KEY = "module";

    /** presenter. */
    protected P mPresenter;
    /** loading dialog. */
    private Dialog mLoadingDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setPresenter(initializePresenter());
        initStatusBar();
    }



    /**
     * 状态栏 和 导航栏 设置
     */
    private void initStatusBar(){
        ImmersionBar.with(this)
                .fitsSystemWindows(false)
                .hideBar(BarHide.FLAG_HIDE_NAVIGATION_BAR)
                .init();
    }
    /**
     * <p>初始化presenter.</p>
     */
    protected abstract P initializePresenter();

    @Override
    public void setPresenter(P presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            if (mLoadingDialog == null) {
                mLoadingDialog = new LoadingDialog(this);
                mLoadingDialog.show();
                mLoadingDialog.setCanceledOnTouchOutside(false);
                mLoadingDialog.setCancelable(false);
            } else {
                mLoadingDialog.show();
            }
        } else {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
        }
    }

    @Override
    public void showModuleActivity(PictureBook book) {
        /*Intent intent = new Intent(this, ModuleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_BOOK_KEY, book);
        startActivity(intent);*/
    }

    @Override
    public void showH5Activity(PictureBook book, BookModule module) {
        Intent intent = new Intent(this, H5Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_BOOK_KEY, book);
        intent.putExtra(EXTRA_MODULE_KEY, module);
        startActivity(intent);
    }

    @Override
    public void showVideoActivity(PictureBook book, BookModule module) {
        /*Intent intent = new Intent(this, VideoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_BOOK_KEY, book);
        intent.putExtra(EXTRA_MODULE_KEY, module);
        startActivity(intent);*/
    }

    @Override
    public void showNoModule() {
        Toast.makeText(this, R.string.base_act_no_module, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void exitSelf() {
        finish();
    }
}
