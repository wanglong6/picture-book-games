package com.jzmedia.pbm.activity.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jzmedia.pbm.R;
import com.jzmedia.pbm.contract.PBMContract;
import com.jzmedia.pbm.database.domain.BookType;
import com.jzmedia.pbm.vp.ShowcaseContract;

import java.util.List;

import static com.jzmedia.common.util.LogUtils.makeLogTag;

/**
 * Created by ChristieIn on 2017/7/20.
 * showcase activity books adapter.
 */

public class ShowcaseActTypesAdapter extends RVCommonAdapter<ShowcaseContract.Presenter,
        BookType, ShowcaseActTypesAdapter.ViewHolder> {
    /** 打印标签. */
    private static final String TAG = makeLogTag("ShowcaseActBooksAdapter");
    /** 选中的位置. */
    private int mSelectedPosition = -1;

    /**
     * <p>构造函数</p>
     * @param context context
     * @param data data
     * @param presenter presenter
     */
    public ShowcaseActTypesAdapter(Context context, List<BookType> data, ShowcaseContract.Presenter presenter) {
        super(context, data, presenter);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.showcase_act_type_item, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        vh.mName = itemView.findViewById(R.id.showcase_act_type_item_name);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder vh, final int position) {
        BookType type = getItem(position);
        switch (type.getTypeId()) {
            case PBMContract.TYPE_PICTUREBOOK:
            case PBMContract.TYPE_EBOOK:
            case PBMContract.TYPE_CARTOON:
                vh.mName.setImageResource(R.drawable.showcase_act_type_all_selector);
                break;
            case PBMContract.TYPE_PICTUREBOOK_QGBD:
            case PBMContract.TYPE_EBOOK_QGBD:
            case PBMContract.TYPE_CARTOON_QGBD:
                vh.mName.setImageResource(R.drawable.showcase_act_type_qgbd_selector);
                break;
            case PBMContract.TYPE_PICTUREBOOK_XGYC:
                vh.mName.setImageResource(R.drawable.showcase_act_type_xgyc_selector);
                break;
            case PBMContract.TYPE_EBOOK_SHRZ:
            case PBMContract.TYPE_CARTOON_SHRZ:
                vh.mName.setImageResource(R.drawable.showcase_act_type_shrz_selector);
                break;
            case PBMContract.TYPE_PICTUREBOOK_XGPY:
                vh.mName.setImageResource(R.drawable.showcase_act_type_xgpy_selector);
                break;
            case PBMContract.TYPE_EBOOK_KXFX:
            case PBMContract.TYPE_CARTOON_KXFX:
                vh.mName.setImageResource(R.drawable.showcase_act_type_kxfx_selector);
                break;
            case PBMContract.TYPE_PICTUREBOOK_SHJW:
            case PBMContract.TYPE_EBOOK_SHJW:
            case PBMContract.TYPE_CARTOON_SHJW:
                vh.mName.setImageResource(R.drawable.showcase_act_type_shjw_selector);
                break;
            default:
                break;

        }
        if (position == mSelectedPosition) {
            vh.mName.setSelected(true);
        } else {
            vh.mName.setSelected(false);
        }
    }

    /**
     * <p>获取选中位置.</p>
     * @return selected position
     */
    public int getSelectedPosition() {
        return mSelectedPosition;
    }

    /**
     * <p>设置选中位置.</p>
     * @param position selected position
     */
    public void setSelectedPosition(int position) {
        mSelectedPosition = position;
    }

    /**
     * <p>types viewholder.</p>
     */
    class ViewHolder extends RVCommonAdapter.ViewHolder {
        /** name. */
        private ImageView mName;

        ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
