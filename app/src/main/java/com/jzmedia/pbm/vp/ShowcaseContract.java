package com.jzmedia.pbm.vp;

import com.jzmedia.pbm.database.domain.BookType;
import com.jzmedia.pbm.database.domain.PictureBook;

import java.util.List;

/**
 * Created by Leo on 2018/3/9.
 * 绘本列表展示的view和presenter接口定义。
 */
public interface ShowcaseContract {

    /**
     * <p>showcase view.</p>
     */
    interface View extends BaseContract.View<Presenter> {

        /**
         * <p>显示首要分类.</p>
         * @param type type
         */
        void showMainType(BookType type);

        /**
         * <p>显示类型</p>
         * @param types types
         */
        void showSubTypes(List<BookType> types);

        /**
         * <p>显示绘本列表</p>
         * @param type type
         * @param books books
         */
        void showBooks(BookType type, List<PictureBook> books);

    }

    /**
     * <p>showcase presenter.</p>
     */
    interface Presenter extends BaseContract.Presenter {

        /**
         * <p>初始化类型</p>
         * @param typeId type id
         */
        void initWithTypeId(int typeId);

        /**
         * <p>打开二级类型</p>
         * @param type 二级类型
         */
        void openSubType(BookType type);

        /**
         * <p>打开book的详情界面,如果book只有一个module则直接打开其module的播放界面.</p>
         * @param book 打开book的详情页面
         */
        void openBook(PictureBook book);

        /**
         * <p>通过typeid查询type信息.</p>
         * @param typeId type id
         * @return type
         */
        BookType queryTypeById(int typeId);
    }

}
