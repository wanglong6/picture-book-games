package com.jzmedia.pbm.vp;

import com.jzmedia.common.loader.ImageLoadCallback;
import com.jzmedia.pbm.database.domain.BookModule;
import com.jzmedia.pbm.database.domain.BookType;
import com.jzmedia.pbm.database.domain.PictureBook;
import com.jzmedia.pbm.model.behavior.BehaviorAction;

/**
 * Created by ChristieIn on 2018/1/22.
 * base view interface and presenter interface.
 */
public interface BaseContract {

    /**
     * <p>base view,通过分析各个view的功能,抽取的base view.</p>
     */
    interface View<P extends Presenter> {
        /**
         * 设置presenter.
         * @param presenter mvp的p模块
         */
        void setPresenter(P presenter);

        /**
         * <p>设置显示/隐藏loading.</p>
         * @param active 设置的loading状态
         */
        void setLoadingIndicator(boolean active);

        /**
         * <p>显示module列表界面.</p>
         * @param book 打开的课本
         */
        void showModuleActivity(PictureBook book);

        /**
         * <p>显示h5页面.</p>
         * @param book 打开的课本
         * @param module 打开的模块
         */
        void showH5Activity(PictureBook book, BookModule module);

        /**
         * <p>显示video界面.</p>
         * @param book 打开的课本
         * @param module 打开的模块
         */
        void showVideoActivity(PictureBook book, BookModule module);

        /**
         * <p>显示没有模块</p>
         */
        void showNoModule();

        /**
         * <p>退出当前界面.</p>
         */
        void exitSelf();
    }

    /**
     * <p>base presenter,通过分析各个view对应的presenter抽取的base presnter.</p>
     */
    interface Presenter {

        /**
         * <p>加载content图片.</p>
         * @param imgUrl url
         * @param view the view to load image
         * @param callback call back
         */
        <T extends android.view.View> void loadContentImage(String imgUrl, T view, ImageLoadCallback<T> callback);

        /**
         * <p>app打开、关闭事件</p>
         * @param action action
         */
        void reportAppAction(BehaviorAction action);

        /**
         * <p>上报page action.为展示分类展示绘本的界面</p>
         * @param type type
         */
        void reportPageEnter(BookType type);

        /**
         * <p>上报page action.绘本模块的展示界面</p>
         * @param book book
         */
        void reportPageEnter(PictureBook book);

        /**
         * <p>上报page action.为展示模块的界面,h5播放界面或视频播放界面.</p>
         * @param book book
         */
        void reportPageEnter(PictureBook book, BookModule module);

        /**
         * <p>上报</p>
         * @param book book
         * @param module module
         */
        void reportContentAction(BehaviorAction action, PictureBook book, BookModule module);

        /**
         * <p>上报</p>
         * @param book book
         * @param module module
         * @param rate rate
         */
        void reportContentDuring(PictureBook book, BookModule module, float rate);

    }

}
