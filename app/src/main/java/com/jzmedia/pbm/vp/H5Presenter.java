package com.jzmedia.pbm.vp;

import android.app.Activity;

import static com.jzmedia.common.util.LogUtils.makeLogTag;

import androidx.annotation.NonNull;

/**
 * Created by Leo on 2018/3/9.
 * h5 presenter.
 */

public class H5Presenter extends BasePresenter<H5Contract.View> implements H5Contract.Presenter {
    /** 打印标签. */
    private static final String TAG = makeLogTag("H5Presenter");

    /**
     * <p>构造函数.</p>
     * @param activity activity
     * @param view the view of mvp
     */
    public H5Presenter(@NonNull Activity activity, @NonNull H5Contract.View view) {
        super(activity, view);
    }

}
