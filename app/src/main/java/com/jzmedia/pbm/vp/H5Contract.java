package com.jzmedia.pbm.vp;

/**
 * Created by Leo on 2018/3/9.
 * 绘本列表展的view和presenter接口定义。
 */
public interface H5Contract {

    /**
     * <p>showcase view.</p>
     */
    interface View extends BaseContract.View<Presenter> {
    }

    /**
     * <p>showcase presenter.</p>
     */
    interface Presenter extends BaseContract.Presenter {

    }

}
