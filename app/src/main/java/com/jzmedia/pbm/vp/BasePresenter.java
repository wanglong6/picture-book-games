package com.jzmedia.pbm.vp;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import com.jzmedia.common.loader.DataLoader;
import com.jzmedia.common.loader.ImageLoadCallback;
import com.jzmedia.pbm.AppApplication;
import com.jzmedia.pbm.AppConfig;
import com.jzmedia.pbm.contract.PBMContract;
import com.jzmedia.pbm.database.PictureBooksContract.*;
import com.jzmedia.pbm.database.PictureBooksDatabase.*;
import com.jzmedia.pbm.database.domain.BookModule;
import com.jzmedia.pbm.database.domain.BookType;
import com.jzmedia.pbm.database.domain.PictureBook;
import com.jzmedia.pbm.model.behavior.BehaviorAction;
import com.jzmedia.pbm.model.behavior.BehaviorType;
import com.jzmedia.pbm.model.web.WebClient;
import com.jzmedia.pbm.model.web.domain.CollectionsPostBean;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.makeLogTag;

import androidx.annotation.NonNull;

/**
 * Created by Leo on 2018/1/22.
 * base presenter抽象类
 */

/*package*/ class BasePresenter<V extends BaseContract.View> implements BaseContract.Presenter {
    /** 打印标签. */
    private static final String TAG = makeLogTag("BasePresenter");
    /** context. */
    private Context mContext;
    /** mvp模块的v模块. */
    /*package*/ V mView;
    /** app config. */
    /*package*/ AppConfig mConfig;
    /** 数据加载器. */
    /*package*/ DataLoader mDataLoader;
    /** 用户数据交互接口. */
    /*package*/ WebClient mWebClient;
    /** 绘本资源数据库open helper. */
    /*package*/ SQLiteOpenHelper mPbOpenHelper;
    /** 用于js调用java代码时，在主线程中执行UI相关的操作. */
    /*package*/ Handler mHandler;

    /**
     * <p>构造函数.</p>
     * @param activity activity
     * @param view mvp的view模块
     */
    /*package*/ BasePresenter(@NonNull Activity activity, @NonNull V view) {
        mContext = checkNotNull(activity, "activity cannot be null");
        mView = checkNotNull(view, "view cannot be null");
        AppApplication application = (AppApplication) activity.getApplication();

        mConfig = application.getAppConfig();
        mDataLoader = application.getDataLoader();
        mWebClient = application.getWebClient();
        mPbOpenHelper = application.getPictureBookOpenHelper();

        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public <T extends View> void loadContentImage(String imgUrl, T view, ImageLoadCallback<T> callback) {
        mDataLoader.loadContentImage(imgUrl, view, callback);
    }


    @Override
    public void reportAppAction(BehaviorAction action) {
        /*CollectionsPostBean behavior = new CollectionsPostBean();
        behavior.setType(BehaviorType.APP.name());
        behavior.setAction(action.name());
        CollectionsPostBean.ObjectBean object = new CollectionsPostBean.ObjectBean();
        behavior.setObject(object);
        //object
        PackageInfo info = PackageUtils.getOwnPackageInfo(mContext);
        if (info != null) {
            object.setId(info.packageName);
            object.setName(PackageUtils.getOwnApplicationLabele(mContext));
            object.setData1(String.valueOf(info.versionCode));
            object.setData2(info.versionName);
        }
        //report
        mWebClient.reportBehavior(behavior);*/
    }

    @Override
    public void reportPageEnter(BookType type) {
        if (type != null) {
            CollectionsPostBean behavior = new CollectionsPostBean();
            behavior.setType(BehaviorType.PAGE.name());
            behavior.setAction(BehaviorAction.enter.name());
            CollectionsPostBean.ObjectBean object = new CollectionsPostBean.ObjectBean();
            behavior.setObject(object);
            object.setId(String.valueOf(type.getTypeId()));
            object.setName(String.valueOf(type.getName()));
            object.setData1("绘本展示列表");
            mWebClient.reportBehavior(behavior);
        }
    }

    @Override
    public void reportPageEnter(PictureBook book) {
        if (book != null) {
            CollectionsPostBean behavior = new CollectionsPostBean();
            behavior.setType(BehaviorType.PAGE.name());
            behavior.setAction(BehaviorAction.enter.name());
            CollectionsPostBean.ObjectBean object = new CollectionsPostBean.ObjectBean();
            behavior.setObject(object);
            object.setId(String.valueOf(book.getBookId()));
            object.setName(String.valueOf(book.getName()));
            object.setData1("模块展示列表");
            mWebClient.reportBehavior(behavior);
        }
    }

    @Override
    public void reportPageEnter(PictureBook book, BookModule module) {
        if (book != null && module != null) {
            CollectionsPostBean behavior = new CollectionsPostBean();
            behavior.setType(BehaviorType.PAGE.name());
            behavior.setAction(BehaviorAction.enter.name());
            CollectionsPostBean.ObjectBean object = new CollectionsPostBean.ObjectBean();
            behavior.setObject(object);
            object.setId(String.valueOf(book.getBookId()));
            object.setName(String.valueOf(book.getName()));
            object.setData1("播放页");
            object.setData2("模块名:" + module.getName());
            object.setData3("模块号:" + module.getSeq());
            mWebClient.reportBehavior(behavior);
        }
    }

    @Override
    public void reportContentAction(BehaviorAction action, PictureBook book, BookModule module) {
        if (book != null && module != null) {
            CollectionsPostBean behavior = new CollectionsPostBean();
            behavior.setType(BehaviorType.CONTENT.name());
            behavior.setAction(action.name());
            CollectionsPostBean.ObjectBean object = new CollectionsPostBean.ObjectBean();
            behavior.setObject(object);
            object.setId(String.valueOf(book.getBookId()));
            object.setName(String.valueOf(book.getName()));
            object.setCpid(String.valueOf(book.getCpId()));
            object.setTypeid(String.valueOf(book.getTypeId()));
            object.setData1("模块名:" + module.getName());
            object.setData2("模块号:" + module.getSeq());
            mWebClient.reportBehavior(behavior);
        }
    }

    @Override
    public void reportContentDuring(PictureBook book, BookModule module, float rate) {
        if (book != null && module != null) {
            NumberFormat percentFormat = NumberFormat.getPercentInstance();
            percentFormat.setMaximumFractionDigits(0);//最大小数位数
            percentFormat.setMinimumFractionDigits(0);//最小小数位数
            String during = percentFormat.format(rate);//自动转换成百分比显示..
            CollectionsPostBean behavior = new CollectionsPostBean();
            behavior.setType(BehaviorType.CONTENT.name());
            behavior.setAction(BehaviorAction.during.name());
            CollectionsPostBean.ObjectBean object = new CollectionsPostBean.ObjectBean();
            behavior.setObject(object);
            object.setId(String.valueOf(book.getBookId()));
            object.setName(String.valueOf(book.getName()));
            object.setCpid(String.valueOf(book.getCpId()));
            object.setTypeid(String.valueOf(book.getTypeId()));
            object.setData1(during);
            object.setData2("模块名:" + module.getName());
            object.setData3("模块号:" + module.getSeq());
            mWebClient.reportBehavior(behavior);
        }
    }

    /**
     * <p>查询type的所有子分类.</p>
     * @param book book
     * @return modules
     */
    /*package*/ List<BookModule> queryModules(PictureBook book) {
        LOGD(TAG, "queryModules() called with: book = [" + book + "]");
        List<BookModule> modules = new ArrayList<>();
        if (book != null) {
            SQLiteDatabase db = mPbOpenHelper.getReadableDatabase();
            Cursor cursor = db.query(Tables.BOOK_MODULES,
                    null,
                    BookModulesColumns.BOOK_ID + "=?",
                    new String[]{String.valueOf(book.getBookId())},
                    null,
                    null,
                    BookModulesColumns.SEQ + " ASC");
            if (cursor != null) {
                try {
                    while (cursor.moveToNext()) {
                        BookModule module = new BookModule();
                        module.setUrl(obtainRealPath(cursor.getString(cursor.getColumnIndexOrThrow(BookModulesColumns.URL))));
                        module.setSeq(cursor.getInt(cursor.getColumnIndexOrThrow(BookModulesColumns.SEQ)));
                        module.setBookId(cursor.getInt(cursor.getColumnIndexOrThrow(BookModulesColumns.BOOK_ID)));
                        module.setName(cursor.getString(cursor.getColumnIndexOrThrow(BookModulesColumns.NAME)));
                        module.setIcon(obtainRealPath(cursor.getString(cursor.getColumnIndexOrThrow(BookModulesColumns.ICON))));
                        modules.add(module);
                    }
                } finally {
                    cursor.close();
                }
            }
        }
        return modules;
    }

    /**
     * <p>获取真实的http全路径.</p>
     * @param path 相对于host的路径
     * @return 全路径
     */
    /*package*/ String obtainRealPath(String path) {
        if (path != null && path.endsWith("index.html")) {
            return PBMContract.ASSET_DIR + path;
            //return PBMContract.OEM_DIR + path;
            //return PBMContract.CONTENT_PORTAL + path;
        } else {
            return PBMContract.ASSET_DIR + path;
            //return PBMContract.OEM_DIR + path;
        }
        //return UrlUtils.concatenateUrl(PBMContract.CONTENT_PORTAL, path);
    }
}
