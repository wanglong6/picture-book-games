package com.jzmedia.pbm.vp;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;

import com.jzmedia.pbm.contract.PBMContract;
import com.jzmedia.pbm.database.PictureBooksContract.*;
import com.jzmedia.pbm.database.PictureBooksDatabase.*;
import com.jzmedia.pbm.database.domain.BookModule;
import com.jzmedia.pbm.database.domain.BookType;
import com.jzmedia.pbm.database.domain.PictureBook;
import java.util.ArrayList;
import java.util.List;
import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.makeLogTag;
import androidx.annotation.NonNull;

/**
 * Created by Leo on 2018/3/9.
 * showcase presenter.
 */

public class ShowcasePresenter extends BasePresenter<ShowcaseContract.View> implements ShowcaseContract.Presenter {
    /** 打印标签. */
    private static final String TAG = makeLogTag("ShowcasePresenter");

    /**
     * <p>构造函数.</p>
     * @param activity activity
     * @param view the view of mvp
     */
    public ShowcasePresenter(@NonNull Activity activity, @NonNull ShowcaseContract.View view) {
        super(activity, view);
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void initWithTypeId(final int typeId) {
        BookType type = queryTypeById(typeId);
        mView.showMainType(type);
        loadSubTypes(type);
    }

    @Override
    public void openSubType(BookType type) {
        LOGD(TAG, "openSubType() called with: type = [" + type + "]");
        SQLiteDatabase db = mPbOpenHelper.getReadableDatabase();
        String selection = BooksColumns.TYPE_ID + " in ( ?";
        List<BookType> types = queryAllSubTypes(type);
        String[] selectionArgs = new String[types.size() + 1];
        selectionArgs[0] = String.valueOf(type.getTypeId());
        int i = 0;
        for (BookType t : types) {
            selection += " ,?";
            selectionArgs[++i] = String.valueOf(t.getTypeId());
        }
        selection += ")";


        Cursor cursor = db.query(Tables.BOOKS,
                null,
                selection,
                selectionArgs,
                null,
                null,
                BooksColumns.TYPE_ID + " ASC");

        List<PictureBook> books = new ArrayList<>();
        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    PictureBook book = new PictureBook();
                    book.setBookId(cursor.getInt(cursor.getColumnIndexOrThrow(BooksColumns.BOOK_ID)));
                    book.setCpId(cursor.getInt(cursor.getColumnIndexOrThrow(BooksColumns.TYPE_ID)));
                    book.setTypeId(cursor.getInt(cursor.getColumnIndexOrThrow(BooksColumns.TYPE_ID)));
                    book.setName(cursor.getString(cursor.getColumnIndexOrThrow(BooksColumns.NAME)));
                    book.setVersion(cursor.getInt(cursor.getColumnIndexOrThrow(BooksColumns.VERSION)));
                    book.setVIcon(obtainRealPath(cursor.getString(cursor.getColumnIndexOrThrow(BooksColumns.V_ICON))));
                    book.setHIcon(obtainRealPath(cursor.getString(cursor.getColumnIndexOrThrow(BooksColumns.H_ICON))));
                    book.setPath(cursor.getString(cursor.getColumnIndexOrThrow(BooksColumns.PATH)));
                    book.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(BooksColumns.DESCRIPTION)));
                    book.setAge(cursor.getString(cursor.getColumnIndexOrThrow(BooksColumns.AGE)));
                    books.add(book);
                }
            } finally {
                cursor.close();
            }
        }

        mView.showBooks(type, books);
        //reportPageEnter(type);
    }

    @Override
    public void openBook(PictureBook book) {
        switch (book.getTypeId()) {
            case PBMContract.TYPE_PICTUREBOOK_QGBD:
            case PBMContract.TYPE_PICTUREBOOK_XGYC:
            case PBMContract.TYPE_PICTUREBOOK_XGPY:
            case PBMContract.TYPE_PICTUREBOOK_SHJW:
                mView.showModuleActivity(book);
                break;
            case PBMContract.TYPE_EBOOK_QGBD:
            case PBMContract.TYPE_EBOOK_SHRZ:
            case PBMContract.TYPE_EBOOK_KXFX:
            case PBMContract.TYPE_EBOOK_SHJW: {
                List<BookModule> modules = queryModules(book);
                if (modules.size() > 0) {
                    mView.showH5Activity(book, modules.get(0));
                } else {
                    mView.showNoModule();
                }
            }
            break;
            case PBMContract.TYPE_CARTOON_QGBD:
            case PBMContract.TYPE_CARTOON_SHRZ:
            case PBMContract.TYPE_CARTOON_KXFX:
            case PBMContract.TYPE_CARTOON_SHJW: {
                List<BookModule> modules = queryModules(book);
                if (modules.size() > 0) {
                    mView.showVideoActivity(book, modules.get(0));
                } else {
                    mView.showNoModule();
                }
            }
            break;
            default:
                break;
        }
    }

    @Override
    public BookType queryTypeById(int typeId) {
        SQLiteDatabase db = mPbOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(Tables.BOOK_TYPES,
                null,
                BookTypesColumns.TYPE_ID + "=?",
                new String[]{String.valueOf(typeId)},
                null,
                null,
                null);

        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    BookType type = new BookType();
                    type.setTypeId(cursor.getInt(cursor.getColumnIndexOrThrow(BookTypesColumns.TYPE_ID)));
                    type.setName(cursor.getString(cursor.getColumnIndexOrThrow(BookTypesColumns.NAME)));
                    type.setMemo(cursor.getString(cursor.getColumnIndexOrThrow(BookTypesColumns.MEMO)));
                    type.setParentId(cursor.getInt(cursor.getColumnIndexOrThrow(BookTypesColumns.PID)));
                    type.setIcon(cursor.getString(cursor.getColumnIndexOrThrow(BookTypesColumns.ICON)));
                    type.setHasSub(cursor.getInt(cursor.getColumnIndexOrThrow(BookTypesColumns.HASSUB)) != 0);
                    return type;
                }
            } finally {
                cursor.close();
            }
        }
        return null;
    }

    /**
     * <p>加载二级类型</p>
     * @param type 一级类型
     */
    private void loadSubTypes(BookType type) {
        LOGD(TAG, "loadSubTypes() called with: type = [" + type + "]");

        final List<BookType> types = new ArrayList<>();
        types.add(type);
        types.addAll(querySubTypes(type));

        mView.showSubTypes(types);
    }

    /**
     * <p>查询type的所有子分类.</p>
     * @param type type
     * @return sub types
     */
    private List<BookType> queryAllSubTypes(BookType type) {
        LOGD(TAG, "querySubTypes() called with: type = [" + type + "]");
        if (!type.isHasSub()) {
            return new ArrayList<>();
        } else {
            List<BookType> types = querySubTypes(type);
            /*for (BookType t : types) {
                types.addAll(queryAllSubTypes(t));
            }*/
            return types;
        }
    }

    /**
     * <p>查询type的子分类</p>
     * @param type type
     * @return 子分类
     */
    private List<BookType> querySubTypes(BookType type) {
        LOGD(TAG, "querySubTypes() called with: type = [" + type + "]");
        SQLiteDatabase db = mPbOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(Tables.BOOK_TYPES,
                null,
                BookTypesColumns.PID + "=?",
                new String[]{String.valueOf(type.getTypeId())},
                null,
                null,
                BookTypesColumns.TYPE_ID + " ASC");

        List<BookType> types = new ArrayList<>();
        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    BookType stype = new BookType();
                    stype.setTypeId(cursor.getInt(cursor.getColumnIndexOrThrow(BookTypesColumns.TYPE_ID)));
                    stype.setName(cursor.getString(cursor.getColumnIndexOrThrow(BookTypesColumns.NAME)));
                    stype.setMemo(cursor.getString(cursor.getColumnIndexOrThrow(BookTypesColumns.MEMO)));
                    stype.setParentId(cursor.getInt(cursor.getColumnIndexOrThrow(BookTypesColumns.PID)));
                    stype.setIcon(cursor.getString(cursor.getColumnIndexOrThrow(BookTypesColumns.ICON)));
                    stype.setHasSub(cursor.getInt(cursor.getColumnIndexOrThrow(BookTypesColumns.HASSUB)) != 0);
                    types.add(stype);
                }
            } finally {
                cursor.close();
            }
        }
        return types;
    }

}
