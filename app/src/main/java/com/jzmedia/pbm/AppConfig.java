package com.jzmedia.pbm;

import android.content.Context;
import android.text.TextUtils;

import com.jzmedia.pbm.settings.SettingsUtils;

import java.util.Random;
import java.util.UUID;

/**
 * Created by Leo on 2018/3/8.
 * config for app.
 */
public class AppConfig {

    /** 应用实例. */
    private String mAppInstanceId;

    /**
     * <p>构造函数</p>
     * @param context context
     */
    public AppConfig(Context context) {
        mAppInstanceId = SettingsUtils.getAppInstanceId(context);
        if (TextUtils.isEmpty(mAppInstanceId)) {
            mAppInstanceId = UUID.randomUUID().toString() + new Random(System.currentTimeMillis()).nextInt(10000);
            SettingsUtils.setAppInstanceId(context, mAppInstanceId);
        }
    }

    /**
     * <p>获取应用实例</p>
     * @return 应用实例
     */
    public String getAppInstanceId() {
        return mAppInstanceId;
    }

}
