package com.jzmedia.pbm.util;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;

/**
 * Created by Leo on 2016/9/11.
 */
public class DensityUtils {

    /**
     * 不能实例化.
     * @throws Exception 实例化时会抛出异常
     */
    private DensityUtils() throws Exception {
        throw new Exception();
    }

    /**
     * 根据分辨率从dp的单位转成为px(像素)
     * @param context 上下文对象
     * @param dpVal 上下文对象
     */
    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpVal, context.getResources().getDisplayMetrics());
    }

    /**
     * <p>根据屏幕分辨率适配root视图.</p>
     * @param context context
     * @param root 需要适配的区域的root view
     */
    public static void adjustScreen(Context context, View root) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        if (wm != null && wm.getDefaultDisplay() != null) {
            wm.getDefaultDisplay().getMetrics(dm);
            int width = dm.widthPixels;         // 屏幕宽度（像素）
            int height = dm.heightPixels;       // 屏幕高度（像素）
            float density = dm.density;         // 屏幕密度（0.75 / 1.0 / 1.5）
            // 屏幕宽度算法:屏幕宽度（像素）/屏幕密度
            int screenWidth = (int) (width / density);  // 屏幕宽度(dp)
            int screenHeight = (int) (height / density);// 屏幕高度(dp)

            float scaleX = screenWidth / 1024f;
            float scaleY = screenHeight / 600f;

            Log.d("dl", "adjustScreen: scaleX" + scaleX);
            Log.d("dl", "adjustScreen: scaleY" + scaleY);

            root.setPivotX(0);
            root.setPivotX(0);

            ObjectAnimator animatorX = ObjectAnimator.ofFloat(root, "scaleX", 1f, scaleX);
            ObjectAnimator animatorY = ObjectAnimator.ofFloat(root, "scaleY", 1f, scaleY);
            AnimatorSet animSet = new AnimatorSet();
            animSet.setDuration(0);
            animSet.setInterpolator(new LinearInterpolator());
            //两个动画同时执行
            animSet.playTogether(animatorX, animatorY);
            animSet.start();
        }
    }

}
