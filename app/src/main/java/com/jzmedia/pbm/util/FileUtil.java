package com.jzmedia.pbm.util;

import android.text.TextUtils;
import android.util.Log;
import com.google.gson.Gson;
import com.jzmedia.pbm.model.upgrade.bean.UpgradeBean;
import com.jzmedia.pbm.model.web.domain.UpdateCfgBean;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * file
 * Created by Leo on 2017/12/29.
 */

public class FileUtil {
    private String TAG = "FileUtil";
    private static FileUtil mUtil;

    public static FileUtil getInstance() {
        if (mUtil == null) {
            mUtil = new FileUtil();
        }
        return mUtil;
    }

    /**
     * 保存配置文件到本地
     *
     * @param response
     */
    public void saveCfgFile(final String response, final String path) {
        File cfgFile = new File(path);
        //判斷該配置文件是否已经存在
        if (cfgFile.exists()) {
            cfgFile.delete();
            //return;
        }
        FileOutputStream fos = null;//FileOutputStream会自动调用底层的close()方法，不用关闭
        BufferedWriter bw = null;
        try {
            fos = new FileOutputStream(path, false);//这里的第二个参数代表追加还是覆盖，true为追加，flase为覆盖
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            bw.write(response);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();//关闭缓冲流
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 读取配置文件信息并保存到实体
     *
     * @return
     */
    public UpdateCfgBean getLocalUpdateCfg(String path) {
        //String path = PBMContract.COMMON_SAVE_DIR + PBMContract.CONFIG_NAME;
        File file = new File(path);
        if (!file.exists()) {
            return null;
        }
        UpdateCfgBean bean = new UpdateCfgBean();
        try {
            InputStreamReader isr = new InputStreamReader(new FileInputStream(path), "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }
            br.close();
            isr.close();

            bean = new Gson().fromJson(builder.toString(), UpdateCfgBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bean;
    }
    /**
     * 读取下载列表文件信息并保存到实体
     *
     * @return
     */
    public UpgradeBean getLocalUpgradeBean(String path) {
        //String path = ConstantUtil.COMMON_SAVE_DIR + DOWNLOAD_LIST_FILE;
        File file = new File(path);
        if (!file.exists()) {
            return null;
        }
        UpgradeBean bean = new UpgradeBean();
        try {
            InputStreamReader isr = new InputStreamReader(new FileInputStream(path), "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }
            br.close();
            isr.close();
            bean = new Gson().fromJson(builder.toString(),UpgradeBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bean;
    }

    /**
     * Get MD5 from file.
     * @param file
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public static String sumFileMd5(File file) throws NoSuchAlgorithmException, IOException  {
        String md5 = "";
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            MessageDigest digester = MessageDigest.getInstance("MD5");
            byte[] bytes = new byte[8192];
            int byteCount;
            while ((byteCount = fis.read(bytes)) > 0) {
                digester.update(bytes, 0, byteCount);
            }
            byte[] digest = digester.digest();
            for (byte b : digest) {
                md5 = md5 + Integer.toString(256 + (0xFF & b), 16).substring(1);
            }
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return md5;
    }

    /**
     * 判断文件的MD5是否一致.
     *
     * @param filePath
     * @param md5
     * @return
     */
    public boolean checkFileMD5(String filePath, String md5) {
        boolean isFileComplete = false;
        File file = new File(filePath);
        if (!file.exists()) {
            Log.d(TAG, "firm file is not exist: ");
            return false;
        }
        if(TextUtils.isEmpty(md5)){
            return false;
        }
        try {
            String fileMd5 = sumFileMd5(file);
            Log.d(TAG, "local file md5: " + fileMd5);
            Log.d(TAG, "service file md5: " + md5);
            if (md5.equals(fileMd5)) {
                isFileComplete = true;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            isFileComplete = false;
        } catch (IOException e) {
            e.printStackTrace();
            isFileComplete = false;
        }
        return isFileComplete;
    }
    /**
     * 截取最后一部分：“xxx/tset.apk” -->  ""tset.apk
     *
     * @param path
     * @return
     */
    public String getFileNameFromPath(String path) {
        String[] res = path.split("/");
        return res[res.length - 1];
    }

    /**
     * 去掉后缀：“test.apk”-->"test"
     *
     * @param file
     * @return
     */
    public String removeSuffix(String file) {
        String[] res = file.split("\\.");
        return res[0];
    }

    /**
     * 根据指定的目录获取文件的大小
     *
     * @param path
     * @return
     */
    public long getFileSize(String path) {
        long fileLength = 0;
        File file = new File(path);
        if (file.exists()) {
            fileLength = file.length();
        }
        return fileLength;
    }

    /**
     * 删除固件
     *
     * @param path
     */
    public void deleteFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            Log.d(TAG, "deleteFile: File not exists !!! " + path);
            return;
        }
        file.delete();
    }

}
