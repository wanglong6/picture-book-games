package com.jzmedia.pbm.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import com.jzmedia.pbm.model.upgrade.bean.ApkBean;
import java.io.File;

public class AppManagerUtil {
    private String TAG = "AppManagerUtil";
    private static AppManagerUtil mUtil;
    //private static Context mContext;

    public static AppManagerUtil getInstance() {
        if (mUtil == null) {
            mUtil = new AppManagerUtil();
            //mContext = context;
        }
        return mUtil;
    }
    /**
     * 使用系统推荐安装APK.
     */
    public void recommandInstallApk(final Context context, File filePath) {
        Intent intentInstall = new Intent();
        //开始启动安装器安装apk  
        intentInstall.setAction(Intent.ACTION_VIEW);
        intentInstall.setDataAndType(Uri.fromFile(filePath), "application/vnd.android.package-archive");
        intentInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intentInstall);
    }
    /**
     * 自升级，删除源文件.
     */
    public boolean deleteDownloadApk(final Context context){
        //File  dir = new File(context.getFilesDir().getAbsolutePath());
        File  dir = context.getExternalFilesDir(null);
        if(!dir.exists()){
            return true;
        }
        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                String path = file.getAbsolutePath();
                if (path.endsWith(".apk")) {
                    file.delete();
                }
            }
        }
        return false;
    }

    /**
     * 获取指定sdcard目录下所有的apk的版本信息
     */
    public ApkBean getDownloadApk(final Context context) {
        PackageManager packageManager = context.getPackageManager();
        ApkBean apkBean = null;
        FileUtil fileUtil = FileUtil.getInstance();
        //File  scannerApk = new File(mContext.getFilesDir().getAbsolutePath());
        File  scannerApk = context.getExternalFilesDir(null);
        if(!scannerApk.exists()){
            return null;
        }
        if (scannerApk.isDirectory()) {
            for (File file : scannerApk.listFiles()) {
                String path = file.getAbsolutePath();
                if (path.endsWith(".apk")) {
                    PackageInfo packageInfo = packageManager.getPackageArchiveInfo(path, PackageManager.GET_ACTIVITIES);
                    if(packageInfo != null){
                        String apk = fileUtil.getFileNameFromPath(path);
                        String apkName = fileUtil.removeSuffix(apk);
                        apkBean = new ApkBean();
                        apkBean.setPackageName(packageInfo.packageName);
                        apkBean.setVersion(packageInfo.versionCode);
                        apkBean.setApkPath(path);
                        apkBean.setApkName(apkName);
                        break;
                    }
                }
            }
        }
        return apkBean;
    }

    /*public void silenceInstallApk(final Context context, final ApkBean apk) {
        try {
            final MyApplicationManager am = new MyApplicationManager(context);
            am.setOnInstalledPackaged(new OnInstalledPackaged() {
                public void packageInstalled(String packageName, int returnCode) {
                    Log.d(TAG, "packageInstalled: PackageName->" + packageName
                            + ", returnCode = " + returnCode);
                }
            });
            try {
                Log.d(TAG, "silenceInstallApk: Now to install.");
                am.installPackage(apk.getApkPath());
                Log.d(TAG, "silenceInstallApk: After installed.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    /**
     * 判断sdcard下的apk是否存在.
     * @param path
     * @return
     */
    public boolean apkIsComplete(final Context context, String path){
        boolean result = false;
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo info = pm.getPackageArchiveInfo(path, PackageManager.GET_ACTIVITIES);
            if (info != null) {
                result = true;
            }
        } catch (Exception e) {
            result = false;
        }
        return result;
    }
}
