package com.jzmedia.pbm.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jzmedia.pbm.contract.PBMContract;

/**
 * Created by Leo He on 2018/3/22.
 */

public class UserActionUtil {

    public static String getUpAppActionInfo(final Context context, int action){
        PackageManager pm = context.getPackageManager();
        String verInfo = null;
        JsonObject json;
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            String appName = info.applicationInfo.loadLabel(pm).toString();

            json = new JsonObject();
            JsonObject object = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            object.addProperty("id", context.getPackageName());
            object.addProperty("name",appName);
            object.addProperty("data1",info.versionName);
            object.addProperty("data2","");
            object.addProperty("data3","");

            json.addProperty("type", "APP");
            if (action == PBMContract.ACTION_ENTER) {
                json.addProperty("action","enter");
            } else {
                json.addProperty("action","quit");
            }

            json.add("object", object);
            json.addProperty("sn", SystemInfoUtil.getStb_sn());
            json.addProperty("mac", "");
            json.addProperty("who", SystemInfoUtil.getStb_sn());
            json.addProperty("from", appName);
            json.addProperty("time", System.currentTimeMillis());
            jsonArray.add(json);
            verInfo = jsonArray.toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return verInfo;
    }

    public static String getUpContentActionInfo(final Context context, String name, int action){
        PackageManager pm = context.getPackageManager();
        String verInfo = null;
        JsonObject json;
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            String appName = info.applicationInfo.loadLabel(pm).toString();

            json = new JsonObject();
            JsonObject object = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            object.addProperty("id", " ");
            object.addProperty("name",name);
            object.addProperty("data1","");
            object.addProperty("data2","");
            object.addProperty("data3","");
            object.addProperty("data4","");
            object.addProperty("data5","");
            object.addProperty("data6","");

            json.addProperty("type", "CONTENT");
            if (action == PBMContract.ACTION_ENTER) {
                json.addProperty("action","enter");
            } else if (action == PBMContract.ACTION_COMPLETE) {
                json.addProperty("action","complete");
            } else {
                json.addProperty("action","quit");
            }

            json.add("object", object);
            json.addProperty("sn", SystemInfoUtil.getStb_sn());
            json.addProperty("mac", "");
            json.addProperty("who", SystemInfoUtil.getStb_sn());
            json.addProperty("from", appName);
            json.addProperty("time", System.currentTimeMillis());
            jsonArray.add(json);
            verInfo = jsonArray.toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return verInfo;
    }

    public static String getUpPageActionInfo(final Context context, String pageName, int action){
        PackageManager pm = context.getPackageManager();
        String verInfo = null;
        JsonObject json;
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            String appName = info.applicationInfo.loadLabel(pm).toString();

            json = new JsonObject();
            JsonObject object = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            object.addProperty("id", " ");
            object.addProperty("name",pageName);
            object.addProperty("data1","");
            object.addProperty("data2","");
            object.addProperty("data3","");
            object.addProperty("data4","");
            object.addProperty("data5","");
            object.addProperty("data6","");

            json.addProperty("type", "PAGE");
            if (action == PBMContract.ACTION_ENTER) {
                json.addProperty("action","enter");
            }  else {
                json.addProperty("action","quit");
            }

            json.add("object", object);
            json.addProperty("sn", SystemInfoUtil.getStb_sn());
            json.addProperty("mac", "");
            json.addProperty("who", SystemInfoUtil.getStb_sn());
            json.addProperty("from", appName);
            json.addProperty("time", System.currentTimeMillis());
            jsonArray.add(json);
            verInfo = jsonArray.toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return verInfo;
    }

    /**
     * 回传版本信息
     */
    public static String getUploadVerInfo(final Context context, String installState){
        PackageManager pm = context.getPackageManager();
        String verInfo = null;
        JsonObject json;
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            String appName = info.applicationInfo.loadLabel(pm).toString();

            json = new JsonObject();
            JsonObject object = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            object.addProperty("id",info.packageName);
            object.addProperty("name",appName);
            object.addProperty("data1",info.versionName);
            object.addProperty("data2","");
            object.addProperty("data3","");
            object.addProperty("data4",installState);
            object.addProperty("data5","");
            object.addProperty("data6","");

            json.addProperty("type", "APP");
            json.addProperty("action", "install");
            json.add("object", object);
            json.addProperty("sn", SystemInfoUtil.getStb_sn());
            json.addProperty("mac", "");
            json.addProperty("who", SystemInfoUtil.getStb_sn());
            json.addProperty("from", appName);
            json.addProperty("time", System.currentTimeMillis());
            jsonArray.add(json);

            verInfo = jsonArray.toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return verInfo;
    }
}
