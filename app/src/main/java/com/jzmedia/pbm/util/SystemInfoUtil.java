package com.jzmedia.pbm.util;

import android.os.Build;
//import android.os.SystemProperties;
import android.text.TextUtils;

import java.lang.reflect.Method;

/**
 * Created by Leo on 2017/12/25.
 */

public class SystemInfoUtil {
    private static final String TAG = SystemInfoUtil.class.getSimpleName();
    private static final String UNKNOWN = "unknown";
    private static final String STB_SERIAL_PROPERTY = "ro.deviceinfo.stbid";
    public static final String STBSERIAL = "0000000000000000";//SystemProperties.get(STB_SERIAL_PROPERTY, UNKNOWN);

    private static final String STB_FW_PROPERTY = "ro.deviceinfo.swversion";
    public static final String STBFW = " ";//SystemProperties.get(STB_FW_PROPERTY, UNKNOWN);

    private static final String STB_PRODUCT_PROPERTY = "ro.deviceinfo.product";
    public static final String STBPRODUCT = "ALILO_T6";//SystemProperties.get(STB_PRODUCT_PROPERTY, UNKNOWN);
    public static final String STBVENDOR = "Alilo";//SystemProperties.get(STB_PRODUCT_PROPERTY, UNKNOWN);

    public static final String STBPUBLISH = "ro.build.date";
    public static final String STBPUBLISHTIME = " ";//SystemProperties.get(STBPUBLISH, UNKNOWN);
    /**
     * 获取机顶盒的sn
     */
    public static String getStb_sn(){
        String sn_string = getSerialNumberCustom();
        if (TextUtils.isEmpty(sn_string) || sn_string.equals("unknown")){
            return "0000000000000000";
        }else {
            return sn_string;
        }
    }
    private static String getSerialNumberCustom() {
        String serial = null;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);
            serial = (String) get.invoke(c, "ro.serialnocustom");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serial;
    }
    /**
     * 获取机顶盒的设备型号
     */
    public static String getModel(){
        String model = STBPRODUCT;
        if (TextUtils.isEmpty(model)){
            return "null";
        }else {
            return model;
        }
    }

    /**
     * 获取机顶盒的设备商
     */
    public static String getVendor(){
        return STBVENDOR;
    }

    /**
     * 系统的版本号
     */
    public String getSystemVersion(){
        String version = Build.VERSION.RELEASE;
        if (TextUtils.isEmpty(version) || version.equals("unknown")){
            return "Android";
        }else {
            return "Android "+ version;
        }
    }
    /**
     * 固件的版本号
     */
    public static String getFirmwareVersion(){
        String version = STBFW;
        if (TextUtils.isEmpty(version) || version.equals("unknown")){
            return "unknown";
        }else {
            return  version;
        }
    }

    /**
     * 软件发布时间
     */
    public String getPublishTime(){
        String time = STBPUBLISHTIME;
        if (TextUtils.isEmpty(time) || time.equals("unknown")){
            return "unknown";
        }else {
            return  time;
        }
    }

}
