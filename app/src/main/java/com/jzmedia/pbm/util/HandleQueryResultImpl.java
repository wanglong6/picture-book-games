package com.jzmedia.pbm.util;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jzmedia.pbm.contract.PBMContract;
import com.jzmedia.pbm.model.upgrade.HandleUpdateImpl;
import com.jzmedia.pbm.model.upgrade.bean.AuthorBean;
import com.jzmedia.common.okhttp.OkHttpUtil;
import com.jzmedia.common.okhttp.response.IResponseDownloadHandler;
import com.jzmedia.common.okhttp.response.RawResHandler;
import org.json.JSONException;
import org.json.JSONObject;


public class HandleQueryResultImpl {
    private String TAG = "HandleQueryResultImpl";
    private static volatile HandleQueryResultImpl mInstance = null;
    private OkHttpUtil mOkHttpUtil ;
    private FileUtil mFileUtil;
    private AppManagerUtil mAppManagerUtil;
    private String mToken;
    /**
     * Token 过期时间.
     */
    private long mTokenOverdueTimeMs = 0;

    private HandleQueryResultImpl() {
        mOkHttpUtil = new OkHttpUtil();
        mAppManagerUtil = AppManagerUtil.getInstance();
        mOkHttpUtil.setDebug(true);
        mFileUtil = FileUtil.getInstance();
    }

    public static  HandleQueryResultImpl getInstance(){
        if(mInstance == null){
            synchronized(HandleQueryResultImpl.class){
                if(mInstance == null){
                    mInstance = new HandleQueryResultImpl ();
                }
            }
        }
        return mInstance;
    }

    /**
     * 获取token
     */
    private void requestToken(final Context context, final Handler handle){
        Log.d(TAG, "requestToken.");
        JsonObject json = new JsonObject();
        json.addProperty("user", SystemInfoUtil.getStb_sn());
        json.addProperty("password","jzmedia");
        json.addProperty("sn",SystemInfoUtil.getStb_sn());
        json.addProperty("platform",SystemInfoUtil.getModel());
        json.addProperty("mac","");
        Log.d(TAG, "json.toString()" + json.toString());
        mOkHttpUtil.post().url(PBMContract.VERSION_AUTHOR)
                .tag(context)
                .jsonParams(json.toString())
                .enqueue(new RawResHandler() {
                    @Override
                    public void onSuccess(int statusCode, String response) {
                        Log.d(TAG, "requestToken: onSuccess: " + response);
                        AuthorBean authorBean = new Gson().fromJson(response, AuthorBean.class);
                        mToken = authorBean.getData().getSessionToken();
                        mTokenOverdueTimeMs = System.currentTimeMillis()
                                + ((authorBean.getData().getSessionExpire()) * 1000);
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.what = PBMContract.TOKEN_REQUES_NOTIFY;
                            msg.arg1 = PBMContract.DOWNLOAD_OK;
                            msg.obj = authorBean;
                            handle.sendMessage(msg);
                        }
                    }
                    @Override
                    public void onFailed(int statusCode, String errMsg) {
                        Log.d(TAG, "requestToken: onFailed: " + errMsg);
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.what = PBMContract.TOKEN_REQUES_NOTIFY;
                            msg.arg1 = PBMContract.DOWNLOAD_FAILED;
                            msg.obj = errMsg;
                            handle.sendMessage(msg);
                        }
                    }
                });
    }

    /**
     * 获取升级配置文件.
     */
    public int downloadUpgradeCfgInfo(final Context context, final HandleUpdateImpl.UpdateHandle handle){
        Log.d(TAG, "getUpgradeCfginfo.");
        Log.d(TAG, "getUpgradeCfginfo: currentTimeMillis = " + System.currentTimeMillis());
        if (TextUtils.isEmpty(mToken)
                || ((System.currentTimeMillis() - mTokenOverdueTimeMs) >= 0)) {
            requestToken(context, null);
            return -1;
        }
        mOkHttpUtil.get().url(PBMContract.VERSION_QUERY)
                .tag(context)
                .addHeader("x-access-token", mToken)
                .addParam("target", "APP")
                .addParam("user", SystemInfoUtil.getStb_sn())
                .addParam("sn", SystemInfoUtil.getStb_sn())
                .addParam("platform", SystemInfoUtil.getModel())
                .addParam("mac","")
                .enqueue(new RawResHandler() {
                    @Override
                    public void onSuccess(int statusCode, String response) {
                        Log.d(TAG, "updateCfg onSuccess: "+response);
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.what = PBMContract.DOWNLOAD_UPGRADE_CFG_NOTIFY;
                            msg.arg1 = PBMContract.DOWNLOAD_OK;
                            msg.obj = response;
                            handle.sendMessage(msg);
                        }
                    }
                    @Override
                    public void onFailed(int statusCode, String errMsg) {
                        Log.d(TAG, "updateCfg onFailed: "+errMsg);
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.what = PBMContract.DOWNLOAD_UPGRADE_CFG_NOTIFY;
                            msg.arg1 = PBMContract.DOWNLOAD_FAILED;
                            msg.obj = errMsg;
                            handle.sendMessage(msg);
                        }
                    }
                });

        return 0;
    }

    /**
     * 下载固件和APP配置信息列表.
     * @param url
     */
    public int downloadFirmApksCfg(final Context context, final Handler handle, String url){
        if (TextUtils.isEmpty(mToken)
                || ((System.currentTimeMillis() - mTokenOverdueTimeMs) >= 0)) {
            requestToken(context, null);
            return -1;
        }
        mOkHttpUtil.get().url(url)
                .tag(context)
                .enqueue(new RawResHandler() {
                    @Override
                    public void onSuccess(int statusCode, String response) {
                        Log.d(TAG, "downloadFirmApksCfg: sucess.");
//                        UpgradeBean datas = new Gson().fromJson(response.toString(),UpgradeBean.class);
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.what = PBMContract.DOWNLOAD_FIRM_APP_CFG_NOTIFY;
                            msg.arg1 = PBMContract.DOWNLOAD_OK;
                            msg.obj = response;
                            handle.sendMessage(msg);
                        }
                    }
                    @Override
                    public void onFailed(int statusCode, String errMsg) {
                        Log.d(TAG, "downloadFirmApksCfg: errMsg->"+errMsg);
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.what = PBMContract.DOWNLOAD_FIRM_APP_CFG_NOTIFY;
                            msg.arg1 = PBMContract.DOWNLOAD_FAILED;
                            msg.obj = errMsg;
                            handle.sendMessage(msg);
                        }
                    }
                });

        return 0;
    }


    /**
     * 回传版本信息
     * @param jsonArray
     */
    public int uploadActionInfo(final Context context, final Handler handle, String jsonArray){
        if (TextUtils.isEmpty(mToken)
                || ((System.currentTimeMillis() - mTokenOverdueTimeMs) >= 0)) {
            requestToken(context, null);
            return -1;
        }
        mOkHttpUtil.post().url(PBMContract.UPLOAD_VERSION)
                .tag(context)
                .addHeader("x-access-token",mToken)
                .jsonParams(jsonArray)
                .enqueue(new RawResHandler() {
                    @Override
                    public void onSuccess(int statusCode, String response) {
                        Log.d(TAG, "uploadActionInfo: onSuccess: " + response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            int code = obj.getInt("code");
                            if(code == 0 && handle != null){
                                Message msg = handle.obtainMessage();
                                msg.what = PBMContract.UPLOAD_VERSION_NOTIFY;
                                msg.arg1 = PBMContract.UPLOAD_OK;
                                handle.sendMessage(msg);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "upload version info exception");
                        }
                    }
                    @Override
                    public void onFailed(int statusCode, String errMsg) {
                        Log.d(TAG, "uploadActionInfo: onFailed: " + errMsg);
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.what = PBMContract.UPLOAD_VERSION_NOTIFY;
                            msg.arg1 = PBMContract.UPLOAD_FAILED;
                            handle.sendMessage(msg);
                        }
                    }
                });

        return 0;
    }

    /**
     * 下载固件或APK文件.
     * @param url url.
     * @param appID PackageName or Platform.
     * @param md5 MD5
     */
    public int downLoadFile(final Context context, final Handler handle,
                             final String url, final String appID, String md5) {
        Log.d(TAG, "downLoadFile: Path-> " + url + ", appID-> " + appID);
        if (TextUtils.isEmpty(mToken)
                || ((System.currentTimeMillis() - mTokenOverdueTimeMs) >= 0)) {
            requestToken(context, null);
            return -1;
        }
        final String fileName = mFileUtil.getFileNameFromPath(url);
        final String projectName = mFileUtil.removeSuffix(fileName);

        String basePath = context.getExternalFilesDir(null).getAbsolutePath() + "/";
        String savePath = null;
        if(appID.startsWith("com.")){
            savePath = basePath + fileName;
            boolean apkCompleted = mAppManagerUtil.apkIsComplete(context, savePath);
            if(apkCompleted){
                Log.d(TAG, "this file is complete: ");
                return 0;
            }
        } else {
            savePath = basePath +fileName;
            boolean md5Ok = mFileUtil.checkFileMD5(savePath,md5);
            if(md5Ok){
                Log.d(TAG, "this firm is complete: ");
                return 0;
            }
        }
        final long bytes = FileUtil.getInstance().getFileSize(savePath);
        Log.d(TAG, "downLoadFile: Start position " +bytes);
        mOkHttpUtil.download().url(url)
                /*.tag(tag)*/
                .filePath(savePath)
                .completeBytes(bytes)
                .enqueue(new IResponseDownloadHandler() {
                    @Override
                    public void onFinish(final String path) {
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.obj = appID;
                            msg.what = PBMContract.FIRM_APP_DOWNLOAD_FINISH;
                            msg.arg1 = PBMContract.DOWNLOAD_OK;
                            handle.sendMessage(msg);
                        }
                    }
                    @Override
                    public void onProgress(int rate) {
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.what = PBMContract.DOWNLOAD_FILE_PROGRESS;
                            msg.arg1 = rate;
                            msg.obj = projectName;
                            handle.sendMessage(msg);
                        }
                    }
                    @Override
                    public void onFailed(String errMsg) {
                        Log.d(TAG, "download onFailed: "+errMsg);
                        //下载失败处理
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.obj = appID;
                            msg.what = PBMContract.FIRM_APP_DOWNLOAD_FINISH;
                            msg.arg1 = PBMContract.DOWNLOAD_FAILED;
                            handle.sendMessage(msg);
                        }
                    }
                    @Override
                    public void onCancel() {
                        Log.d(TAG, "download onCancel: ");
                        handle.sendEmptyMessage(PBMContract.DOWNLOAD_FILE_CANCLE);
                    }
                    @Override
                    public void onStart(long total) {
                        super.onStart(total);
                        Log.d(TAG, "download file total == "+total);
                        if (handle != null) {
                            Message msg = handle.obtainMessage();
                            msg.what = PBMContract.DOWNLOAD_FILE_PROGRESS;
                            msg.arg1 = 0;
                            msg.obj = projectName;
                            handle.sendMessage(msg);
                        }
                    }
                });

                return 0;
    }
}
