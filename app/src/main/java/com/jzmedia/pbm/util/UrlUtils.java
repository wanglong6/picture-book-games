package com.jzmedia.pbm.util;

import java.net.MalformedURLException;
import java.net.URL;

import static com.jzmedia.common.util.LogUtils.makeLogTag;

/**
 * Created by Leo on 2017/7/24.
 * url utils.
 */
public class UrlUtils {
    /** 打印标签. */
    private static final String TAG = makeLogTag("UrlUtils");

    /**
     * <p>隐藏构造函数.</p>
     * @throws Exception 抛出异常
     */
    private UrlUtils() throws Exception {
        throw new Exception();
    }

    /**
     * <p>拼接url.</p>
     * @param host host
     * @param path 相对路径
     * @return 真实url
     */
    public static String concatenateUrl(String host, String path) {
        if (path != null) {
            try {
                URL url = new URL(new URL(host), path);
                return url.toExternalForm();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return host;
    }

}
