package com.jzmedia.pbm.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.jzmedia.common.util.LogUtils.makeLogTag;

/**
 * Created by ChristieIn on 2017/9/4.
 * setting utils.
 */

public class SettingsUtils {
    /** 打印标签. */
    private static final String TAG = makeLogTag("SettingsUtils");
    /** 应用实例号. */
    private static final String APP_INSTANCE_ID = "instance_id";

    /**
     * <p>保存instance id.</p>
     * @param context  Context to be used to edit the {@link SharedPreferences}.
     * @param id New value that will be set.
     */
    public static void setAppInstanceId(final Context context, final String id) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString(APP_INSTANCE_ID, id).apply();
    }

    /**
     * <p>获取instance id.</p>
     * @param context  Context to be used to edit the {@link SharedPreferences}.
     */
    public static String getAppInstanceId(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(APP_INSTANCE_ID, null);
    }

}
