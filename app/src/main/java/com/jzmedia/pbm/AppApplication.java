package com.jzmedia.pbm;

import android.app.Application;
import android.database.sqlite.SQLiteOpenHelper;

import com.jzmedia.common.loader.DataLoader;
import com.jzmedia.pbm.database.PictureBooksDatabase;
import com.jzmedia.pbm.model.web.WebClient;

/**
 * Created by Leo on 2018/3/8.
 * Application of Picture Books Museum.
 */

public class AppApplication extends Application {
    /** app config. */
    private AppConfig mConfig;
    /** 用户无关的,无需缓存的json和半缓存图片数据加载器. */
    private DataLoader mDataLoader;
    /** 交互接口. */
    private WebClient mWebClient;
    /** 绘本资源查询数据库. */
    private SQLiteOpenHelper mPictureBookOpenHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        //app config
        mConfig = new AppConfig(this);

        //data loader
        mDataLoader = new DataLoader(this);

        //web client
        mWebClient = new WebClient(this);

        //picture book database open helper.
        mPictureBookOpenHelper = new PictureBooksDatabase(this);

        //crash handler
        CrashHandler handler = new CrashHandler(this, mWebClient);
        handler.init();
    }

    /**
     * <p>获取app config.</p>
     * @return app config
     */
    public AppConfig getAppConfig() {
        return mConfig;
    }

    /**
     * <p>获取DataLoader.</p>
     * @return data loaders
     */
    public DataLoader getDataLoader() {
        return mDataLoader;
    }

    /**
     * <p>获取web client</p>
     * @return web client
     */
    public WebClient getWebClient() {
        return mWebClient;
    }

    /**
     * <p>获取picture book open helper.</p>
     * @return open helper
     */
    public SQLiteOpenHelper getPictureBookOpenHelper() {
        return mPictureBookOpenHelper;
    }

    /**
     * <p>释放资源请求.</p>
     */
    public void release() {
        mPictureBookOpenHelper.close();
        mWebClient.release();
        mDataLoader.release();
    }

}
