package com.jzmedia.pbm.contract;

/**
 * Created by ChristieIn on 2018/3/13.
 * <p>Picture Books Museum Contract.</p>
 * <p>这里是应用使用的一些常量.</p>
 */
public class PBMContract {
    /** 业务支持服务器ip. */
    public static final String PBM_SERVER_IP = "http://update.jzmediatech.com";
    /** 业务支撑服务端口.*/
    public static final String PBM_SERVER_PORT = "80";
    /** 内容服务器. */
    public static final String CONTENT_PORTAL = "http://127.0.0.1:6321";
    /** asset目录. */
    public static final String ASSET_DIR = "file:///android_asset";
    /** oem目录. */
    public static final String OEM_DIR = "file:///oem/jzm";

    /** log loggable*/
    public static final String LOGGABLE_EXTRA_KEY = "loggable";

    /** 绘本精读. */
    public static final int TYPE_PICTUREBOOK = 1;
    /** 电子图书. */
    public static final int TYPE_EBOOK = 2;
    /** 绘本动画. */
    public static final int TYPE_CARTOON = 3;
    /** 绘本精读-性格养成. */
    public static final int TYPE_PICTUREBOOK_XGYC = 101;
    /** 绘本精读-情感表达. */
    public static final int TYPE_PICTUREBOOK_QGBD = 102;
    /** 绘本精读-习惯培养. */
    public static final int TYPE_PICTUREBOOK_XGPY = 103;
    /** 绘本精读-社会交往 */
    public static final int TYPE_PICTUREBOOK_SHJW = 104;
    /** 电子图书-情感表达. */
    public static final int TYPE_EBOOK_QGBD = 201;
    /** 电子图书-生活认知. */
    public static final int TYPE_EBOOK_SHRZ = 202;
    /** 电子图书-社会交往 */
    public static final int TYPE_EBOOK_SHJW = 203;
    /** 电子图书-科学发现. */
    public static final int TYPE_EBOOK_KXFX = 204;
    /** 绘本动画-情感表达. */
    public static final int TYPE_CARTOON_QGBD = 301;
    /** 绘本动画-生活认知. */
    public static final int TYPE_CARTOON_SHRZ = 302;
    /** 绘本动画-社会交往 */
    public static final int TYPE_CARTOON_SHJW = 303;
    /** 绘本动画-科学发现. */
    public static final int TYPE_CARTOON_KXFX = 304;
    /** 点读. */
    public static final int MODULE_DD = 2;
    /** 整读. */
    public static final int MODULE_ZD = 1;
    /** 故事回顾. */
    public static final int MODULE_GSHG = 3;
    /** 拓展延伸. */
    public static final int MODULE_TZYS = 4;
    /** 互动. */
    public static final int MODULE_HD = 5;

    /**版本查询接口：*/
    public static final String VERSION_QUERY = "http://update.jzmediatech.com/api/1.0/stb/config/update";
    /**设备认证的接口*/
    public static final String VERSION_AUTHOR = "http://update.jzmediatech.com/api/1.0/stb/authorization";
    /**版本信息回传*/
    public static final String UPLOAD_VERSION = "http://update.jzmediatech.com/api/1.0/stb/collections";

    public static final int TOKEN_REQUES_NOTIFY = 1;
    /**
     * 总的升级配置文件下载通知处理.
     */
    public static final int DOWNLOAD_UPGRADE_CFG_NOTIFY = 2;
    /**
     * 固件和应用列表下载通知处理.
     */
    public static final int DOWNLOAD_FIRM_APP_CFG_NOTIFY = 3;
    public static final int UPLOAD_VERSION_NOTIFY = 4;
    /**
     * Check whether exist already download firm or apk file.
     * Handle this when the service startup.
     */
    public static final int CHECK_WHETHER_NEED_INSTALL = 5;
    public static final int DOWNLOAD_FILE_CANCLE = 6;
    public static final int DOWNLOAD_FILE_PROGRESS = 7;
    public static final int FIRM_APP_DOWNLOAD_START = 8;
    public static final int FIRM_APP_DOWNLOAD_FINISH = 9;
    public static final int CHECK_FIRM_VER_UPLOAD = 10;
    public static final int UPGRADE_CLIENT_REPLACED = 11;

    public static final int DOWNLOAD_OK = 0;
    public static final int DOWNLOAD_FAILED = 1;
    public static final int UPLOAD_OK = 0;
    public static final int UPLOAD_FAILED = 1;

    /**版本回传*/
    public static final String DOWN_LOAD_PROGRESS = "com.jzmedia.iedu.upgrade.download.progress";

    public static final String USER_ACTION_MSG = "com.jzmedia.iedu.USER_ACTION_MSG";//用户行为的广播
    /**下载完成*/
    public static final String UPGRADE_REFRESH_STATUS = "com.jzmedia.iedu.upgrade.refresh.status";

    /**配置文件名*/
    public static final String CONFIG_NAME = "update.txt";


    //数据收集相关.
    public static final String USER_ACTION = "action";
    public static final String ACTION_TYPE = "type";
    public static final String NAME = "name";
    public static final int ACTION_TYPE_APP = 0;
    public static final int ACTION_TYPE_CONTENT = 1;
    public static final int ACTION_TYPE_PAGE = 2;

    public static final int ACTION_ENTER = 0;// enter
    public static final int ACTION_QUIT = 1;//quit
    public static final int ACTION_COMPLETE = 2;//complete.


}
