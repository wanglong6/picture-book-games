package com.jzmedia.pbm;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
//import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.Toast;

import com.jzmedia.common.util.PackageUtils;
import com.jzmedia.pbm.model.behavior.BehaviorAction;
import com.jzmedia.pbm.model.behavior.BehaviorType;
import com.jzmedia.pbm.model.web.WebClient;
import com.jzmedia.pbm.model.web.domain.CollectionsPostBean;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.LOGE;
import static com.jzmedia.common.util.LogUtils.makeLogTag;

import androidx.annotation.NonNull;

/**
 * Created by Leo on 2018/3/8.
 * crash handler of picture books museum.
 */

public class CrashHandler implements Thread.UncaughtExceptionHandler {
    /** 打印标签. */
    private static final String TAG = makeLogTag("CrashHandler");
    /** crash文件名格式. */
    private static final String BASE_FILENAME_FORMAT = "crash.%s.log";
    /** 上下文. */
    private Context mContext;
    /** 系统默认的处理类. */
    private UncaughtExceptionHandler mDefaultHandler;
    /** 格式化日期，作为日志文件的一部分. */
    private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.getDefault());
    /** web client. */
    private WebClient mWebClient;

    /**
     * <p>构造函数</p>
     * @param context 上下文对象
     */
    /* package */ CrashHandler(@NonNull Context context, @NonNull WebClient client) {
        mContext = checkNotNull(context, "context cannot be null");
        mWebClient = checkNotNull(client, "context cannot be null");
    }

    /**
     * <p>初始化异常捕获类.</p>
     */
    /* package */void init() {
        //获取系统默认的UncaughtException处理器
        mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        //设置CrashHandler为默认的处理器
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        LOGD(TAG, "uncaughtException() called with: thread = [" + thread + "], ex = [" + ex + "]");
        if (!handleException(ex) && mDefaultHandler != null) {
            //如果用户没有处理则让系统默认的异常处理器处理
            mDefaultHandler.uncaughtException(thread, ex);
        } else {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                LOGE(TAG, "error : ", e);
            }
            //退出程序

            ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
            if (am != null) {
                am.killBackgroundProcesses(mContext.getPackageName());
            }
            System.exit(1);
        }
    }

    /**
     * 自定义异常处理，收集错误信息，发送错误报告等操作
     *
     * @param ex 异常
     * @return true:处理了该异常 false:未处理该异常
     */
    private boolean handleException(Throwable ex) {
        LOGD(TAG, "handleException() called with: ex = [" + ex + "]");
        if (ex == null) {
            return false;
        }
        //使用Toast显示提示消息
        new Thread() {
            public void run() {
                Looper.prepare();
                Toast.makeText(mContext, "程序异常", Toast.LENGTH_LONG).show();
                Looper.loop();
            }
        }.start();
        //收集设备参数信息
        Map<String, String> infos = collectDeviceInfo();
        //生成crash信息字符串
        String crash = obtainCrashInfo(infos, ex);
        LOGE(TAG, "handleException() called with: ex = [" + ex + "]");
        String crashFileName = obtainCrashFileName();

        //保存日志
        if (!TextUtils.isEmpty(crash) && !TextUtils.isEmpty(crashFileName)) {
            try {
                saveCrashInfo2File(crash, File.createTempFile("temp", crashFileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(crash)) {
            reportCrash(crash);
        }

        return true;
    }

    /**
     * <p>收集设备参数信息</p>
     */
    private Map<String, String> collectDeviceInfo() {
        Map<String, String> infos = new HashMap<String, String>();
        try {
            PackageInfo pkgInfo = mContext.getPackageManager()
                    .getPackageInfo(mContext.getPackageName(), 0);
            if (pkgInfo != null) {
                String versionName = pkgInfo.versionName == null ? "null" : pkgInfo.versionName;
                String versionCode = pkgInfo.versionCode + "";
                infos.put("versionName", versionName);
                infos.put("versionCode", versionCode);
            }
            Field[] fields = Build.class.getDeclaredFields();
            for (Field field : fields) {
                try {
                    field.setAccessible(true);
                    infos.put(field.getName(), field.get(null).toString());
                    LOGD(TAG, field.getName() + " : " + field.get(null));
                } catch (Exception e) {
                    LOGE(TAG, "an error occured when collect crash info", e);
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return infos;
    }

    private String obtainCrashInfo(Map<String, String> infos, Throwable ex) {
        StringBuffer sb = new StringBuffer();
        for (Map.Entry<String, String> entry : infos.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            sb.append(key + "=" + value + "\n");
        }
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        ex.printStackTrace(printWriter);
        Throwable cause = ex.getCause();
        while (cause != null) {
            cause.printStackTrace(printWriter);
            cause = cause.getCause();
        }
        printWriter.close();
        String result = writer.toString();
        sb.append(result);
        return sb.toString();
    }

    private File obtainCrashFileDir() {

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File dir = mContext.getCacheDir();
            //File dir = TAExternalOverFroyoUtils.getDiskCacheDir(mContext, CRASH_DIR);
            if (!dir.exists()) {
                boolean ret = dir.mkdirs();
                if (!ret) {
                    return null;
                }
            }
            return dir;
        }
        return null;
    }

    private String obtainCrashFileName() {
        long timestamp = System.currentTimeMillis();
        String time = formatter.format(new Date());
        return String.format(Locale.getDefault(), BASE_FILENAME_FORMAT, time + "-" + timestamp);
    }

    private void deleteFileInDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File file : files) {
                    try {
                        file.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * <p>保存错误信息到文件中</p>
     * @param crash 异常字符串
     * @return 返回文件名称，便于将文件传送到服务器
     */
    private void saveCrashInfo2File(String crash, File file) {
        FileOutputStream fos = null;
        try {
            LOGD(TAG, "getAbsolutePath: " + file.getAbsolutePath());
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            } else {
                file.createNewFile();
            }
            fos = new FileOutputStream(file);
            fos.write(crash.getBytes());
        } catch (FileNotFoundException e) {
            LOGE(TAG, "an error occured while writing file...", e) ;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * <p>上报crash</p>
     * @param crash crash
     */
    private void reportCrash(String crash) {
        CollectionsPostBean behavior = new CollectionsPostBean();
        behavior.setType(BehaviorType.APP.name());
        behavior.setAction(BehaviorAction.crash.name());
        CollectionsPostBean.ObjectBean object = new CollectionsPostBean.ObjectBean();
        behavior.setObject(object);
        //object
        PackageInfo info = PackageUtils.getOwnPackageInfo(mContext);
        if (info != null) {
            object.setId(info.packageName);
            object.setName(PackageUtils.getOwnApplicationLabele(mContext));
            object.setData1(String.valueOf(info.versionCode));
            object.setData2(info.versionName);
            object.setData3(Base64.encodeToString(crash.getBytes(), Base64.DEFAULT));
        }
        //report
        mWebClient.reportBehavior(behavior);
    }

}
