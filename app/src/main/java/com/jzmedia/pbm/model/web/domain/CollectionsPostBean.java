package com.jzmedia.pbm.model.web.domain;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by ChristieIn on 2018/3/9.
 * 数据收集post bean.
 */
@Keep
public class CollectionsPostBean {
    /**
     * type : APP
     * action : enter/quit
     * object : {"id":"PKGID","name":"应用名称","data1":"应用版本","data2":"应用开发者","data3":""}
     * sn : SNSNSNSNSNS
     * mac : EEEEEEEEEEEE
     * who : boatman
     * from : iedu_jiuzhou
     * time : 1491810942
     * vendor : 九州
     */
    private static final String INIT_VAL = "";
    /** 记录_id,不用于上传. **/
    private long _id = 0;
    private String type = INIT_VAL;
    private String action = INIT_VAL;
    private ObjectBean object;
    private String sn = INIT_VAL;
    private String mac = INIT_VAL;
    private String who = INIT_VAL;
    private String from = INIT_VAL;
    private long time = 0;
    private String platform = INIT_VAL;
    private String vendor = INIT_VAL;

    public long getRowId() {
        return _id;
    }

    public void setRowId(long rowId) {
        this._id = rowId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public ObjectBean getObject() {
        return object;
    }

    public void setObject(ObjectBean object) {
        this.object = object;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    @Keep
    public static class ObjectBean {
        /**
         * id : PKGID
         * name : 应用名称
         * data1 : 应用版本
         * data2 : 应用开发者
         * data3 :
         */

        private String id = INIT_VAL;
        private String name = INIT_VAL;
        private String cpid = INIT_VAL;
        private String typeid = INIT_VAL;
        private String data1 = INIT_VAL;
        private String data2 = INIT_VAL;
        private String data3 = INIT_VAL;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCpid() {
            return cpid;
        }

        public void setCpid(String cpid) {
            this.cpid = cpid;
        }

        public String getTypeid() {
            return typeid;
        }

        public void setTypeid(String typeid) {
            this.typeid = typeid;
        }

        public String getData1() {
            return data1;
        }

        public void setData1(String data1) {
            this.data1 = data1;
        }

        public String getData2() {
            return data2;
        }

        public void setData2(String data2) {
            this.data2 = data2;
        }

        public String getData3() {
            return data3;
        }

        public void setData3(String data3) {
            this.data3 = data3;
        }

        @Override
        public String toString() {
            return "ObjectBean{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", cpid='" + cpid + '\'' +
                    ", typeid='" + typeid + '\'' +
                    ", data1='" + data1 + '\'' +
                    ", data2='" + data2 + '\'' +
                    ", data3='" + data3 + '\'' +
                    '}';
        }
    }
}
