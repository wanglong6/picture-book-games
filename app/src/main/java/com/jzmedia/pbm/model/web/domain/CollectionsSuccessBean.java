package com.jzmedia.pbm.model.web.domain;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by ChristieIn on 2018/3/9.
 * <p>数据收集成功响应体</p>
 */
@Keep
public class CollectionsSuccessBean extends BaseSuccessBean<String> {
}
