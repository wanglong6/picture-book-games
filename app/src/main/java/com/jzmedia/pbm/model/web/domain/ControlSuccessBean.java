package com.jzmedia.pbm.model.web.domain;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by ChristieIn on 2018/3/27.
 * control success bean.
 */
@Keep
public class ControlSuccessBean extends BaseSuccessBean<ControlSuccessBean.DataBean> {

    @Keep
    public static class DataBean {

        /**
         * enable : true
         * mode : package
         * cycle : 5
         * number : 50
         */
        private boolean enable;
        private String mode;
        private int cycle;
        private int number;

        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public int getCycle() {
            return cycle;
        }

        public void setCycle(int cycle) {
            this.cycle = cycle;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "enable=" + enable +
                    ", mode='" + mode + '\'' +
                    ", cycle=" + cycle +
                    ", number=" + number +
                    '}';
        }
    }
}
