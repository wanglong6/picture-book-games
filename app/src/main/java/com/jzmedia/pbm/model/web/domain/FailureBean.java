package com.jzmedia.pbm.model.web.domain;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by ChristieIn on 2018/3/9.
 * failure bean.
 */
@Keep
public class FailureBean {

    /**
     * code : 2002
     * error : 认证失败
     */

    private int code;
    private String error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "FailureBean{" +
                "code=" + code +
                ", error='" + error + '\'' +
                '}';
    }
}
