package com.jzmedia.pbm.model.web;

import android.content.Context;
//import android.support.annotation.NonNull;


import com.jzmedia.pbm.util.NetworkUtils;
import com.jzmedia.pbm.util.SystemInfoUtil;

import static com.google.common.base.Preconditions.checkNotNull;

import androidx.annotation.NonNull;

/**
 * Created by ChristieIn on 2017/8/25.
 * stb info.
 */
public class StbInfo {

    /**
     * <p>上下文对象.</p>
     */
    private Context mContext;

    /**
     * <p>包可见构造函数.</p>
     */
    /*package*/ StbInfo(@NonNull Context context) {
        mContext = checkNotNull(context, "client cannot be null");
    }

    public StbInfo() {
    }


    /**
     * <p>获取机顶盒唯一标识id.</p>
     * @return 唯一标识id
     */
    public String getUserCode() {
        return SystemInfoUtil.getStb_sn();
    }

    /**
     * <p>获取密码.</p>
     * @return 密码
     */
    public String getPassword() {
        return "jzmedia";
    }

    /**
     * <p>获取stb sn.</p>
     * @return stb sn
     */
    public String getSn() {
        return SystemInfoUtil.getStb_sn();
    }

    /**
     * <p>获取stb mac.</p>
     * <p>Android不推荐使用mac地址,并且从Android6.0开始获取mac都返回02:00:00:00:00:00</p>
     * @return stb mac
     */
    public String getMac() {
        return NetworkUtils.getMac();
    }

    /**
     * <p>获取stb platform.</p>
     * @return stb platform
     */
    public String getPlatform() {
        return SystemInfoUtil.getModel();
    }

    /**
     * <p>获取stb vendor.</p>
     * @return stb vendor
     */
    public String getVendor() {
        return SystemInfoUtil.getVendor();
    }

}
