package com.jzmedia.pbm.model.behavior;

/**
 * Created by ChristieIn on 2017/8/25.
 *
 */

public enum BehaviorAction {
    poweron("开机",	BehaviorType.SYS),//x
    poweroff("关机", BehaviorType.SYS),//x
    enter("进入",	BehaviorType.APP, BehaviorType.CONTENT, BehaviorType.PAGE),//y
    quit("退出", BehaviorType.APP, BehaviorType.CONTENT),//y
    install("安装", BehaviorType.APP),//y
    uninstall("卸载", BehaviorType.APP),//y
    upgrade("升级", BehaviorType.APP, BehaviorType.SYS),//y
    update("更新", BehaviorType.CONTENT),//x
    crash("崩溃", BehaviorType.APP),//y
    upload("上传	", BehaviorType.APP),
    order("订购",	BehaviorType.APP),//x
    cancel("取消", BehaviorType.APP),//x
    during("持续	", BehaviorType.CONTENT);//y

    private String description;
    private BehaviorType[] types;

    BehaviorAction(String descption, BehaviorType... types) {
        this.description = descption;
        this.types = types;
    }

    public String getDescription() {
        return description;
    }

    public BehaviorType[] getTypes() {
        return types;
    }
}
