package com.jzmedia.pbm.model.web;

import com.jzmedia.pbm.model.web.domain.AuthorizationPostBean;
import com.jzmedia.pbm.model.web.domain.AuthorizationSuccessBean;
import com.jzmedia.pbm.model.web.domain.CollectionsPostBean;
import com.jzmedia.pbm.model.web.domain.CollectionsSuccessBean;
import com.jzmedia.pbm.model.web.domain.ControlSuccessBean;
import com.jzmedia.pbm.model.web.domain.UpdateCfgBean;
import com.jzmedia.pbm.model.web.domain.FtpserversObtainSuccessBean;
import com.jzmedia.pbm.model.web.listener.WebClientListener;

import java.util.List;

/**
 * Created by ChristieIn on 2017/7/31.
 * web client连接状态基类.
 */
/*package*/ abstract class ClientState {

    /**
     * <p>认证.</p>
     * @param wc the instance of WebClient
     * @param data the post data to authorize
     */
    public abstract void authorize(WebClient wc, AuthorizationPostBean data,
                                   WebClientListener<AuthorizationSuccessBean> listener);

    /**
     * <p>取消认证.</p>
     * @param wc the instance of WebClient
     */
    public abstract void unauthorize(WebClient wc);

    public abstract void queryUpgradeCfg(WebClient wc, WebClientListener<UpdateCfgBean> listener);

    /**
     * <p>上报行为数据.</p>
     * @param wc the instance of WebClient
     * @param data 行为数据
     * @param listener listener
     */
    public abstract void reportBehavior(WebClient wc, List<CollectionsPostBean> data,
                                        WebClientListener<CollectionsSuccessBean> listener);

    /**
     * <p>查询上报控制.</p>
     * @param wc the instance of WebClient
     * @param listener listener
     */
    public abstract void queryUploadControl(WebClient wc,
                                            WebClientListener<ControlSuccessBean> listener);

    /**
     * <p>查询ftp servers.</p>
     * @param wc the instance of WebClient
     * @param listener listener
     */
    public abstract void queryFtpServers(WebClient wc,
                                         WebClientListener<FtpserversObtainSuccessBean> listener);

}
