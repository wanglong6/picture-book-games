package com.jzmedia.pbm.model.upgrade.bean;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by Leo on 2018/3/20.
 */
@Keep
public class AuthorBean {

    /**
     * code : 0
     * data : {"sessionToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtaWQiOiI1YTQwYTMzNTlhMDRmYjFhMmY2OGUyZTEiLCJ1c2VyaWQiOiI2Nzk1MDIxNzE2MDAwMDAxNyIsImlhdCI6MTUxNDE4NTg0MCwiZXhwIjoxNTE0MTg5NDQwfQ.nXPb5m5uJW5_Kph7oSnY0PNTCNo2PMU_RRRO_6tsKIM","sessionExpire":3600}
     */

    private int code;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }
    @Keep
    public static class DataBean {
        /**
         * sessionToken : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtaWQiOiI1YTQwYTMzNTlhMDRmYjFhMmY2OGUyZTEiLCJ1c2VyaWQiOiI2Nzk1MDIxNzE2MDAwMDAxNyIsImlhdCI6MTUxNDE4NTg0MCwiZXhwIjoxNTE0MTg5NDQwfQ.nXPb5m5uJW5_Kph7oSnY0PNTCNo2PMU_RRRO_6tsKIM
         * sessionExpire : 3600   //token过期时间, 单位秒钟
         */

        private String sessionToken;
        private int sessionExpire;

        public String getSessionToken() {
            return sessionToken;
        }

        public void setSessionToken(String sessionToken) {
            this.sessionToken = sessionToken;
        }

        public int getSessionExpire() {
            return sessionExpire;
        }

        public void setSessionExpire(int sessionExpire) {
            this.sessionExpire = sessionExpire;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "sessionToken='" + sessionToken + '\'' +
                    ", sessionExpire=" + sessionExpire +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "AuthorBean{" +
                "code=" + code +
                ", data=" + data +
                '}';
    }
}
