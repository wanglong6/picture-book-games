package com.jzmedia.pbm.model.web.domain;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by ChristieIn on 2018/3/9.
 * 认证post数据.
 */
@Keep
public class AuthorizationPostBean {

    /**
     * user : boatman
     * password : pwjfpewj
     * sn : XXXXXXXXXX
     * mac : AABBCCDDEE
     * platform: BABA
     */
    private String INIT_VAL = "";
    private String user = INIT_VAL;
    private String password = INIT_VAL;
    private String sn = INIT_VAL;
    private String mac = INIT_VAL;
    private String platform = INIT_VAL;
    private String vendor = INIT_VAL;
    private String from = INIT_VAL;
    private String key = INIT_VAL;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "AuthorizationPostBean{" +
                "INIT_VAL='" + INIT_VAL + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                ", sn='" + sn + '\'' +
                ", mac='" + mac + '\'' +
                ", platform='" + platform + '\'' +
                ", vendor='" + vendor + '\'' +
                ", from='" + from + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}
