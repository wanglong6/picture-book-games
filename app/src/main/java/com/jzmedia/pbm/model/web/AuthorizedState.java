package com.jzmedia.pbm.model.web;



import com.jzmedia.pbm.model.web.domain.AuthorizationPostBean;
import com.jzmedia.pbm.model.web.domain.AuthorizationSuccessBean;
import com.jzmedia.pbm.model.web.domain.CollectionsPostBean;
import com.jzmedia.pbm.model.web.domain.CollectionsSuccessBean;
import com.jzmedia.pbm.model.web.domain.ControlSuccessBean;
import com.jzmedia.pbm.model.web.domain.FtpserversObtainSuccessBean;
import com.jzmedia.pbm.model.web.domain.UpdateCfgBean;
import com.jzmedia.pbm.model.web.listener.WebClientListener;

import java.util.List;

import static com.jzmedia.common.util.LogUtils.makeLogTag;
import static com.jzmedia.common.util.LogUtils.LOGW;

/**
 * Created by ChristieIn on 2017/7/31.
 * 已认证状态.
 */
/*package*/ class AuthorizedState extends ClientState {
    /** 打印标签. */
    private static final String TAG = makeLogTag("AuthorizedState");

    /**
     * <p>隐藏构造函数.</p>
     */
    private AuthorizedState() {
    }

    /**
     * <p>线程安全的单例.</p>
     */
    private static class SingletonHolder {
        static AuthorizedState instance = new AuthorizedState();
    }

    /**
     * <p>获取单例.</p>
     * @return 单例
     */
    /*package*/ static AuthorizedState getInstance() {
        return SingletonHolder.instance;
    }

    @Override
    public void authorize(WebClient wc, AuthorizationPostBean data,
                          WebClientListener<AuthorizationSuccessBean> listener) {
        LOGW(TAG, "already authorized. reauthorize.");
        wc.doAuthorize(data, listener);
    }

    @Override
    public void unauthorize(WebClient wc) {
        wc.setState(UnAuthorizedState.getInstance());
    }

    @Override
    public void queryUpgradeCfg(WebClient wc, WebClientListener<UpdateCfgBean> listener) {
        wc.doQueryUpgradeCfg(listener);
    }

    @Override
    public void reportBehavior(WebClient wc, List<CollectionsPostBean> data, WebClientListener<CollectionsSuccessBean> listener) {
        wc.doReportBehavior(data, listener);
    }

    @Override
    public void queryUploadControl(WebClient wc, WebClientListener<ControlSuccessBean> listener) {
        wc.doQueryUploadControl(listener);
    }

    @Override
    public void queryFtpServers(WebClient wc,
                                WebClientListener<FtpserversObtainSuccessBean> listener) {
        wc.doQueryFtpServers(listener);
    }

}
