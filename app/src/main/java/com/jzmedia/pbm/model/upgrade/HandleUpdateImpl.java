package com.jzmedia.pbm.model.upgrade;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.jzmedia.pbm.contract.PBMContract;
import com.jzmedia.pbm.model.upgrade.bean.ApkBean;
import com.jzmedia.pbm.model.upgrade.bean.AuthorBean;
import com.jzmedia.pbm.model.upgrade.bean.UpgradeBean;
import com.jzmedia.pbm.model.web.domain.UpdateCfgBean;
import com.jzmedia.pbm.util.AppManagerUtil;
import com.jzmedia.pbm.util.FileUtil;
import com.jzmedia.pbm.util.HandleQueryResultImpl;
import com.jzmedia.pbm.util.NetworkUtils;
import com.jzmedia.pbm.util.UserActionUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class HandleUpdateImpl {
    private String TAG = "HandleUpdateImpl";
    private Context mContext;
    private static HandleUpdateImpl mhandleUpdateImpl;
    private HandleQueryResultImpl mHandleQueryImpl;
    private static UpdateHandle mHandler;
    /**
     * When request or post failed, it ++1.
     * Used for request or post again after 2,4,6,8,configure time minutes.
     */
    private int mNetworkConnectFailedCount = 0;
    private static final int QUERY_FAILED_MAX = 4;
    private int mQueryFailedCount = 0;
    private FileUtil mFileUtil;
    private AppManagerUtil mAppManagerUtil;
    /**本地token*/
    private String mToken;
    private long mOverdueTimeMs = 0;
    /**
     * 本地保存的升级配置文件.
     */
    private static UpdateCfgBean mLocalUpdateCfg;
    /**
     * 下载的升级配置文件.
     */
    private UpdateCfgBean mUpdateCfg;
    /**
     * 下载的固件、应用版本配置文件.
     */
    private List<UpgradeBean.DataBean> mAppCfgList;
    /**判断是否处于下载状态*/
    private boolean mIsDownloading = false;
    private int mDownloadRate = 0;
    private String mDownloadName;

    public static HandleUpdateImpl getInstance(){
        if(mhandleUpdateImpl == null){
            mhandleUpdateImpl = new HandleUpdateImpl();
        }
        return mhandleUpdateImpl;
    }
    /**
     * start update
     */
    public void startUpdate(boolean replaced){
        Log.d(TAG, "startUpdate: Enter.");
        mHandler = new UpdateHandle();
        mFileUtil = FileUtil.getInstance();
        mAppManagerUtil = AppManagerUtil.getInstance();
        mHandleQueryImpl = HandleQueryResultImpl.getInstance();
        mHandler.removeCallbacks(updateThread);
        mHandler.post(updateThread);

        if (replaced) {
            mHandler.sendEmptyMessageDelayed(PBMContract.UPGRADE_CLIENT_REPLACED, 3000);
        }
        Log.d(TAG, "startUpdate: Exit");
    }

    public void stopUpdate() {
        Log.d(TAG, "stopUpdate: Enter.");
        mHandler.removeCallbacks(updateThread);
        mUpdateCfg = null;
        mAppCfgList = null;
        mUpdateCfg = null;
    }

    /**
     * 该Thread只请求Token，下载配置文件。其它下载任务走Handler处理.
     *
     *
     */
    private Runnable updateThread = new Runnable() {
        public void run() {
            Log.d(TAG, "updateThread run.");
            //判断网络是否可用
            if (NetworkUtils.isNetworkConnected(mContext)) {
                //网络计数归0
                mNetworkConnectFailedCount = 0;
                if (mUpdateCfg  == null) {
                    Log.d(TAG, "updateThread: To download updateCfg file.");
                    if (0 != mHandleQueryImpl.downloadUpgradeCfgInfo(mContext, mHandler)) {
                        if (mQueryFailedCount < QUERY_FAILED_MAX) {
                            mHandler.postDelayed(updateThread, 2000);
                            mQueryFailedCount++;
                        }
                    } else {
                        mQueryFailedCount = 0;
                    }
                } else if (mAppCfgList == null){
                    Log.d(TAG, "updateThread: To download firm/apks version cfg file.");
                    if (mUpdateCfg != null) {
                        if (0 != mHandleQueryImpl.downloadFirmApksCfg(mContext, mHandler,
                                mUpdateCfg.getData().getConfigFilePath())) {
                            if (mQueryFailedCount < QUERY_FAILED_MAX) {
                                mHandler.postDelayed(updateThread, 2000);
                                mQueryFailedCount++;
                            }
                        } else {
                            mQueryFailedCount = 0;
                        }
                    }
                } else {
                    checkNeedInstall();
                }
            } else {
                //If network is unavailable, to start check network again.
                Log.d(TAG, "updateThread: Network is unavailable !!!");
            }
        }
    };
    public class UpdateHandle extends Handler {
        @Override
        public void handleMessage(final Message msg) {
            switch (msg.what) {
                case PBMContract.CHECK_WHETHER_NEED_INSTALL:
                    //Todo. check whether exist need download or install(firm or apk).
                    checkNeedInstall();
                    break;
                case PBMContract.TOKEN_REQUES_NOTIFY:
                    Log.d(TAG, "TOKEN_REQUES_NOTIFY.");
                    tokenRequestNotifyHandle(msg);
                    break;
                case PBMContract.DOWNLOAD_UPGRADE_CFG_NOTIFY:
                    //升级配置信息获取通知.
                    Log.d(TAG, "DOWNLOAD_UPGRADE_CFG_NOTIFY.");
                    upgradeCfgInfoDownloadNotifyHandle(msg);
                    break;
                case PBMContract.DOWNLOAD_FIRM_APP_CFG_NOTIFY:
                    //获取需要下载的文件信息 成功
                    Log.d(TAG, "DOWNLOAD_FIRM_APP_CFG_NOTIFY.");
                    apkCfgDownloadNotifyHandle(msg);
                    break;
                case PBMContract.FIRM_APP_DOWNLOAD_START:
                    UpgradeBean.DataBean bean = (UpgradeBean.DataBean)msg.obj;
                    mDownloadName = mFileUtil.getFileNameFromPath(bean.getPath());
                    mHandleQueryImpl.downLoadFile(mContext, mHandler,
                            bean.getPath(), bean.getId(), bean.getMd5());
                    break;
                case PBMContract.FIRM_APP_DOWNLOAD_FINISH:
                    Log.d(TAG, "DOWN_LOAD_FINISH.");
                    downloadFinishOne(msg);
                    break;
                case PBMContract.DOWNLOAD_FILE_CANCLE:
                    Log.d(TAG, "DOWNLOAD_FILE_CANCLE.");
                    mIsDownloading = false;
                    Intent downloadFinishIntent = new Intent(PBMContract.UPGRADE_REFRESH_STATUS);
                    mContext.sendBroadcast(downloadFinishIntent);
                    mHandler.removeCallbacks(updateThread);
                    mHandler.post(updateThread);
                    break;
                case PBMContract.DOWNLOAD_FILE_PROGRESS:
                    if (!mIsDownloading) {
                        mIsDownloading = true;
                    }
                    mDownloadRate = msg.arg1;
                    /*Intent intent = new Intent(PBMContract.DOWN_LOAD_PROGRESS);
                    intent.putExtra("rate",mDownloadRate);
                    intent.putExtra("name",mDownloadName);
                    mContext.sendBroadcast(intent);*/
                    break;

                case PBMContract.UPLOAD_VERSION_NOTIFY:
                    //应用版本信息上传成功
                    Log.d(TAG, "UPLOAD_VERSION_NOTIFY.");
                    if (msg.arg1 == PBMContract.UPLOAD_OK) {
                        Log.d(TAG, "UPLOAD_VERSION_NOTIFY: Upload version ok.");
                    } else {
                        Log.d(TAG, "UPLOAD_VERSION_NOTIFY: Upload version failed !!!");
                    }
                    break;
                case PBMContract.UPGRADE_CLIENT_REPLACED:
                    Log.d(TAG, "UPGRADE_CLIENT_REPLACED.");
                    mAppManagerUtil.deleteDownloadApk(mContext);
                    String uploadVerInfo = UserActionUtil.getUploadVerInfo(mContext, "OK");
                    if (0 != mHandleQueryImpl.uploadActionInfo(mContext, mHandler, uploadVerInfo)) {
                        if (mQueryFailedCount < QUERY_FAILED_MAX) {
                            mHandler.sendEmptyMessageDelayed(PBMContract.UPGRADE_CLIENT_REPLACED, 2000);
                            mQueryFailedCount++;
                        }
                    } else {
                        mQueryFailedCount = 0;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 下载完CFG文件，版本号没有变化，判断是否有下载未完成，或者是否有需要安装的。
     */
    private void checkNeedInstall() {
        Log.d(TAG, "checkNeedInstall.");

        //Check APK.
        ApkBean apkBean = mAppManagerUtil.getDownloadApk(mContext);
        if (apkBean == null) {
            Log.d(TAG, "checkNeedInstall: No download apk.");
            return;
        }
        int curVersion = getCurVersion(mContext);

        if (apkBean.getVersion() > curVersion) {
            Log.d(TAG, "checkNeedInstall: APK install now.");
            File apkFile = new File(apkBean.getApkPath());
            mAppManagerUtil.recommandInstallApk(mContext, apkFile);
            return;
        } else {
            Log.d(TAG, "checkNeedInstall: Out of version apk, now to delete.");
            File apkFile = new File(apkBean.getApkPath());
            apkFile.delete();
        }
        mHandler.post(updateThread);
        Log.d(TAG, "checkNeedInstall: No install task.");
    }

    private void tokenRequestNotifyHandle(final Message msg) {
        Log.d(TAG, "tokenRequestNotifyHandle.");
        if (msg.arg1 == PBMContract.DOWNLOAD_OK) {
            AuthorBean authorBean = (AuthorBean) msg.obj;
            if (authorBean.getCode() == 0) {
                mToken = authorBean.getData().getSessionToken();
                Log.d(TAG, "tokenRequestNotifyHandle: mToken-> " + mToken);
                mOverdueTimeMs = System.currentTimeMillis()
                        + ((authorBean.getData().getSessionExpire() - 5) * 1000);

                Log.d(TAG, "tokenRequestNotifyHandle: No local update cfg.");
                //Request OK, Reset failed count.
                mNetworkConnectFailedCount = 0;
                mHandler.post(updateThread);
            }
        } else {
            Log.d(TAG, "tokenRequestNotifyHandle: Request token failed !!!");
        }
    }

    /**
     * 处理升级配置文件下载通知.
     * @param msg
     */
    private void upgradeCfgInfoDownloadNotifyHandle(final Message msg) {
        Log.d(TAG, "upgradeCfgInfoDownloadNotifyHandle.");
        if (msg.arg1 == PBMContract.DOWNLOAD_OK) {
            mNetworkConnectFailedCount = 0;
            String response = (String) msg.obj;
            mUpdateCfg = new Gson().fromJson(response, UpdateCfgBean.class);
            UpdateCfgBean.DataBean dataBean = mUpdateCfg.getData();
            int cfgVer = dataBean.getConfigFileVer();
            if (mUpdateCfg.getCode() == 0) {
                if (mLocalUpdateCfg == null
                        || cfgVer > mLocalUpdateCfg.getData().getConfigFileVer()) {
                    mFileUtil.saveCfgFile(response, PBMContract.CONFIG_NAME);
                    mLocalUpdateCfg = mUpdateCfg;
                }
                Log.d(TAG, "upgradeCfgInfoDownloadNotifyHandle: " +
                        "Version is " + cfgVer);

                //不管版本是否有变化，启动后都要下载一次版本配置文件.
                mAppCfgList = null;
                mHandler.removeCallbacks(updateThread);
                mHandler.post(updateThread);
            }
        } else {
            Log.d(TAG, "upgradeCfgInfoDownloadNotifyHandle: Download cfg file failed !!!");
        }
    }

    /**
     * 处理固件和APP版本配置文件下载通知.
     * @param msg handler message.
     */
    private void apkCfgDownloadNotifyHandle(final Message msg) {
        Log.d(TAG, "firmAppsCfgDownloadNotifyHandle.");
        if (msg.arg1 == PBMContract.DOWNLOAD_OK) {
            mNetworkConnectFailedCount = 0;
            String responseDownLoad = (String) msg.obj;
            Log.d(TAG, " " + responseDownLoad);
            UpgradeBean datas = new Gson().fromJson(responseDownLoad, UpgradeBean.class);
            mAppCfgList = datas.getData();

            boolean isNeedDownload = checkNeedDownload(mAppCfgList.get(0));

            if (isNeedDownload) {
                mIsDownloading = true;
                Message downloadMsg = mHandler.obtainMessage();
                downloadMsg.what = PBMContract.FIRM_APP_DOWNLOAD_START;
                downloadMsg.obj = mAppCfgList.get(0);
                mHandler.sendMessage(downloadMsg);
            } else {
                Log.d(TAG, "firmAppsCfgDownloadNotifyHandle: No download.");
                checkNeedInstall();
            }
        } else {
            Log.d(TAG, "firmAppsCfgDownloadNotifyHandle: Failed !!!");
            mIsDownloading = false;
            //To do: Download failed handle.
            //networkFailedHandle();
        }
    }
    /**
     * 固件或APK，一个下载完成处理.
     * 先判断MD5是否OK，MD5一致，则继续下载下一个。
     * MD5校验不一致，再尝试一遍，还不行则跳过。
     * @param msg Message.
     */
    private void downloadFinishOne(final Message msg) {
        Log.d(TAG, "downloadFinishOne.");
        String appID = (String) msg.obj;
        if (msg.arg1 == PBMContract.DOWNLOAD_OK) {
            Log.d(TAG, "downloadFinishOne: Download complete: " + appID);
            mNetworkConnectFailedCount = 0;

            String apkPath = mContext.getExternalFilesDir(null).getAbsolutePath() + "/"
                    + mFileUtil.getFileNameFromPath(mAppCfgList.get(0).getPath());
            Log.d(TAG, "downloadFinishOne: To check MD5. " + apkPath);
            boolean md5Ok = mFileUtil.checkFileMD5(apkPath, mAppCfgList.get(0).getMd5());
            if (md5Ok) {
                Log.d(TAG, "downloadFinishOne: Download APK OK.");
                ApkBean apk = mAppManagerUtil.getDownloadApk(mContext);
                String path = apk.getApkPath();
                Log.d(TAG, "downloadFinishOne: " + path);
                File file = new File(path);
                mAppManagerUtil.recommandInstallApk(mContext, file);
            } else {
                Log.d(TAG, "downloadFinishOne: APK check MD5 failed !!!");
                mFileUtil.deleteFile(apkPath);
            }
        } else {
            Log.d(TAG, "downloadFinishOne: Download failed->" + appID);
            mIsDownloading = false;
        }
    }

    /**
     *
     * @param data
     * @return
     */
    private boolean checkNeedDownload(UpgradeBean.DataBean data) {
        if (data == null) {
            Log.d(TAG, "Param null or size is 0 !!!");
            return false;
        }

        int curVersion = getCurVersion(mContext);
        ApkBean alreadyDownloadApk = mAppManagerUtil.getDownloadApk(mContext);
        if (alreadyDownloadApk != null) {
            if (alreadyDownloadApk.getVersion() == Integer.parseInt(data.getV())) {
                return false;
            } else {
                File apkFile = new File(alreadyDownloadApk.getApkPath());
                apkFile.delete();
            }
        }
        if (Integer.parseInt(data.getV()) > curVersion) {
            return true;
        }
        return false;
    }

    public int getCurVersion(Context ctx) {
        int localVersion = 0;
        try {
            PackageInfo packageInfo = ctx.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            localVersion = packageInfo.versionCode;
            Log.d(TAG, "localVersion = " + localVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return localVersion;
    }
}
