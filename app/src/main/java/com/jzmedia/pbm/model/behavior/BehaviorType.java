package com.jzmedia.pbm.model.behavior;

/**
 * Created by ChristieIn on 2017/8/25.
 * behavior types.
 */

public enum BehaviorType {
    SYS,//系统级别的事件
    APP,//APP级别的事件
    CONTENT,//内容级别的事件(视频…)
    PAGE;//页面级别的事件
}
