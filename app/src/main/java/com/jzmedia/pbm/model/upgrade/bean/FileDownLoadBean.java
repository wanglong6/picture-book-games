package com.jzmedia.pbm.model.upgrade.bean;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by huangfen on 2017/12/23.
 */
@Keep
public class FileDownLoadBean {
    /**下载文件的名字*/
    private String fileName;
    /**下载任务标识*/
    private int tag;
    /**下载的进度*/
    private int rate;

    /**下载的任务数量*/
    private int taskCount;

    public FileDownLoadBean(String fileName, int tag, int rate, int taskCount) {
        this.fileName = fileName;
        this.tag = tag;
        this.rate = rate;
        this.taskCount = taskCount;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public int getTaskCount() {
        return taskCount;
    }

    public void setTaskCount(int taskCount) {
        this.taskCount = taskCount;
    }

    @Override
    public String toString() {
        return "FileDownLoadBean{" +
                "fileName='" + fileName + '\'' +
                ", tag=" + tag +
                ", rate=" + rate +
                ", taskCount=" + taskCount +
                '}';
    }
}
