package com.jzmedia.pbm.model.web.domain;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by ChristieIn on 2018/3/9.
 * base success bean.
 */
@Keep
public abstract class BaseSuccessBean<Data> {
    private int code;
    private Data data;
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BaseSuccessBean{" +
                "code=" + code +
                ", data=" + data +
                '}';
    }
}
