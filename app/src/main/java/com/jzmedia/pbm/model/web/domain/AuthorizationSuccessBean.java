package com.jzmedia.pbm.model.web.domain;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by ChristieIn on 2018/3/9.
 * 认证成功.
 */
@Keep
public class AuthorizationSuccessBean extends BaseSuccessBean<AuthorizationSuccessBean.DataBean> {

    @Keep
    public static class DataBean {

        /**
         * sessionToken : qmdj8pdidnmyzp0c7yqil91oc
         * sessionExpire : 3600
         */

        private String sessionToken;
        private int sessionExpire;

        public String getSessionToken() {
            return sessionToken;
        }

        public void setSessionToken(String sessionToken) {
            this.sessionToken = sessionToken;
        }

        public int getSessionExpire() {
            return sessionExpire;
        }

        public void setSessionExpire(int sessionExpire) {
            this.sessionExpire = sessionExpire;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "sessionToken='" + sessionToken + '\'' +
                    ", sessionExpire=" + sessionExpire +
                    '}';
        }
    }
}
