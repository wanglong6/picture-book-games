package com.jzmedia.pbm.model.web;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
//import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jzmedia.common.util.PackageUtils;
import com.jzmedia.common.volley.JObjectRequest;
import com.jzmedia.pbm.model.behavior.BehaviorHelper;
import com.jzmedia.pbm.model.web.domain.AuthorizationPostBean;
import com.jzmedia.pbm.model.web.domain.AuthorizationSuccessBean;
import com.jzmedia.pbm.model.web.domain.BaseSuccessBean;
import com.jzmedia.pbm.model.web.domain.CollectionsPostBean;
import com.jzmedia.pbm.model.web.domain.CollectionsSuccessBean;
import com.jzmedia.pbm.model.web.domain.ControlSuccessBean;
import com.jzmedia.pbm.model.web.domain.FailureBean;
import com.jzmedia.pbm.model.web.domain.FtpserversObtainSuccessBean;
import com.jzmedia.pbm.model.web.domain.UpdateCfgBean;
import com.jzmedia.pbm.model.web.domain.UpdateConfigParams;
import com.jzmedia.pbm.model.web.listener.WebClientListener;
import com.jzmedia.pbm.settings.SettingsUtils;
import com.jzmedia.pbm.util.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.LOGE;
import static com.jzmedia.common.util.LogUtils.LOGI;
import static com.jzmedia.common.util.LogUtils.makeLogTag;

import androidx.annotation.NonNull;

/**
 * Created by ChristieIn on 2017/7/31.
 * 服务端对接接口.
 */
public class WebClient {
    /** 打印标签. */
    private static final String TAG = makeLogTag("WebClient");
    /** 用于提示的上下文对象. */
    private Context mContext;
    /** 取消认证状态的计时handler. */
    private Handler mHandler;
    /* 请求队列. */
    private RequestQueue mQueue;
    /* json格式转化. */
    private Gson mGson;
    /** 状态. */
    private ClientState mState;
    /** access token. */
    private String mAccessToken;
    /** control params. */
    private ControlSuccessBean.DataBean mControlParams = null;
    /** stb info service. */
    private StbInfo mStbInfo;
    /** behavior helper. */
    private BehaviorHelper mBehaviorHelper;

    /**
     * <p>构造函数</p>
     * @param context context
     */
    public WebClient(@NonNull Context context) {
        mContext = checkNotNull(context, "context cannot be null");
        mQueue = Volley.newRequestQueue(context);
        mStbInfo = new StbInfo(context);
        mGson = new Gson();
        mHandler = new Handler(Looper.getMainLooper());
        mBehaviorHelper = new BehaviorHelper(mContext, this, mStbInfo);

        //初始化为未认证状态
        setState(UnAuthorizedState.getInstance());
    }

    /**
     * <p>设置状态.</p>
     * @param state 新状态
     */
    /*package*/ void setState(ClientState state) {
        mState = state;
    }

    /**
     * <p>认证.</p>
     * @param task task
     * @param listener listener
     */
    /*package*/ void authorize(final Runnable task, final WebClientListener listener) {
        mState.authorize(this, createAuthorizationPostData(),
                new WebClientListener<AuthorizationSuccessBean>() {

                    @Override
                    public void onSuccess(AuthorizationSuccessBean success) {
                        LOGI(TAG, "onSuccess" + success.toString());
                        //认证成功
                        setAccessToken(success.getData().getSessionToken());
                        setState(AuthorizedState.getInstance());
                        //启动反认证timer
                        if (mHandler != null) {
                            int delay = (int) ((success.getData().getSessionExpire() * 0.75) + 1) * 1000;
                            mHandler.removeCallbacks(mUnauthorizeRunnable);
                            mHandler.postDelayed(mUnauthorizeRunnable, delay);
                        }
                        //认证成功后的动作
                        if (task != null && !TextUtils.isEmpty(getAccessToken())) {
                            task.run();
                        }
                    }

                    @Override
                    public void onFailure(FailureBean failure) {
                        LOGI(TAG, "onFailure" + failure);
                        if (listener != null) {
                            listener.onFailure(failure);
                        }
                    }

                    @Override
                    public void onError(String error) {
                        LOGI(TAG, "onError");
                        if (listener != null) {
                            listener.onError(error);
                        }
                    }
        });
    }

    /**
     * <p>反认证.</p>
     */
    /*package*/ void unauthorize() {
        mState.unauthorize(this);
        mControlParams = null;
        mBehaviorHelper.setUnAuthorized();
    }

    public void queryUpgradeCfg(WebClientListener<UpdateCfgBean> listener) {
        mState.queryUpgradeCfg(this, listener);
    }

    /**
     * <p>上报数据</p>
     * @param behavior behavior
     */
    public void reportBehavior(final CollectionsPostBean behavior) {
        LOGD(TAG, "reportBehaviors() called with: behavior = [" + behavior + "]");
        if (behavior != null) {
            behavior.setSn(mStbInfo.getSn());
            behavior.setWho(mStbInfo.getUserCode());
            behavior.setMac(mStbInfo.getMac());
            behavior.setFrom(mContext.getPackageName());
            behavior.setTime(System.currentTimeMillis());
            behavior.setPlatform(mStbInfo.getPlatform());
            behavior.setVendor(mStbInfo.getVendor());
            if (NetworkUtils.isNetworkConnected(mContext)) {
                if (mControlParams == null) {
                    mState.queryUploadControl(this, new WebClientListener<ControlSuccessBean>() {
                        @Override
                        public void onSuccess(ControlSuccessBean success) {
                            mControlParams = success.getData();
                            if (mControlParams != null) {
                                reportBehavior(behavior);
                            } else {
                                mBehaviorHelper.saveBehavior(behavior);
                            }
                        }

                        @Override
                        public void onFailure(FailureBean failure) {
                            LOGD(TAG, "onFailure() called with: failure = [" + failure + "]");
                            mBehaviorHelper.saveBehavior(behavior);
                        }

                        @Override
                        public void onError(String error) {
                            LOGD(TAG, "onError() called with: error = [" + error + "]");
                            mBehaviorHelper.saveBehavior(behavior);
                        }
                    });
                } else {
                    mBehaviorHelper.reportBehavior(behavior, mControlParams);
                }
            } else {
                mBehaviorHelper.saveBehavior(behavior);
            }
        }
    }

    /**
     * <p>上报数据.除了BehaviorHelper,其他类不要使用这个接口.</p>
     * @param data 数据
     * @param listener listener
     */
    public void reportBehavior(List<CollectionsPostBean> data, final WebClientListener<CollectionsSuccessBean> listener) {
        LOGD(TAG, "reportBehavior() called with: data = [" + data + "], listener = [" + listener + "]");
        mState.reportBehavior(this, data, listener);
    }

    /**
     * <p>上报数据.</p>
     * @param listener listener
     */
    public void queryFtpServers(WebClientListener<FtpserversObtainSuccessBean> listener) {
        LOGD(TAG, "queryFtpServers() called with: listener = [" + listener + "]");
        mState.queryFtpServers(this, listener);
    }

    /**
     * <p>查询上报控制参数</p>
     * @param listener listener
     */
    /*package*/ void queryUploadControl(WebClientListener<ControlSuccessBean> listener) {
        mState.queryUploadControl(this, listener);
    }

    /**
     * <p>释放请求.</p>
     */
    public void release() {
        mQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
        mQueue.getCache().clear();
        unauthorize();

    }

    /**
     * <p>获取access token.</p>
     * @return access token
     */
    private String getAccessToken() {
        return mAccessToken;
    }

    /**
     * <p>设置access token.</p>
     * @param token access token
     */
    private void setAccessToken(String token) {
        mAccessToken = token;
    }

    /**
     * <p>到时间后取消授权.</p>
     */
    private Runnable mUnauthorizeRunnable = new Runnable() {
        @Override
        public void run() {
            unauthorize();
        }
    };

    /**
     * <p>执行正真的操作.</p>
     * @param url url
     * @param clazz 数据类型
     * @param headers 请求头
     * @param json json body
     * @param listener listener
     * @param <Success> 执行成功后的json数据结构对应的bean类型
     */
    private <Success extends BaseSuccessBean> void doAction(String url,
                                                            final Class<Success> clazz,
                                                            Map<String, String> headers,
                                                            Object json,
                                                            final WebClientListener<Success> listener) {
        LOGD(TAG, "doAction() called with: url = [" + url + "], clazz = [" + clazz + "], headers = ["
                + headers + "], json = [" + json + "], listener = [" + listener + "]");
        String jsonBody = json == null ? null : mGson.toJson(json);
        Request request = new JObjectRequest(url, headers, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LOGI(TAG, "onResponse" + response) ;
                        if (response != null) {
                            try {
                                int code = response.getInt(WebClientContract.RESPONSE_CODE_KEY);
                                if (listener != null) {
                                    if (code == WebClientContract.SUCCESS) {
                                        listener.onSuccess(mGson.fromJson(response.toString(), clazz));
                                    } else {
                                        //认证失败
                                        if (code == WebClientContract.WEB_RESULT_CODE_AUTHORIZE_FAILURE) {
                                            unauthorize();
                                        }
                                        listener.onFailure(mGson.fromJson(response.toString(), FailureBean.class));
                                    }
                                }
                            } catch (JSONException| JsonSyntaxException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    listener.onError("json exception");
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.onError("response is null");
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LOGE(TAG, "onError..");
                        if (listener != null) {
                            listener.onError(error == null ? "error" : error.getMessage());
                        }
                    }
                });
        mQueue.add(request);
    }

    /**
     * <p>认证.</p>
     * @param data 认证post数据
     * @param listener listener
     */
    /* package */ void doAuthorize(AuthorizationPostBean data,
                                   WebClientListener<AuthorizationSuccessBean> listener) {
        LOGD(TAG, "doAuthorize() called with: data = [" + data + "], listener = [" + listener + "]");
        doAction(WebClientContract.WEB_AUTHORIZATION_URL,
                AuthorizationSuccessBean.class,
                null,
                data,
                listener);
    }

    /**
     * <p>查询升级配置.</p>
     * @param listener listener
     */
    /* package */ void doQueryUpgradeCfg(WebClientListener<UpdateCfgBean> listener) {
        UpdateConfigParams cfgParams = new UpdateConfigParams();
        cfgParams.setTarget("ALL");
        cfgParams.setUser(mStbInfo.getUserCode());
        cfgParams.setSn(mStbInfo.getSn());
        cfgParams.setPlatform(mStbInfo.getPlatform());
        cfgParams.setMac(mStbInfo.getMac());
        cfgParams.setVendor(mStbInfo.getVendor());

        doAction(WebClientContract.WEB_UPDATE_CONFIG_URL + "?" + cfgParams.toParams(),
                UpdateCfgBean.class,
                getHeaders(mAccessToken),
                null,
                listener);
    }

    /**
     * <p>上报用户行为数据.</p>
     * @param data 行为数据
     * @param listener listener
     */
    /* package */ void doReportBehavior(List<CollectionsPostBean> data, WebClientListener<CollectionsSuccessBean> listener) {
        doAction(WebClientContract.WEB_COLLECTION_URL,
                CollectionsSuccessBean.class,
                getHeaders(mAccessToken),
                data,
                listener);
    }

    /**
     * <p>查询控制参数.</p>
     * @param listener listener
     */
    /* package */ void doQueryUploadControl(WebClientListener<ControlSuccessBean> listener) {
        doAction(WebClientContract.WEB_CONTROL_URL,
                ControlSuccessBean.class,
                getHeaders(mAccessToken),
                null,
                listener);
    }

    /**
     * <p>查询控制参数.</p>
     * @param listener listener
     */
    /* package */ void doQueryFtpServers(WebClientListener<FtpserversObtainSuccessBean> listener) {
        doAction(WebClientContract.WEB_FTP_SERVERS_URL,
                FtpserversObtainSuccessBean.class,
                getHeaders(mAccessToken),
                null,
                listener);
    }

    /**
     * <p>获取headers,包含token</p>
     * @param token access token
     * @return headers
     */
    private Map<String, String> getHeaders(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put(WebClientContract.WEB_HEADER_TOKEN_KEY, token);
        return headers;
    }

    /**
     * <p>获取认证post数据.</p>
     * @return post数据
     */
    private AuthorizationPostBean createAuthorizationPostData() {
        AuthorizationPostBean post = new AuthorizationPostBean();
        post.setUser(mStbInfo.getUserCode());
        post.setPassword(mStbInfo.getPassword());
        post.setSn(mStbInfo.getSn());
        post.setMac(mStbInfo.getMac());
        post.setPlatform(mStbInfo.getPlatform());
        post.setVendor(mStbInfo.getVendor());
        post.setFrom(mContext.getPackageName());
        post.setKey(SettingsUtils.getAppInstanceId(mContext));
        return post;
    }

}
