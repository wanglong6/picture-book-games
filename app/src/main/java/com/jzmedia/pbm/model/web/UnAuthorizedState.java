package com.jzmedia.pbm.model.web;

import com.jzmedia.pbm.model.web.domain.AuthorizationPostBean;
import com.jzmedia.pbm.model.web.domain.AuthorizationSuccessBean;
import com.jzmedia.pbm.model.web.domain.CollectionsPostBean;
import com.jzmedia.pbm.model.web.domain.CollectionsSuccessBean;
import com.jzmedia.pbm.model.web.domain.ControlSuccessBean;
import com.jzmedia.pbm.model.web.domain.FtpserversObtainSuccessBean;
import com.jzmedia.pbm.model.web.domain.UpdateCfgBean;
import com.jzmedia.pbm.model.web.listener.WebClientListener;

import java.util.List;

/**
 * Created by ChristieIn on 2017/7/31.
 * 未认证状态.
 */

/*package*/ class UnAuthorizedState extends ClientState {

    /**
     * <p>隐藏构造函数.</p>
     */
    private UnAuthorizedState() {
    }

    /**
     * <p>线程安全的单例.</p>
     */
    private static class SingletonHolder {
        static UnAuthorizedState instance = new UnAuthorizedState();
    }

    /**
     * <p>获取单例.</p>
     * @return 单例
     */
    /*package*/ static UnAuthorizedState getInstance() {
        return SingletonHolder.instance;
    }

    @Override
    public void authorize(WebClient wc, AuthorizationPostBean data,
                          WebClientListener<AuthorizationSuccessBean> listener) {
        wc.doAuthorize(data, listener);
    }

    @Override
    public void unauthorize(WebClient wc) {
        /* 未认证状态下，取消认证不需要做任何事. */
    }

    @Override
    public void queryUpgradeCfg(final WebClient wc, final WebClientListener<UpdateCfgBean> listener) {
        wc.authorize(new Runnable() {
            @Override
            public void run() {
                wc.queryUpgradeCfg(listener);
            }
        }, listener);
    }

    @Override
    public void reportBehavior(final WebClient wc, final List<CollectionsPostBean> data,
                               final WebClientListener<CollectionsSuccessBean> listener) {
        wc.authorize(new Runnable() {
            @Override
            public void run() {
                wc.reportBehavior(data, listener);
            }
        }, listener);
    }

    @Override
    public void queryUploadControl(final WebClient wc, final WebClientListener<ControlSuccessBean> listener) {
        wc.authorize(new Runnable() {
            @Override
            public void run() {
                wc.queryUploadControl(listener);
            }
        }, listener);
    }

    @Override
    public void queryFtpServers(final WebClient wc,
                                final WebClientListener<FtpserversObtainSuccessBean> listener) {
        wc.authorize(new Runnable() {
            @Override
            public void run() {
                wc.queryFtpServers(listener);
            }
        }, listener);
    }


}
