package com.jzmedia.pbm.model.upgrade.bean;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

import java.util.List;

/**
 * Created by huangfen on 2017/12/23.
 */
@Keep
public class UpgradeBean {
    /**
     * version : 1
     * datetime : 2017-12-20 08:57:46
     * data : [{"v":1,"id":"jzlx","path":"http://xxx.xxx.xxx.xxx:xx/xxx/newAsset.json","md5":"xxxxxxxxxxxx","flag":0}]
     */

    private int version;
    private String datetime;
    private List<DataBean> data;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }
    @Keep
    public static class DataBean {
        /**
         * target
         * v : 1
         * id : jzlx
         * path : http://xxx.xxx.xxx.xxx:xx/xxx/newAsset.json
         * md5 : xxxxxxxxxxxx
         * flag : 0     0：推荐升级。 1: 强制升级
         */

        private String v;
        private String id;
        private String path;
        private String md5;
        private int flag;
        private String target;

        public String getV() {
            return v;
        }

        public void setV(String v) {
            this.v = v;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getMd5() {
            return md5;
        }

        public void setMd5(String md5) {
            this.md5 = md5;
        }

        public int getFlag() {
            return flag;
        }

        public void setFlag(int flag) {
            this.flag = flag;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "v='" + v + '\'' +
                    ", id='" + id + '\'' +
                    ", path='" + path + '\'' +
                    ", md5='" + md5 + '\'' +
                    ", flag=" + flag +
                    ", target='" + target + '\'' +
                    '}';
        }
    }
    /***/
    @Override
    public String toString() {
        return "UpgradeBean{" +
                "version=" + version +
                ", datetime='" + datetime + '\'' +
                ", data=" + data +
                '}';
    }
}
