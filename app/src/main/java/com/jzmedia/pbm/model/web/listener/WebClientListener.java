package com.jzmedia.pbm.model.web.listener;


import com.jzmedia.pbm.model.web.domain.BaseSuccessBean;
import com.jzmedia.pbm.model.web.domain.FailureBean;

/**
 * Created by ChristieIn on 2017/7/31.
 * web client listener.
 */
public interface WebClientListener<SuccessBean extends BaseSuccessBean> {

    /**
     * <p>成功时回调函数.</p>
     * @param success 成功的json
     */
    void onSuccess(SuccessBean success);

    /**
     * <p>失败时的回调函数.这个失败是说网络接口调用成功，但是包含服务器返回的错误信息.</p>
     * @param failure 失败的json数据
     */
    void onFailure(FailureBean failure);

    /**
     * <p>web错误回调函数.不同于failure，这里可能是网络错误.</>
     * @param error 错误信息
     */
    void onError(String error);
}
