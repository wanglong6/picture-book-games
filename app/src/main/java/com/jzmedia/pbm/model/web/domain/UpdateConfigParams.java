package com.jzmedia.pbm.model.web.domain;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by ChristieIn on 2018/3/14.
 * update config params.
 */
@Keep
public class UpdateConfigParams {
    private String target;
    private String user;
    private String sn;
    private String mac;
    private String platform;
    private String vendor;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * <p>post transform to get.</p>
     * @return params
     */
    public String toParams() {
        String params = "target=" + (target == null ? "" : target);
        params += "&user=" + (user == null ? "" : user);
        params += "&sn=" + (sn == null ? "" : sn);
        params += "&mac=" + (mac == null ? "" : mac);
        params += "&platform=" + (platform == null ? "" : platform);
        params += "&vendor=" + (vendor == null ? "" : vendor);
        return params;
    }

}
