package com.jzmedia.pbm.model.web;

import com.jzmedia.pbm.contract.PBMContract;

/**
 * Created by ChristieIn on 2017/8/1.
 * web client contract.
 */
public class WebClientContract {
    /** 业务支撑返回码key. */
    /*package*/ static final String RESPONSE_CODE_KEY = "code";
    /** 业务支撑返回成功. */
    /*package*/ static final int SUCCESS = 0;//成功
    /** 业务支撑api version. */
    private static final String REPORT_APIVER = "1.0";
    /** report base url. */
    private static final String WEB_BASE_URL = PBMContract.PBM_SERVER_IP + ":"
            + PBMContract.PBM_SERVER_PORT + "/api/" + REPORT_APIVER;
    /** header token key. */
    /*package*/ static final String WEB_HEADER_TOKEN_KEY = "x-access-token";
    /** 认证地址. */
    /*package*/ static final String WEB_AUTHORIZATION_URL = WEB_BASE_URL + "/stb/authorization";
    /** 数据收集地址. */
    /*package*/ static final String WEB_COLLECTION_URL = WEB_BASE_URL + "/stb/collections";
    /** ftp服务器. */
    /*package*/ static final String WEB_FTP_SERVERS_URL = WEB_BASE_URL + "/stb/ftpservers";
    /** 控制参数. */
    /*package*/ static final String WEB_CONTROL_URL = WEB_BASE_URL + "/stb/control";
    /**版本查询接口：*/
    /*package*/ static final String WEB_UPDATE_CONFIG_URL = WEB_BASE_URL + "/stb/config/update";

    /** 单条上传. */
    public static final String CONTROL_MODE_SINGLE = "single";
    /** 批量上传. */
    public static final String CONTROL_MODE_PATCH = "patch";
    /** 数据库上传. */
    public static final String CONTROL_MODE_PACKAGE = "package";

    public static final int WEB_RESULT_CODE_MODIFY_FAILURE = 1000;//修改失败
    public static final int WEB_RESULT_CODE_DELETE_FAILURE = 1001;//删除失败
    public static final int WEB_RESULT_CODE_REPORT_FAILURE = 1002;//报告失败
    public static final int WEB_RESULT_CODE_REGISTE_FAILURE = 2000;//注册失败
    public static final int WEB_RESULT_CODE_REPEAT_REGISTE = 2001;//重复注册
    public static final int WEB_RESULT_CODE_AUTHORIZE_FAILURE = 2002;//认证失败
    public static final int WEB_RESULT_CODE_BIND_FAILURE = 2003;//绑定失败
    public static final int WEB_RESULT_CODE_ACTIVATE_FAILURE = 2004;//激活失败
    public static final int WEB_RESULT_CODE_REPEAT_ACTIVATE = 2005;//重复激活
    public static final int WEB_RESULT_CODE_ADD_FAILURE = 3000;//添加失败
    public static final int WEB_RESULT_CODE_REPEAT_ADD = 3001;//重复添加
    public static final int WEB_RESULT_CODE_ORDER_FAILURE = 3002;//订购失败
    public static final int WEB_RESULT_CODE_REPEAT_ORDER = 3003;//重复订购
    public static final int WEB_RESULT_CODE_OBTAIN_FAILURE = 4000;//获取失败
    public static final int WEB_RESULT_CODE_UNAUTHORIZED = 4444;//用户未授权
    public static final int WEB_RESULT_CODE_PRODUCT_NOT_ACTIVATED = 4445;//产品未激活
    public static final int WEB_RESULT_CODE_APP_NOT_SUPPORT = 4446;//应用不支持
    public static final int WEB_RESULT_CODE_SERVICE_NOT_STARTED = 4447;//服务未开启
    public static final int WEB_RESULT_CODE_VERSION_UPDATE = 5000;//版本有更新
    public static final int WEB_RESULT_CODE_UPGRADE_NOT_FINISH = 5001;//升级未完成
    public static final int WEB_RESULT_CODE_PARAM_ERROR = 5555;//参数有异常
    public static final int WEB_RESULT_CODE_SERVICE_ERROR = 6000;//服务异常
    public static final int WEB_RESULT_CODE_INTEFACE_NOT_SUPPORT = 8181;//接口不支持
    public static final int WEB_RESULT_CODE_REQUEST_TOO_OFTEN = 8384;//访问太频繁

}
