package com.jzmedia.pbm.model.web.domain;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by ChristieIn on 2018/3/14.
 * update config bean.
 */
@Keep
public class UpdateCfgBean extends BaseSuccessBean<UpdateCfgBean.DataBean> {

    @Keep
    public static class DataBean {
        /**
         * updateTarget : ALL
         * configFileVer : 1   //与update.json的内部的版本号 version一致
         * configFilePath : http://update.jzmediatech.com/update.json
         * encryptFlag : 0   //配置文件是否加密， 0 :未加密； >1 :已加密（不同数字代表不同加密方法，待定）。
         * updateFlag : 1  //是否忽略configFileVer， 0 :否； 1 :是，不管版本号直接下载update.json。
         * cycleMinutes : 0  //下一轮本接口调用间隔周期，单位分钟，设为 -1 表示暂停调用；0 表示按终端默认周期调用；>0 按此设定的周期调用。
         */
        private String updateTarget;
        private int configFileVer;
        private String configFilePath;
        private int encryptFlag;
        private int updateFlag;
        private int cycleMinutes;

        public String getUpdateTarget() {
            return updateTarget;
        }

        public void setUpdateTarget(String updateTarget) {
            this.updateTarget = updateTarget;
        }

        public int getConfigFileVer() {
            return configFileVer;
        }

        public void setConfigFileVer(int configFileVer) {
            this.configFileVer = configFileVer;
        }

        public String getConfigFilePath() {
            return configFilePath;
        }

        public void setConfigFilePath(String configFilePath) {
            this.configFilePath = configFilePath;
        }

        public int getEncryptFlag() {
            return encryptFlag;
        }

        public void setEncryptFlag(int encryptFlag) {
            this.encryptFlag = encryptFlag;
        }

        public int getUpdateFlag() {
            return updateFlag;
        }

        public void setUpdateFlag(int updateFlag) {
            this.updateFlag = updateFlag;
        }

        public int getCycleMinutes() {
            return cycleMinutes;
        }

        public void setCycleMinutes(int cycleMinutes) {
            this.cycleMinutes = cycleMinutes;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "updateTarget='" + updateTarget + '\'' +
                    ", configFileVer=" + configFileVer +
                    ", configFilePath='" + configFilePath + '\'' +
                    ", encryptFlag=" + encryptFlag +
                    ", updateFlag=" + updateFlag +
                    ", cycleMinutes=" + cycleMinutes +
                    '}';
        }
    }
}
