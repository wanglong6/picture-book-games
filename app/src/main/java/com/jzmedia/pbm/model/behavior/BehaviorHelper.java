package com.jzmedia.pbm.model.behavior;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
//import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.jzmedia.pbm.database.BehaviorContract.DataCollectionColumns;
import com.jzmedia.pbm.database.BehaviorDatabase;
import com.jzmedia.pbm.database.BehaviorDatabase.Tables;
import com.jzmedia.pbm.model.web.StbInfo;
import com.jzmedia.pbm.model.web.WebClient;
import com.jzmedia.pbm.model.web.WebClientContract;
import com.jzmedia.pbm.model.web.domain.CollectionsPostBean;
import com.jzmedia.pbm.model.web.domain.CollectionsSuccessBean;
import com.jzmedia.pbm.model.web.domain.ControlSuccessBean;
import com.jzmedia.pbm.model.web.domain.FailureBean;
import com.jzmedia.pbm.model.web.domain.FtpserversObtainSuccessBean;
import com.jzmedia.pbm.model.web.listener.WebClientListener;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.LOGE;
import static com.jzmedia.common.util.LogUtils.makeLogTag;

import androidx.annotation.NonNull;

/**
 * Created by ChristieIn on 2018/3/28.
 * behavior helper.
 */
public class BehaviorHelper {
    /** 打印标签. */
    private static final String TAG = makeLogTag("BehaviorHelper");
    /** 工作线程名称. */
    private static final String WORK_THREAD_NAME = "behavior_work_thread";
    /** 数据库保存的最大数据条数. */
    private static final int MAX_BEHAVIOR_COUNT = 10000;
    /** 数据统计文件上传文件名format. */
    private static final String BEHAVIOR_DB_UP_FN_FORMAT = "DB-%s-%s-%d.db";
    /** 取出数据库中的数据上传,最大的上传次数,如果大于这个限制则使用文件上传. */
    private static final int DB_BEHAVIOR_MAX_REPORT_COUNT = 20;
    /** work handler. */
    private Handler mWorkHandler;
    /** work thread. */
    private HandlerThread mWorkThread;
    /** 数据库. */
    private SQLiteOpenHelper mBehaviorOpenHelper;
    /** context. */
    private Context mContext;
    /** webclient. */
    private WebClient mWebClient;
    private StbInfo mStbInfo;
    /** 数据统计,多条提交. */
    private List<CollectionsPostBean> mPatchBehaviors;
    /** 最后一次提交的时间. */
    private long mLastReportTime;
    /** ftp servers. */
    private FtpserversObtainSuccessBean.DataBean mFtpServer;

    /**
     * <p>构造函数.</p>
     */
    public BehaviorHelper(@NonNull Context context, @NonNull WebClient wc, @NonNull StbInfo info) {
        mContext = checkNotNull(context, "context cannot be null");
        mWebClient = checkNotNull(wc, "wc cannot be null");
        mStbInfo = checkNotNull(info, "info cannot be null");
        mBehaviorOpenHelper = new BehaviorDatabase(context);
        mPatchBehaviors = new ArrayList<>();
        mLastReportTime = System.currentTimeMillis();
        //启动工作线程
        startWorkThread();
    }

    /**
     * <p>webclient unaurhorize时,设置</p>
     */
    public void setUnAuthorized() {
        mFtpServer = null;
    }

    /**
     * <p>上报数据</p>
     * @param behavior 数据
     */
    public void reportBehavior(final CollectionsPostBean behavior, final ControlSuccessBean.DataBean control) {
        if (control != null) {
            if (control.isEnable()) {
                if (TextUtils.equals(control.getMode(),
                        WebClientContract.CONTROL_MODE_SINGLE)) {
                    mWorkHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            //上报已有数据
                            reportBehaviorInDb(1);
                            //上报当前数据
                            reportSingleBehavior(behavior);
                        }
                    });
                } else if (TextUtils.equals(control.getMode(),
                        WebClientContract.CONTROL_MODE_PATCH)) {
                    mWorkHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            //上报已有数据
                            reportBehaviorInDb(control.getNumber());

                            //增加当前数据到记录中
                            mPatchBehaviors.add(behavior);
                            long nextReportTime = mLastReportTime + span(control);
                            if (mPatchBehaviors.size() >= control.getNumber()
                                    || System.currentTimeMillis() >= nextReportTime) {
                                //上报数据
                                mWorkHandler.removeCallbacks(mPatchBehaviorsRunnable);
                                reportPatchBehaviors();
                            } else {
                                //下次上报数据
                                mWorkHandler.removeCallbacks(mPatchBehaviorsRunnable);
                                mWorkHandler.postAtTime(mPatchBehaviorsRunnable, nextReportTime);
                            }
                        }
                    });
                } else if (TextUtils.equals(control.getMode(),
                        WebClientContract.CONTROL_MODE_PACKAGE)) {
                    mWorkHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            saveBehaviorInWorkThread(behavior);
                            int count = getBehaviorCount();
                            long nextReportTime = mLastReportTime + span(control);
                            if (count >= control.getNumber()
                                    || System.currentTimeMillis() >= nextReportTime) {
                                //上报数据
                                mWorkHandler.removeCallbacks(mPackageBehaviorsRunnable);
                                reportPackageBehaviors();
                            } else {
                                //下次上报数据
                                mWorkHandler.removeCallbacks(mPackageBehaviorsRunnable);
                                mWorkHandler.postAtTime(mPackageBehaviorsRunnable, nextReportTime);
                            }
                        }
                    });
                }
            } else {
                saveBehavior(behavior);
            }
        } else {
            saveBehavior(behavior);
        }
    }

    /**
     * <p>保存到数据库</p>
     * @param behavior 数据
     */
    public void saveBehavior(final CollectionsPostBean behavior) {
        mWorkHandler.post(new Runnable() {
            @Override
            public void run() {
                saveBehaviorInWorkThread(behavior);
            }
        });
    }

    /**
     * <p>关闭工作线程</p>
     */
    public void release() {
        mBehaviorOpenHelper.close();
        stopWorkThread();
    }

    /**
     * <p>打开工作线程.</p>
     */
    private void startWorkThread() {
        /* 如果工作线程已经打开，则先关闭后再新建工作线程 */
        stopWorkThread();
        mWorkThread = new HandlerThread(WORK_THREAD_NAME);
        mWorkThread.start();
        mWorkHandler = new Handler(mWorkThread.getLooper());
    }

    /**
     * <p>关闭工作线程.</p>
     */
    private void stopWorkThread() {
        if (mWorkThread != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mWorkThread.quitSafely();
            } else {
                mWorkThread.quit();
            }
            mWorkThread = null;
            mWorkHandler = null;
        }
    }

    /**
     * <p>设置ftp server</p>
     * @param server server
     */
    private synchronized void setFtpServer(FtpserversObtainSuccessBean.DataBean server) {
        mFtpServer = server;
    }

    /**
     * <p>获取ftp server</p>
     * @return ftp server
     */
    private synchronized FtpserversObtainSuccessBean.DataBean getFtpServer() {
        return mFtpServer;
    }



    /**
     * <p>上报时间间隔,单位毫秒</p>
     * @param control control
     * @return 时间间隔
     */
    private int span(ControlSuccessBean.DataBean control) {
        int span = control.getCycle() * 60 * 1000;
        if (span < 0) {
            return 0;
        } else {
            return span;
        }
    }

    /**
     * <p>上报single数据</p>
     * @param behavior 数据
     */
    private void reportSingleBehavior(CollectionsPostBean behavior) {
        List<CollectionsPostBean> data = new ArrayList<>(1);
        data.add(behavior);
        reportBehavior(data);
    }

    /**
     * <p>上报patch数据.</p>
     */
    private Runnable mPatchBehaviorsRunnable = new Runnable() {
        @Override
        public void run() {
            LOGD(TAG, "mPatchBehaviorsRunnable: run() called");
            reportPatchBehaviors();
        }
    };

    /**
     * <p>上报patch数据</p>
     */
    private void reportPatchBehaviors() {
        LOGD(TAG, "reportPatchBehaviors() called");
        List<CollectionsPostBean> data = new ArrayList<>(mPatchBehaviors.size());
        Collections.copy(data, mPatchBehaviors);
        mPatchBehaviors.clear();
        reportBehavior(data);
    }

    /**
     * <p>上报package数据.</p>
     */
    private Runnable mPackageBehaviorsRunnable = new Runnable() {
        @Override
        public void run() {
            LOGD(TAG, "mPackageBehaviorsRunnable: run() called");
            reportPackageBehaviors();
        }
    };

    /**
     * <p>上报package数据</p>
     */
    private void reportPackageBehaviors() {
        LOGD(TAG, "reportPackageBehaviors() called");
        if (getFtpServer() == null) {
            mWebClient.queryFtpServers(new WebClientListener<FtpserversObtainSuccessBean>() {
                @Override
                public void onSuccess(FtpserversObtainSuccessBean success) {
                    LOGD(TAG, "onSuccess() called with: success = [" + success + "]");
                    if (success != null
                            && success.getData() != null
                            && success.getData().size() > 0) {
                        setFtpServer(success.getData().get(0));
                        if (getFtpServer() != null) {
                            mWorkHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    reportPackageBehaviors();
                                }
                            });
                        }
                    }
                }

                @Override
                public void onFailure(FailureBean failure) {
                    LOGD(TAG, "onFailure() called with: failure = [" + failure + "]");
                }

                @Override
                public void onError(String error) {
                    LOGD(TAG, "onError() called with: error = [" + error + "]");
                }
            });
        } else {
            uploadFile(getFtpServer());
        }
    }

    /**
     * <p>上报数据库文件中的数据</p>
     * @param max 一次最多上报的数据条数.
     */
    private void reportBehaviorInDb(int max) {
        int count = getBehaviorCount();
        if (count <= max * DB_BEHAVIOR_MAX_REPORT_COUNT) {
            reportBehaviorFromDb(max);
        } else {
            //文件上传
            reportPackageBehaviors();
        }
    }

    /**
     * <p>上报已有数据</p>
     * @param max 每次最多上报数据条数
     */
    private void reportBehaviorFromDb(int max) {
        LOGD(TAG, "reportBehaviorInDb() called with: max = [" + max + "]");
        SQLiteDatabase db = mBehaviorOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(Tables.DATACOLLECTION, null, null, null, null, null, null);
        List<CollectionsPostBean> behaviors = createBehaviors(cursor);
        int fromIndex = 0;
        int toIndex = fromIndex + max;

        while (fromIndex < behaviors.size() && max > 0) {
            toIndex = behaviors.size() > toIndex ? toIndex : behaviors.size();
            List<CollectionsPostBean> data = behaviors.subList(fromIndex, toIndex);
            fromIndex = toIndex;
            toIndex = fromIndex + max;
            doReportBehaviors(data);
            //连续上报数据时,需要间隔一小段时间
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mLastReportTime = System.currentTimeMillis();
        }
    }

    /**
     * <p>通过client接口上报数据</p>
     * @param data data
     */
    private void doReportBehaviors(final List<CollectionsPostBean> data) {
        //上报数据库文件中的数据
        mWebClient.reportBehavior(data, new WebClientListener<CollectionsSuccessBean>() {
            @Override
            public void onSuccess(CollectionsSuccessBean success) {
                LOGD(TAG, "onSuccess() called with: success = [" + success + "]");
                deleteBehaviorsFromDb(data);
            }

            @Override
            public void onFailure(FailureBean failure) {
                LOGD(TAG, "onFailure() called with: failure = [" + failure + "]");
            }

            @Override
            public void onError(String error) {
                LOGD(TAG, "onError() called with: error = [" + error + "]");
            }
        });
    }

    /**
     * <p>生成上报data</p>
     * @param cursor cursor
     * @return post数据
     */
    private List<CollectionsPostBean> createBehaviors(Cursor cursor) {
        List<CollectionsPostBean> data = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                CollectionsPostBean behavior = new CollectionsPostBean();
                behavior.setRowId(cursor.getLong(cursor.getColumnIndexOrThrow(DataCollectionColumns._ID)));
                behavior.setType(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.TYPE)));
                behavior.setAction(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.ACTION)));
                behavior.setSn(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.SN)));
                behavior.setMac(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.MAC)));
                behavior.setFrom(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.FROM)));
                behavior.setTime(cursor.getLong(cursor.getColumnIndexOrThrow(DataCollectionColumns.TIME)));
                behavior.setPlatform(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.PLATFORM)));
                behavior.setVendor(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.VENDOR)));
                CollectionsPostBean.ObjectBean obj = new CollectionsPostBean.ObjectBean();
                obj.setId(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.OBJ_ID)));
                obj.setName(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.OBJ_NAME)));
                obj.setTypeid(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.OBJ_TYPE_ID)));
                obj.setCpid(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.OBJ_CP_ID)));
                obj.setData1(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.OBJ_DATA1)));
                obj.setData2(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.OBJ_DATA2)));
                obj.setData3(cursor.getString(cursor.getColumnIndexOrThrow(DataCollectionColumns.OBJ_DATA3)));
                behavior.setObject(obj);
                data.add(behavior);
            }
        }
        return data;
    }

    /**
     * <p>上报数据.上报失败后将记录添加到数据库中</p>
     * @param data 数据
     */
    private void reportBehavior(final List<CollectionsPostBean> data) {
        LOGD(TAG, "reportBehavior() called with: data = [" + data + "]");
        mWebClient.reportBehavior(data, new WebClientListener<CollectionsSuccessBean>() {
            @Override
            public void onSuccess(CollectionsSuccessBean success) {
                LOGD(TAG, "onSuccess() called with: success = [" + success + "]");
            }

            @Override
            public void onFailure(FailureBean failure) {
                LOGD(TAG, "onFailure() called with: failure = [" + failure + "]");
                saveBehaviorsInWorkThread(data);
            }

            @Override
            public void onError(String error) {
                LOGD(TAG, "onError() called with: error = [" + error + "]");
                saveBehaviorsInWorkThread(data);
            }
        });
        mLastReportTime = System.currentTimeMillis();
    }

    /**
     * <p>上传数据文件</p>
     * @param server ftp server
     */
    public void uploadFile(FtpserversObtainSuccessBean.DataBean server) {
        File behaviorDbFile = mContext.getDatabasePath(mBehaviorOpenHelper.getDatabaseName());
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(behaviorDbFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //进行上传的数据库名字的格式化
        String upFileName = String.format(Locale.getDefault(),
                BEHAVIOR_DB_UP_FN_FORMAT,
                mStbInfo.getSn(),
                mStbInfo.getMac().replaceAll(":", ""),
                System.currentTimeMillis());


        //进行上传
        boolean result = upload(
                server.getIp(),
                server.getPort(),
                server.getUsername(),
                server.getPassword(),
                "/home/sjsj",
                upFileName,
                fis);
        if (result) {//上传成功
            deleteAllBehaviorsFromDb();//清除表的数据
            //发请求告诉服务器，数据库已经上传到了ftp上
            CollectionsPostBean behavior = new CollectionsPostBean();
            behavior.setType(BehaviorType.SYS.name());
            behavior.setAction(BehaviorAction.upload.name());
            behavior.setSn(mStbInfo.getSn());
            behavior.setMac(mStbInfo.getMac());
            behavior.setWho(mStbInfo.getUserCode());
            behavior.setFrom(mContext.getPackageName());
            behavior.setTime(System.currentTimeMillis());
            behavior.setPlatform(mStbInfo.getPlatform());
            behavior.setVendor(mStbInfo.getVendor());
            CollectionsPostBean.ObjectBean obj = new CollectionsPostBean.ObjectBean();
            behavior.setObject(obj);
            obj.setId(mStbInfo.getSn());
            obj.setName("上传文件");
            obj.setData1(upFileName);
            obj.setData2(server.getIp() + ":" + server.getPort());
            obj.setData3(server.getProtocol());
            reportSingleBehavior(behavior);
        }
    }

    /**
     * Description: 向FTP服务器上传文件
     *
     * @param ip FTP服务器hostname
     * @param port FTP服务器端口
     * @param username FTP登录账号
     * @param password FTP登录密码
     * @param path FTP服务器保存目录
     * @param fileName 上传到FTP服务器上的文件名
     * @param is 输入流
     * @return 成功返回true，否则返回false
     */
    public boolean upload(String ip,int port, String username,String password, String path,
                                    String fileName,InputStream is){
        FTPClient client = new FTPClient();
        boolean success = false;
        try {
            client.connect(ip, port);
            boolean loginResult = client.login(username, password);//登录结果
            int replyCode = client.getReplyCode();
            if (loginResult && FTPReply.isPositiveCompletion(replyCode)){//登录成功
                LOGD(TAG, "ftpUpload:登录成功");
                //设置上传目录
                client.changeWorkingDirectory(path);
                client.setBufferSize(1024);
                client.setControlEncoding("UTF-8");
                client.enterLocalPassiveMode();
                client.setFileType(FTPClient.BINARY_FILE_TYPE);//添加这一句使得上传的文件不会受损
                client.storeFile(fileName, is);
                client.logout();
                success = true;//上传成功
                LOGD(TAG, "ftpUpload:上传成功");
            }
        } catch (SocketException e) {
            e.printStackTrace();
            LOGE(TAG, "ftpUpload: SocketException");
        } catch (IOException e) {
            e.printStackTrace();
            LOGE(TAG, "ftpUpload: IOException");
        }finally {
            try {
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
                LOGD(TAG, "ftpUpload:关闭ftp连接发生异常");
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return success;
    }

    /**
     * <p>创建content values用于保存数据.</p>
     * @param behavior 数据
     * @return content values
     */
    private ContentValues createContentValues(CollectionsPostBean behavior) {
        LOGD(TAG, "createContentValues() called with: behavior = [" + behavior + "]");
        ContentValues values = new ContentValues();
        values.put(DataCollectionColumns.TYPE, behavior.getType());
        values.put(DataCollectionColumns.ACTION, behavior.getAction());
        values.put(DataCollectionColumns.SN, behavior.getSn());
        values.put(DataCollectionColumns.MAC, behavior.getMac());
        values.put(DataCollectionColumns.WHO, behavior.getWho());
        values.put(DataCollectionColumns.PLATFORM, behavior.getPlatform());
        values.put(DataCollectionColumns.VENDOR, behavior.getVendor());
        values.put("[" + DataCollectionColumns.FROM + "]", behavior.getFrom());
        values.put(DataCollectionColumns.TIME, behavior.getTime());
        values.put(DataCollectionColumns.OBJ_ID, behavior.getObject().getId());
        values.put(DataCollectionColumns.OBJ_NAME, behavior.getObject().getName());
        values.put(DataCollectionColumns.OBJ_TYPE_ID, behavior.getObject().getTypeid());
        values.put(DataCollectionColumns.OBJ_CP_ID, behavior.getObject().getCpid());
        values.put(DataCollectionColumns.OBJ_DATA1, behavior.getObject().getData1());
        values.put(DataCollectionColumns.OBJ_DATA2, behavior.getObject().getData2());
        values.put(DataCollectionColumns.OBJ_DATA3, behavior.getObject().getData3());
        return values;
    }

    /**
     * <p>保存到数据库</p>
     * @param behaviors 数据
     */
    private void saveBehaviorsInWorkThread(List<CollectionsPostBean> behaviors) {
        LOGD(TAG, "saveBehaviorsInWorkThread() called with: behaviors = [" + behaviors + "]");
        if (behaviors != null && behaviors.size() > 0) {
            SQLiteDatabase db = null;
            try {
                db = mBehaviorOpenHelper.getWritableDatabase();
                db.beginTransaction();
                for (CollectionsPostBean behavior: behaviors) {
                    db.insert(Tables.DATACOLLECTION, null, createContentValues(behavior));
                }
            } catch (IllegalStateException ise) {
                ise.printStackTrace();
            } finally {
                if (db != null) {
                    db.endTransaction();
                }
            }
        }

        deleteRedundantBehaviors();
    }

    /**
     * <p>保存到数据库</p>
     * @param behavior 数据
     */
    private void saveBehaviorInWorkThread(CollectionsPostBean behavior) {
        LOGD(TAG, "saveBehavior2Db() called");
        if (behavior != null) {
            try {
                SQLiteDatabase db = mBehaviorOpenHelper.getWritableDatabase();
                long rowId = db.insert(BehaviorDatabase.Tables.DATACOLLECTION, null,
                        createContentValues(behavior));
                LOGD(TAG, "saveBehavior2Db: rowId = " + rowId);
            } catch (IllegalStateException ise) {
                ise.printStackTrace();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        deleteRedundantBehaviors();
    }

    /**
     * <p>数据超过最大条数时,删除最开始的记录。</p>
     */
    private void deleteRedundantBehaviors() {
        long count = getBehaviorCount();
        if (count > MAX_BEHAVIOR_COUNT) {
            SQLiteDatabase db = mBehaviorOpenHelper.getWritableDatabase();
            String sql = String.format(Locale.getDefault(),
                    "DELETE FROM [%s] WHERE [%s] IN (SELECT [%s] FROM [%s] ORDER BY [%s] ASC LIMIT %d)",
                    BehaviorDatabase.Tables.DATACOLLECTION,
                    DataCollectionColumns._ID,
                    DataCollectionColumns._ID,
                    BehaviorDatabase.Tables.DATACOLLECTION,
                    DataCollectionColumns._ID,
                    count - MAX_BEHAVIOR_COUNT);
            db.execSQL(sql);
        }
    }

    /**
     * <p>从数据库中删除数据.</p>
     * @param behaviors behaviors
     */
    private void deleteBehaviorsFromDb(List<CollectionsPostBean> behaviors) {
        LOGD(TAG, "deleteBehaviorsFromDb() called with: behaviors = [" + behaviors + "]");
        if (behaviors != null && behaviors.size() > 0) {
            int index = 0;
            StringBuilder whereClauseSb = new StringBuilder(DataCollectionColumns._ID + " in (");
            String[] whereArgs = new String[behaviors.size()];
            for (CollectionsPostBean behavior : behaviors) {
                if (index == 0) {
                    whereClauseSb.append("?");
                }  else {
                    whereClauseSb.append(", ?");
                }
                whereArgs[index] = String.valueOf(behavior.getRowId());
                index++;
            }
            whereClauseSb.append(")");

            SQLiteDatabase db = mBehaviorOpenHelper.getWritableDatabase();
            db.delete(Tables.DATACOLLECTION, whereClauseSb.toString(), whereArgs);
        }
    }

    /**
     * <p>删除所有记录.</p>
     */
    private void deleteAllBehaviorsFromDb() {
        LOGD(TAG, "deleteAllBehaviorsFromDb() called");
        SQLiteDatabase db = mBehaviorOpenHelper.getWritableDatabase();
        db.delete(Tables.DATACOLLECTION, null, null);
    }

    /**
     * <p>查询记录条数</p>
     * @return 记录条数
     */
    private int getBehaviorCount() {
        Cursor cursor = null;
        try {
            SQLiteDatabase db = mBehaviorOpenHelper.getWritableDatabase();
            String sql = "SELECT COUNT(*) FROM [" + BehaviorDatabase.Tables.DATACOLLECTION + "]";
            cursor = db.rawQuery(sql, null);
            cursor.moveToFirst();
            return cursor.getInt(0);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

}
