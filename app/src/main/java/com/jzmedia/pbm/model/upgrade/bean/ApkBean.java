package com.jzmedia.pbm.model.upgrade.bean;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

/**
 * Created by Leo on 2018/1/2.
 */
@Keep
public class ApkBean {
    private String packageName;
    private int version;
    private String apkPath;
    private String apkName;

    public String getApkName() {
        return apkName;
    }

    public void setApkName(String apkName) {
        this.apkName = apkName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getVersion() {
        return version;
    }

    public String getApkPath() {
        return apkPath;
    }

    public void setApkPath(String apkPath) {
        this.apkPath = apkPath;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "ApkBean{" +
                "packageName='" + packageName + '\'' +
                ", version=" + version +
                ", apkPath='" + apkPath + '\'' +
                ", apkName='" + apkName + '\'' +
                '}';
    }
}
