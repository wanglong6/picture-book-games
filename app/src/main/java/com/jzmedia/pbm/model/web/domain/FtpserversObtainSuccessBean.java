package com.jzmedia.pbm.model.web.domain;

//import android.support.annotation.Keep;

import androidx.annotation.Keep;

import java.util.List;

/**
 * Created by ChristieIn on 2018/3/9.
 * ftpservers obtain success bean.
 */
@Keep
public class FtpserversObtainSuccessBean extends BaseSuccessBean<List<FtpserversObtainSuccessBean.DataBean>> {

    @Keep
    public static class DataBean {
        /**
         * id : 1
         * name : 1号服务器
         * ip : 192.168.160.9
         * port : 21
         * path : /
         * protocol : ftp
         * username : sjsj
         * password : 1234
         * description : 这是一台FTP的测试服务器
         */

        private int id;
        private String name;
        private String ip;
        private int port;
        private String path;
        private String protocol;
        private String username;
        private String password;
        private String description;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public int getPort() {
            return port;
        }

        public void setPort(int port) {
            this.port = port;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getProtocol() {
            return protocol;
        }

        public void setProtocol(String protocol) {
            this.protocol = protocol;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", ip='" + ip + '\'' +
                    ", port=" + port +
                    ", path='" + path + '\'' +
                    ", protocol='" + protocol + '\'' +
                    ", username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    ", description='" + description + '\'' +
                    '}';
        }
    }
}
