package com.jzmedia.pbm.database;

import android.provider.BaseColumns;

/**
 * Created by ChristieIn on 2018/3/8.
 * user behavior database contract.
 */

public class BehaviorContract {

    /**
     * <p>type表</p>
     */
    public interface DataCollectionColumns extends BaseColumns {
        /** 数据类型. */
        String TYPE = "type";
        /** 事件. */
        String ACTION = "action";
        /** sn. */
        String SN = "sn";
        /** mac. */
        String MAC = "mac";
        /** who. */
        String WHO = "who";
        /** from. */
        String FROM = "from";
        /** 数据时间. */
        String TIME = "time";
        /** 平台型号. */
        String PLATFORM = "platform";
        /** 厂家. */
        String VENDOR = "vendor";
        /** 事件id. */
        String OBJ_ID = "id";
        /** 事件名. */
        String OBJ_NAME = "name";
        /** type id. */
        String OBJ_TYPE_ID = "type_id";
        /** cp id. */
        String OBJ_CP_ID = "cp_id";
        /** data1. */
        String OBJ_DATA1 = "data1";
        /** data2. */
        String OBJ_DATA2 = "data2";
        /** data3. */
        String OBJ_DATA3 = "data3";
    }

}
