package com.jzmedia.pbm.database.domain;

import java.io.Serializable;

/**
 * Created by ChristieIn on 2018/3/16.
 * picture book.
 */
public class PictureBook implements Serializable {
    /** 课本id. */
    private int bookId;
    /** 内容分类id. */
    private int cpId;
    /** 内容分类id. */
    private int typeId;
    /** 名称. */
    private String name;
    /** 版本. */
    private int version;
    /** v封面缩略图地址. */
    private String vIcon;
    /** h封面缩略图地址. */
    private String hIcon;
    /** 绘本存储的目录. */
    private String path;
    /** 描述. */
    private String description;
    /** 适合年龄段. */
    private String age;

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getCpId() {
        return cpId;
    }

    public void setCpId(int cpId) {
        this.cpId = cpId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getVIcon() {
        return vIcon;
    }

    public void setVIcon(String icon) {
        this.vIcon = icon;
    }

    public String getHIcon() {
        return hIcon;
    }

    public void setHIcon(String icon) {
        this.hIcon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "PictureBook{" +
                "bookId=" + bookId +
                ", cpId=" + cpId +
                ", typeId=" + typeId +
                ", name='" + name + '\'' +
                ", version=" + version +
                ", vIcon='" + vIcon + '\'' +
                ", hIcon='" + hIcon + '\'' +
                ", path='" + path + '\'' +
                ", description='" + description + '\'' +
                ", age='" + age + '\'' +
                '}';
    }

}
