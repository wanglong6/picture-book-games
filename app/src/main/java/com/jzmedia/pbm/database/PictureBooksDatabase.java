package com.jzmedia.pbm.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.LOGW;
import static com.jzmedia.common.util.LogUtils.makeLogTag;
import static com.jzmedia.pbm.database.PictureBooksContract.*;

/**
 * Created by ChristieIn on 2018/3/8.
 * picture books database.
 */

public class PictureBooksDatabase extends SQLiteOpenHelper {
    /** 打印标签. */
    private static final String TAG = makeLogTag("PictureBooksDatabase");
    /** 数据库文件名. */
    private static final String DATABASE_NAME = "content.db";
    /**
     * <p>数据库版本.</p>
     * <p>数据库版本1,对应应用版本1.0.0</p>
     */
    private static final int VER_2018_RELEASE_A = 5; // app version 1.0.0
    /** 数据当前版本. */
    private static final int CUR_DATABASE_VERSION = VER_2018_RELEASE_A;

    /**
     * <p>数据表</p>
     */
    public interface Tables {
        String BOOK_TYPES = "booktypes";
        String BOOKS = "books";
        String BOOK_MODULES = "bookmodules";
        String PROPERTIES = "properties";
        String BOOK_PROPERTIES = "bookproperties";
        String CONFIGS = "configs";

        // When tables get deprecated, add them to this list (so they get correctly deleted
        // on database upgrades).
        enum DeprecatedTables {
            ;
            String tableName;
            DeprecatedTables(String tableName) {
                this.tableName = tableName;
            }
        };
    }

    /**
     * <p>构造函数</p>
     * @param context context
     */
    public PictureBooksDatabase(Context context) {
        super(context, DATABASE_NAME, null, CUR_DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //创建BookTypes表
        db.execSQL("CREATE TABLE [" + Tables.BOOK_TYPES + "] ("
                + "[" + BookTypesColumns._ID + "] INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "[" + BookTypesColumns.TYPE_ID + "] INTEGER NOT NULL,"
                + "[" + BookTypesColumns.NAME + "] TEXT,"
                + "[" + BookTypesColumns.MEMO + "] TEXT,"
                + "[" + BookTypesColumns.PID + "] INTEGER DEFAULT 0,"
                + "[" + BookTypesColumns.ICON + "] TEXT,"
                + "[" + BookTypesColumns.HASSUB + "] INTEGER DEFAULT 0,"
                + "UNIQUE ([" + BookTypesColumns.TYPE_ID + "]) ON CONFLICT REPLACE)");

        //在表BookTypes表里创建索引
        db.execSQL("CREATE INDEX [idx_types] ON [" + Tables.BOOK_TYPES + "] (["
                + BookTypesColumns.PID + "])");

        //创建Books表
        db.execSQL("CREATE TABLE [" + Tables.BOOKS + "] ("
                + "[" + BooksColumns._ID + "] INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "[" + BooksColumns.BOOK_ID + "] INTEGER NOT NULL,"
                + "[" + BooksColumns.CP_ID + "] INTEGER NOT NULL,"
                + "[" + BooksColumns.TYPE_ID + "] INTEGER NOT NULL,"
                + "[" + BooksColumns.NAME + "] TEXT,"
                + "[" + BooksColumns.VERSION + "] INTEGER NOT NULL,"
                + "[" + BooksColumns.V_ICON + "] TEXT,"
                + "[" + BooksColumns.H_ICON + "] TEXT,"
                + "[" + BooksColumns.PATH + "] TEXT,"
                + "[" + BooksColumns.DESCRIPTION + "] TEXT,"
                + "[" + BooksColumns.AGE + "] TEXT,"
                + "UNIQUE ([" + BooksColumns.BOOK_ID + "]) ON CONFLICT REPLACE)");

        //在表Books表里创建索引
        db.execSQL("CREATE INDEX [idx_books] ON [" + Tables.BOOKS + "] (["
                + BooksColumns.TYPE_ID + "])");

        //创建modules表
        db.execSQL("CREATE TABLE [" + Tables.BOOK_MODULES + "] ("
                + "[" + BookModulesColumns._ID + "] INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "[" + BookModulesColumns.URL + "] TEXT,"
                + "[" + BookModulesColumns.SEQ + "] TEXT,"
                + "[" + BookModulesColumns.BOOK_ID + "] INTEGER,"
                + "[" + BookModulesColumns.NAME + "] TEXT,"
                + "[" + BookModulesColumns.ICON + "] TEXT)");

        //在表Modules表里创建索引
        db.execSQL("CREATE INDEX [idx_modules] ON [" + Tables.BOOK_MODULES + "] (["
                + BookModulesColumns.BOOK_ID + "])");

        //创建Properties表
        db.execSQL("CREATE TABLE [" + Tables.PROPERTIES + "] ("
                + "[" + PropertiesColumns._ID + "] INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "[" + PropertiesColumns.PROP_ID + "] INTEGER,"
                + "[" + PropertiesColumns.NAME + "] TEXT)");

        //在表Properties表里创建索引
        db.execSQL("CREATE INDEX [idx_properties] ON [" + Tables.PROPERTIES + "] (["
                + PropertiesColumns.PROP_ID + "])");

        //创建Books-Properties关联表
        db.execSQL("CREATE TABLE [" + Tables.BOOK_PROPERTIES + "] ("
                + "[" + BookPropertiesColumns._ID + "] INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "[" + BookPropertiesColumns.BOOK_ID + "] INTEGER,"
                + "[" + BookPropertiesColumns.PROP_ID + "] INTEGER)");

        db.execSQL("CREATE INDEX [idx_bookproperties] ON [" + Tables.BOOK_PROPERTIES + "] (["
                + BookPropertiesColumns.BOOK_ID + "], ["
                + BookPropertiesColumns.PROP_ID + "])");

        //创建configs表
        db.execSQL("CREATE TABLE [" + Tables.CONFIGS + "] ("
                + "[" + ConfigsColumns._ID + "] INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "[" + ConfigsColumns.KEY + "] INTEGER NOT NULL,"
                + "[" + ConfigsColumns.VALUE + "] TEXT,"
                + "UNIQUE ([" + ConfigsColumns.KEY + "]) ON CONFLICT REPLACE)");

        /*表增加字段例子
        db.execSQL("ALTER TABLE " + Tables.BOOKS
                + " ADD COLUMN " + BooksColumns.PATH + " TEXT");*/

        //初始化数据
        for (String sql : PictureBooksData.sqls) {
            db.execSQL(sql);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        LOGD(TAG, "onUpgrade() from " + oldVersion + " to " + newVersion);

        // Current DB version. We update this variable as we perform upgrades to reflect
        // the current version we are in.
        int version = oldVersion;

        // Check if we can upgrade from release 2014 C to release 2015 A.
        //if (version == VER_2018_RELEASE_A) {
            //LOGD(TAG, "Upgrading database from 2018 release A to 2018 release B.");
            //upgradeFrom2018Ato2018B(db);
            //upgradeDataFrom2018Ato2018B(db);
            //version = VER_2018_RELEASE_B;
        //}

        LOGD(TAG, "After upgrade logic, at version " + version);

        // Drop tables that have been deprecated.
        for (Tables.DeprecatedTables deprecatedTable : Tables.DeprecatedTables.values()) {
            db.execSQL("DROP TABLE IF EXISTS [" + deprecatedTable.tableName + "]");
        }

        if (version != CUR_DATABASE_VERSION) {
            LOGW(TAG, "Upgrade unsuccessful -- destroying old data during upgrade");
            db.execSQL("DROP TABLE IF EXISTS [" + Tables.BOOK_TYPES + "]");
            db.execSQL("DROP TABLE IF EXISTS [" + Tables.BOOKS + "]");
            db.execSQL("DROP TABLE IF EXISTS [" + Tables.BOOK_MODULES + "]");
            db.execSQL("DROP TABLE IF EXISTS [" + Tables.PROPERTIES + "]");
            db.execSQL("DROP TABLE IF EXISTS [" + Tables.BOOK_PROPERTIES + "]");
            db.execSQL("DROP TABLE IF EXISTS [" + Tables.CONFIGS + "]");

            onCreate(db);
        }
    }

}
