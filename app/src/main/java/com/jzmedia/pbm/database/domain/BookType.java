package com.jzmedia.pbm.database.domain;

import java.io.Serializable;

/**
 * Created by ChristieIn on 2018/3/16.
 * book type.
 */
public class BookType implements Serializable {

    /** 类型id. */
    private int typeId;
    /** 名称. */
    private String name;
    /** 备注. */
    private String memo;
    /** 父分类，如果是一级分类则为0. */
    private int parentId;
    /** icon文件. */
    private String icon;
    /** 是否有子类. */
    private boolean hasSub;

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isHasSub() {
        return hasSub;
    }

    public void setHasSub(boolean hasSub) {
        this.hasSub = hasSub;
    }

    @Override
    public String toString() {
        return "BookType{" +
                "typeId=" + typeId +
                ", name='" + name + '\'' +
                ", memo='" + memo + '\'' +
                ", parentId=" + parentId +
                ", icon='" + icon + '\'' +
                ", hasSub=" + hasSub +
                '}';
    }
}
