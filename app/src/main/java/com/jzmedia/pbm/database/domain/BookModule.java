package com.jzmedia.pbm.database.domain;

import java.io.Serializable;

/**
 * Created by ChristieIn on 2018/3/16.
 * book module.
 */
public class BookModule implements Serializable {
    /** 访问地址. */
    private String url;
    /** 序号. */
    private int seq;
    /** 所属课本id. */
    private int bookId;
    /** 模块名称. */
    private String name;
    /** 模块图标. */
    private String icon;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "BookModule{" +
                "url='" + url + '\'' +
                ", seq=" + seq +
                ", bookId=" + bookId +
                ", name='" + name + '\'' +
                ", icon='" + icon + '\'' +
                '}';
    }
}
