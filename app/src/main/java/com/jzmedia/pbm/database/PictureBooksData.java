package com.jzmedia.pbm.database;

/**
 * Created by ChristieIn on 2018/3/21.
 */

public class PictureBooksData {
    public static String[] sqls = {
            //book types
            /*"insert into [booktypes] values(null, 1, '绘本精读', null, 0, null, 1)",
            "insert into [booktypes] values(null, 101, '性格养成', null, 1, null, 0)",
            "insert into [booktypes] values(null, 102, '情感表达', null, 1, null, 0)",
            "insert into [booktypes] values(null, 103, '习惯培养', null, 1, null, 0)",
            "insert into [booktypes] values(null, 104, '社会交往', null, 1, null, 0)",*/
            "insert into [booktypes] values(null, 2, '电子图书馆', null, 0, null, 1)",
            "insert into [booktypes] values(null, 201, '情感表达', null, 2, null, 0)",
            "insert into [booktypes] values(null, 202, '生活认知', null, 2, null, 0)",
            "insert into [booktypes] values(null, 203, '社会交往', null, 2, null, 0)",
            "insert into [booktypes] values(null, 204, '科学发现', null, 2, null, 0)",

            "insert into [books] values(null, 25, 0, 201, '不管怎样都爱你', 0, '/dzts/qgbd/kwayy/kwayy.png', '', '/dzts/qgbd/kwayy/', null, null)",
            "insert into [books] values(null, 26, 0, 201, '吃饭了', 0, '/dzts/qgbd/kdann/kdann.png', '', '/dzts/qgbd/kwann/', null, null)",
            "insert into [books] values(null, 27, 0, 201, '穿衣服', 0, '/dzts/qgbd/wyzdn/wyzdn.png', '', '/dzts/qgbd/wyzdn/', null, null)",
            "insert into [books] values(null, 28, 0, 201, '第一个吻', 0, '/dzts/qgbd/wdcxygm/wdcxygm.png', '', '/dzts/qgbd/wdcxygm/', null, null)",
            "insert into [books] values(null, 29, 0, 201, '卡卡搬新家', 0, '/dzts/qgbd/wdggl/wdggl.png', '', '/dzts/qgbd/wdggl/', null, null)",
            "insert into [books] values(null, 30, 0, 202, '瓢虫侠大拯救', 0, '/dzts/shrz/lw/lw.png', '', '/dzts/shrz/lw/', null, null)",
            "insert into [books] values(null, 31, 0, 202, '找朋友', 0, '/dzts/shrz/djdydhq/djdydhq.png', '', '/dzts/shrz/djdydhq/', null, null)",
            "insert into [books] values(null, 32, 0, 202, '拯救超人', 0, '/dzts/zjcr/zjcr.png', '', '/dzts/zjcr/', null, null)",

            "insert into [bookmodules] values(null, '/hbyx/bgzydan/index.html', 0, 25, '', null)",
            "insert into [bookmodules] values(null, '/hbyx/cfl/index.html', 0, 26, '', null)",
            "insert into [bookmodules] values(null, '/hbyx/cyf/index.html', 0, 27, '', null)",
            "insert into [bookmodules] values(null, '/hbyx/dygw/index.html', 0, 28, '', null)",
            "insert into [bookmodules] values(null, '/hbyx/kkbxj/index.html', 0, 29, '', null)",
            "insert into [bookmodules] values(null, '/hbyx/pcxdzj/index.html', 0, 30, '', null)",
            "insert into [bookmodules] values(null, '/hbyx/zpy/index.html', 0, 31, '', null)",
            "insert into [bookmodules] values(null, '/hbyx/zjcr/index.html', 0, 32, '', null)"
    };
}
