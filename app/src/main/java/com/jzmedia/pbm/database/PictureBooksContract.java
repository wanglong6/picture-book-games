package com.jzmedia.pbm.database;

import android.provider.BaseColumns;

/**
 * Created by ChristieIn on 2018/3/8.
 * picture books database contract.
 */

public class PictureBooksContract {

    /**
     * <p>type表</p>
     */
    public interface BookTypesColumns extends BaseColumns {
        /** 类型id. */
        String TYPE_ID = "type_id";
        /** 名称. */
        String NAME = "name";
        /** 备注. */
        String MEMO = "memo";
        /** 父分类，如果是一级分类则为0. */
        String PID = "pid";
        /** icon文件. */
        String ICON = "icon";
        /** 是否有子类，有为1，没有为0. */
        String HASSUB = "hassub";
    }

    /**
     * <p>book表</p>
     */
    public interface BooksColumns extends BaseColumns {
        /** 课本id. */
        String BOOK_ID = "book_id";
        /** 内容提供商id. */
        String CP_ID = "cp_id";
        /** 内容分类id. */
        String TYPE_ID = "type_id";
        /** 名称. */
        String NAME = "name";
        /** 版本. */
        String VERSION	 = "version";
        /** 封面缩略图地址vertical. */
        String V_ICON = "v_icon";
        /** 封面缩略图地址horizontal. */
        String H_ICON = "h_icon";
        /** 绘本存储的目录. */
        String PATH = "path";
        /** 描述. */
        String DESCRIPTION = "description";
        /** 适合年龄段. */
        String AGE = "age";
    }

    /**
     * <p>module表</p>
     */
    public interface BookModulesColumns extends BaseColumns {
        /** 访问地址. */
        String URL = "url";
        /** 序号. */
        String SEQ = "seq";
        /** 所属课本id. */
        String BOOK_ID = "book_id";
        /** 模块名称. */
        String NAME = "name";
        /** 模块图标. */
        String ICON = "icon";
    }

    /**
     * <p>property表</p>
     */
    public interface PropertiesColumns extends BaseColumns {
        /** 属性id. */
        String PROP_ID = "prop_id";
        /** 属性名. */
        String NAME = "name";
    }

    /**
     * <p>book与property对应表</p>
     */
    public interface BookPropertiesColumns extends BaseColumns {
        /** 课本id. */
        String BOOK_ID = "book_id";
        /** 属性id. */
        String PROP_ID = "prop_id";
    }

    /**
     * <p>config表</p>
     */
    public interface ConfigsColumns extends BaseColumns {
        /** 配置信息key. */
        String KEY = "key";
        /** 配置信息value. */
        String VALUE = "value";
    }

}
