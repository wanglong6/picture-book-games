/**
 * Created by qiu on 2016/03/14.
 */
window.isDebugMode = false;
window.__pt_version = '1.1.3';

if(window.isDebugMode)
{
    window.ebookObj = {
        jsHandlerBack:function(param){ trace('jsHandlerBack: ' + param); },
        jsGoHomePage:function(param){ trace('jsGoHomePage: ' + param); }
    };
}
window.__requireUrls = [];

window.__requireUrls.push('js/com/3rd/TweenMax.min.js');
window.__requireUrls.push('js/com/createjs/tweenjs-0.6.1.min.js');
window.__requireUrls.push('js/com/createjs/soundjs-0.6.1.min.js');
window.__requireUrls.push('js/com/createjs/easeljs-0.8.2.min.js');

window.__requireUrls.push('js/src/core.js');
window.__requireUrls.push('js/src/global.js');
window.__requireUrls.push('js/src/external.js');

window.__requireUrls.push('js/com/pinetree/net/URLVariables.js');
window.__requireUrls.push('js/com/pinetree/net/URLRequestMethod.js');
window.__requireUrls.push('js/com/pinetree/net/URLRequest.js');
window.__requireUrls.push('js/com/pinetree/net/URLLoaderDataFormat.js');
window.__requireUrls.push('js/com/pinetree/net/URLLoader.js');

window.__requireUrls.push('js/com/pinetree/signals/Signal.js');

window.__requireUrls.push('js/com/pinetree/ui/ExImage.js');
window.__requireUrls.push('js/com/pinetree/ui/UIComponent.js');
window.__requireUrls.push('js/com/pinetree/ui/UIContainer.js');

window.__requireUrls.push('js/com/pinetree/utils/ArrayUtil.js');
window.__requireUrls.push('js/com/pinetree/utils/DelayCaller.js');
window.__requireUrls.push('js/com/pinetree/utils/Dictionary.js');
window.__requireUrls.push('js/com/pinetree/utils/JSONParser.js');
window.__requireUrls.push('js/com/pinetree/utils/MathUtil.js');
window.__requireUrls.push('js/com/pinetree/utils/Keyboards.js');
window.__requireUrls.push('js/com/pinetree/utils/KeyboardUtil.js');
window.__requireUrls.push('js/com/pinetree/utils/StringUtil.js');
window.__requireUrls.push('js/com/pinetree/utils/SoundManager.js');
window.__requireUrls.push('js/com/pinetree/utils/StorageManager.js');
window.__requireUrls.push('js/com/pinetree/utils/TextLoader.js');

window.__requireUrls.push('js/src/ui/Entity.js');
window.__requireUrls.push('js/src/ui/GameContainer.js');
window.__requireUrls.push('js/src/ui/LoadData.js');
window.__requireUrls.push('js/src/ui/StratPanel.js');

window.__requireUrls.push('js/src/HIDKeyboardManager.js');
window.__requireUrls.push('js/src/Main.js');
window.__requireUrls.push('js/src/Logic.js');

if(window.isDebugMode)
{
    var len = window.__requireUrls.length;
    for(var i = 0; i < len; i ++)
    {
        document.write("<script type='text/javascript' src='" + window.__requireUrls[i] + "'></script>");
    }
    console.log(window.__requireUrls.join(","));
}
else
{
    document.write("<script type='text/javascript' src='js/game.js?v=" + window.__pt_version + "'></script>");
}
