/**
 * Created by qiu on 2016/03/14.
 */
window.isDebugMode = false;
window.useDecyrptedJson = false;
if(window.isDebugMode)
{
    if(typeof window.ebookObj !== 'object'){
        window.ebookObj = {}
    }
    window.ebookObj.jsHandlerBack = function(param){ trace('jsHandlerBack: ' + param); };
    window.ebookObj.jsGoHomePage  = function(param){ trace('jsGoHomePage: ' + param);};
}else{
    if(typeof window.ebookObj == 'object' && window.ebookObj != null){
        if(typeof window.ebookObj.decryptData === "function"){ //需要使用加密版本
            window.useDecyrptedJson = true;
        }
    }
}


window.__requireUrls = [];

window.__requireUrls.push('js/com/3rd/TweenMax.min.js');
window.__requireUrls.push('js/com/createjs/tweenjs-0.6.1.min.js');
window.__requireUrls.push('js/com/createjs/soundjs-0.6.1.min.js');
window.__requireUrls.push('js/com/createjs/easeljs-0.8.2.min.js');

window.__requireUrls.push('js/src/core.js');
window.__requireUrls.push('js/src/global.js');
window.__requireUrls.push('js/src/external.js');

window.__requireUrls.push('js/com/pinetree/net/URLVariables.js');
window.__requireUrls.push('js/com/pinetree/net/URLRequestMethod.js');
window.__requireUrls.push('js/com/pinetree/net/URLRequest.js');
window.__requireUrls.push('js/com/pinetree/net/URLLoaderDataFormat.js');
window.__requireUrls.push('js/com/pinetree/net/URLLoader.js');

window.__requireUrls.push('js/com/pinetree/signals/Signal.js');

window.__requireUrls.push('js/com/pinetree/ui/ExImage.js');
window.__requireUrls.push('js/com/pinetree/ui/UIComponent.js');
window.__requireUrls.push('js/com/pinetree/ui/UIContainer.js');
window.__requireUrls.push('js/com/pinetree/ui/MovieClip.js');

window.__requireUrls.push('js/com/pinetree/utils/ArrayUtil.js');
window.__requireUrls.push('js/com/pinetree/utils/DelayCaller.js');
window.__requireUrls.push('js/com/pinetree/utils/Dictionary.js');
window.__requireUrls.push('js/com/pinetree/utils/JSONParser.js');
window.__requireUrls.push('js/com/pinetree/utils/MathUtil.js');
window.__requireUrls.push('js/com/pinetree/utils/Keyboards.js');
window.__requireUrls.push('js/com/pinetree/utils/KeyboardUtil.js');
window.__requireUrls.push('js/com/pinetree/utils/StringUtil.js');
window.__requireUrls.push('js/com/pinetree/utils/SoundManager.js');
window.__requireUrls.push('js/com/pinetree/utils/StorageManager.js');
window.__requireUrls.push('js/com/pinetree/utils/TextLoader.js');
window.__requireUrls.push('js/com/pinetree/utils/Drop.js');
window.__requireUrls.push('js/com/pinetree/utils/LoaderQueue.js');

window.__requireUrls.push('js/src/ui/components/BitmapItem.js');

window.__requireUrls.push('js/src/ui/ObjPositionManager.js');
window.__requireUrls.push('js/src/ui/DebugView.js');
window.__requireUrls.push('js/src/ui/SceneBase.js');
window.__requireUrls.push('js/src/ui/SceneGame1.js');
window.__requireUrls.push('js/src/ui/SceneGame2.js');
window.__requireUrls.push('js/src/ui/SceneGame3.js');
window.__requireUrls.push('js/src/ui/SceneGame4.js');
window.__requireUrls.push('js/src/ui/SceneMask.js');
window.__requireUrls.push('js/src/ui/SceneLevelEnd.js');
window.__requireUrls.push('js/src/ui/SceneEnd.js');

window.__requireUrls.push('js/src/ui/GameLogic.js');
window.__requireUrls.push('js/src/ui/GameContainer.js');
window.__requireUrls.push('js/src/ui/EnumSounds.js');
window.__requireUrls.push('js/src/ui/EnumScene.js');
window.__requireUrls.push('js/src/ui/UIbase.js');
window.__requireUrls.push('js/src/ui/Vo.js');
window.__requireUrls.push('js/src/ui/PreLogic.js');


window.__requireUrls.push('js/src/HIDKeyboardManager.js');
window.__requireUrls.push('js/src/TabFocusMgr.js');
window.__requireUrls.push('js/src/Main.js');
window.__requireUrls.push('js/src/Logic.js');

if(window.isDebugMode)
{
    var len = window.__requireUrls.length;
    for(var i = 0; i < len; i ++)
    {
        document.write("<script type='text/javascript' src='" + window.__requireUrls[i] + "'></script>");
    }
    console.log(window.__requireUrls.join(","));
}
else
{
    document.write("<script type='text/javascript' src='js/game.js?v=1.0.1_20180418'></script>");
}
