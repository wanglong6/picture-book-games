/**
 * Created by qiu on 2016/03/14.
 */
;
window.isDebugMode = false;
window.__pt_version = '1.1.2';
window.useDecyrptedJson = false;
if(window.isDebugMode)
{
  if(typeof window.ebookObj !== 'object'){
    window.ebookObj = {}
  }
  window.ebookObj.jsHandlerBack = function(param){ trace('jsHandlerBack: ' + param); };
  window.ebookObj.jsGoHomePage  = function(param){ trace('jsGoHomePage: ' + param);};
}else{
  if(typeof window.ebookObj == 'object' && window.ebookObj != null){
    if(typeof window.ebookObj.decryptData === "function"){ //需要使用加密版本
      window.useDecyrptedJson = true;
    }
  }
}

;(function () {
    var urlData = [
        {
            folder:'js/com/createjs/',
            urls:[
                'tweenjs-0.6.1.min.js',
                'soundjs-0.6.1.min.js',
                'easeljs-0.8.2.min.js'
            ]
        },
        {
            folder:'js/src/',
            urls:[
                'core.js',
                'global.js',
                'external.js'
            ]
        },
        {
            folder:'js/com/pinetree/net/',
            urls:[
                'URLVariables.js',
                'URLRequestMethod.js',
                'URLRequest.js',
                'URLLoaderDataFormat.js',
                'URLLoader.js'
            ]
        },
        {
            folder:'js/com/pinetree/signals/',
            urls:[
                'Signal.js'
            ]
        },
        {
            folder:'js/com/pinetree/ui/',
            urls:[
                'ExImage.js',
                'UIComponent.js',
                'UIContainer.js',
                'BitmapItem.js',
                'MovieClip.js',
                'debug/DebugView.js'
            ]
        },
        {
            folder:'js/com/pinetree/utils/',
            urls:[
                'ArrayUtil.js',
                'DelayCaller.js',
                'Dictionary.js',
                'JSONLoader.js',
                'JSONParser.js',
                'Keyboards.js',
                'KeyboardUtil.js',
                'LoaderQueue.js',
                'MathUtil.js',
                'SoundManager.js',
                'StorageManager.js',
                'StringUtil.js',
                'TextLoader.js',
                'TriggerManager.js'
            ]
        },
        {
            folder:'js/src/ui/',
            urls:[
                'Container.js',
                'StartAnimation.js',
                'EndAnimation.js',
                'GameOverPanel.js',
                'TwoPanel.js',
                'Monster.js',
                'ThreePanel.js'
            ]
        },
        {
            folder:'js/src/logic/',
            urls:[
                'PreloadLogic.js',
                'GameLogic.js',
                'Logic.js'
            ]
        },
        {
            folder:'js/src/',
            urls:[
                'Main.js'
            ]
        }
    ];

    if(window.isDebugMode)
    {
        var i, len = urlData.length, urls = [], obj;
        for(i = 0; i < len; i ++)
        {
            obj = urlData[i];
            if(obj.urls)
            {
                obj.urls.forEach(function (url) {
                    urls.push(obj.folder + url);
                });
            }
        }

        len = urls.length;
        for(i = 0; i < len; i ++)
        {
            document.write("<script type='text/javascript' src='" + urls[i] + "'></script>");
        }
        console.log(urls.join(","));
    }
    else
    {
        document.write("<script type='text/javascript' src='js/game.js?v=" + window.__pt_version + "'></script>");
    }
})();
