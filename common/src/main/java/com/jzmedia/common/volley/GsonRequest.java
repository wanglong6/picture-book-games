package com.jzmedia.common.volley;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by ChristieIn on 2017/7/20.
 * gson request.
 */
public class GsonRequest<T> extends JsonRequest<T> {
    public static final int DEFAULT_TIMEOUT_MS = 10 * 1000;
    private final Gson gson = new Gson();
    private final Class<T> clazz;

    /**
     * <p>Make a GET request and return a parsed object from JSON.</p>
     * @param url URL of the request to make
     * @param clazz Relevant class object, for Gson's reflection
     * @param listener response listener
     * @param errorListener error response listener
     */
    public GsonRequest(String url, Class<T> clazz, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(url, clazz, null, listener, errorListener);
    }

    /**
     * <p>Make a GET request and return a parsed object from JSON.</p>
     * @param url URL of the request to make
     * @param clazz Relevant class object, for Gson's reflection
     * @param requestBody request body to post
     * @param listener response listener
     * @param errorListener error response listener
     */
    public GsonRequest(String url, Class<T> clazz, String requestBody,
                       Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, requestBody, listener, errorListener);
        this.clazz = clazz;
        setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT_MS,//默认超时时间为2.5秒，应设置一个稍微大点儿的
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,//默认最大尝试次数
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(gson.fromJson(json, clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

}