package com.jzmedia.common.volley;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by ChristieIn on 2017/8/1.
 * <p>Json Array Request.和volley原有的JsonArrayRequest区别是requestbody是string类型.</p>
 */
public class JArrayRequest extends JsonRequest<JSONArray> {
    /** 默认超时时间. */
    private static final int PLC_TIMEOUT_MS = 10 * 1000;
    /** 请求头. */
    private final Map<String, String> mHeaders;

    /**
     * <p>Creates a new request.</p>
     * @param method the HTTP method to use
     * @param url URL to fetch the JSON from
     * @param headers Map of request headers
     * @param jsonBody A {@link String} to post with the request. Null is allowed and
     *   indicates no parameters will be posted along with request.
     * @param listener Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public JArrayRequest(int method, String url, Map<String, String> headers, String jsonBody,
                         Response.Listener<JSONArray> listener,
                         Response.ErrorListener errorListener) {
        super(method, url, jsonBody, listener, errorListener);
        mHeaders = headers;
        //默认超时时间为2.5秒，应设置一个稍微大点儿的
        setRetryPolicy(new DefaultRetryPolicy(PLC_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    /**
     * Constructor which defaults to <code>GET</code> if <code>jsonRequest</code> is
     * <code>null</code>, <code>POST</code> otherwise.
     *
     * @see #JArrayRequest(int, String, Map, String, Response.Listener, Response.ErrorListener)
     */
    public JArrayRequest(String url, Map<String, String> headers, String jsonBody,
                         Response.Listener<JSONArray> listener,
                         Response.ErrorListener errorListener) {
        this(jsonBody == null ? Method.GET : Method.POST, url, headers, jsonBody,
                listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaders != null ? mHeaders : super.getHeaders();
    }

    @Override
    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(new JSONArray(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }
}
