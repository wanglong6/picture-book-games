package com.jzmedia.common.loader;

import android.graphics.Bitmap;
import android.view.View;

/**
 * Created by ChristieIn on 2018/3/23.
 * <p>image load call back.</p>
 */
public interface ImageLoadCallback<T extends View> {
    /**
     * <p>图片加载完成回调方法.</p>
     * @param bitmap 图片
     * @param view 加载图片的视图
     */
    void onImageLoaded(Bitmap bitmap, T view);

    /**
     * <p>图片加载失败的回调方法.</p>
     * @param view 加载图片的视图
     */
    void onError(T view);
}