package com.jzmedia.common.loader;

/**
 * Created by ChristieIn on 2018/3/23.
 * <p>gson数据加载回调接口.</p>
 * @param <T> gson数据
 */
public interface GsonLoadCallback<T> {
    /**
     * <p>gson数据加载完成回调方法.</p>
     * @param response gson数据
     */
    void onGsonLoaded(T response);

    /**
     * <p>gson数据加载失败的回调方法.</p>
     */
    void onError();
}
