package com.jzmedia.common.loader;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;

import java.io.File;

/**
 * Created by ChristieIn on 2017/7/26.
 * bitmap cache.
 */

public class BitmapCache implements ImageLoader.ImageCache {
    /** 设置最大缓存为4Mb，大于这个值会启动自动回收. */
    private static final int CACHE_MAX_SIZE = 4 * 1024 * 1024;
    /** 设置最大缓存为16Mb，大于这个值会启动自动回收. */
    private static final int DISK_CACHE_MAX_SIZE = 16 * 1024 * 1024;
    /** Default on-disk cache directory. */
    private static final String DEFAULT_CACHE_DIR = "imagecache";
    /** LruCache对象. */
    private LruCache<String, Bitmap> mLruCache;

    /**
     * 构造函数.
     */
    public BitmapCache() {
        this(CACHE_MAX_SIZE);
    }

    /**
     * <p>构造函数.</p>
     * @param cacheSize cache size
     */
    /*package*/ BitmapCache(int cacheSize) {
        //初始化 LruCache
        mLruCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount();
            }
        };
    }

    @Override
    public Bitmap getBitmap(String url) {
        return mLruCache.get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        mLruCache.put(url, bitmap);
    }

    /**
     * 清除缓存.
     */
    /*package*/ void clearCache() {
        mLruCache.evictAll();
    }

    /**
     * <p>文件缓存请求队列</p>
     * @param context context
     * @return queue
     */
    public static RequestQueue newRequestQueue(Context context) {
        Network network = new BasicNetwork(new HurlStack());
        File cacheDir = new File(context.getCacheDir(), DEFAULT_CACHE_DIR);
        RequestQueue queue = new RequestQueue(new DiskBasedCache(cacheDir, DISK_CACHE_MAX_SIZE), network);
        queue.start();
        return queue;
    }

}
