package com.jzmedia.common.loader;

import android.content.Context;
//import android.support.annotation.NonNull;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.jzmedia.common.volley.GsonRequest;

import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.makeLogTag;

import androidx.annotation.NonNull;

/**
 * Created by ChristieIn on 2017/9/6.
 * data loader. 下载json、图片数据数据.
 */
public class DataLoader {
    /** 打印标签. */
    private static final String TAG = makeLogTag("DataLoader");
    /** 设置最大缓存为2Mb，大于这个值会启动自动回收. */
    private static final int CONTENT_IMAGE_CACHE_SIZE = 2 * 1024 * 1024;
    /** 请求队列. */
    private RequestQueue mQueue;
    /** content icon图片加载器，可缓存content icon图片数据. */
    private ImageLoader mContentImageLoader;
    /** content icon图片缓存. */
    private BitmapCache mContentImageLoaderCache;

    /**
     * <p>构造函数</p>
     * @param context 上下文对象
     */
    public DataLoader(@NonNull Context context) {
        mQueue = Volley.newRequestQueue(context);

        //content icon image loader
        mContentImageLoaderCache = new BitmapCache(CONTENT_IMAGE_CACHE_SIZE);
        mContentImageLoader = new ImageLoader(mQueue, mContentImageLoaderCache);
    }

    /**
     * <p>加载json数据.</p>
     * @param url 请求地址
     * @param clazz class 类型
     * @param callback callback
     */
    public <T> void loadGson(String url, Class<T> clazz, final GsonLoadCallback<T> callback) {
        LOGD(TAG, "loadJzlxGson() called with: url = [" + url + "], clazz = [" + clazz + "], callback = [" + callback + "]");
        Request request = new GsonRequest<>(url,
                clazz, null,
                new Response.Listener<T>() {
                    @Override
                    public void onResponse(T response) {
                        LOGD(TAG, "onResponse() called with: response = [" + response.toString() + "]");
                        callback.onGsonLoaded(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LOGD(TAG, "onErrorResponse() called with: error = [" + error + "]");
                        callback.onError();
                    }
                });

        //给网络请求加上tag,以便在需要取消请求时通过tag找到这个请求.
        request.setTag(url);
        mQueue.cancelAll(url);
        mQueue.add(request);
    }

    /**
     * <p>加载图片.</p>
     * @param imgUrl url
     * @param view the view to load image
     * @param callback call back
     */
    public <T extends View> void loadContentImage(String imgUrl, final T view, final ImageLoadCallback<T> callback) {
        ImageLoader.ImageListener listener = new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (callback != null) {
                    callback.onImageLoaded(response.getBitmap(), view);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onError(view);
                }
            }
        };
        mContentImageLoader.get(imgUrl, listener);
    }

    /**
     * <p>释放所有请求.</p>
     */
    public void release() {
        mQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
        mQueue.getCache().clear();
        mContentImageLoaderCache.clearCache();
    }

}
