// Copyright 2015 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.jzmedia.common.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;

import java.io.File;

/**
 * This class provides package checking related methods.
 */
public class PackageUtils {

    private PackageUtils() {
        // Hide constructor
    }

    /**
     * install package normal by system intent
     *
     * @param context context
     * @param filePath file path of package
     * @return whether apk exist
     */
    public static boolean installNormal(Context context, String filePath) {
        return installNormal(context, new File(filePath));
    }

    /**
     * install package normal by system intent
     *
     * @param context context
     * @param file file
     * @return whether apk exist
     */
    public static boolean installNormal(Context context, File file) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        if (!file.exists() || !file.isFile() || file.length() <= 0) {
            return false;
        }
        i.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
        return true;
    }

    /**
     * Retrieves the PackageInfo object for this application.
     *
     * @param context Any context.
     * @return The PackageInfo object for this application.
     */
    public static PackageInfo getOwnPackageInfo(Context context) {
        PackageManager manager = context.getPackageManager();
        try {
            String packageName = context.getPackageName();
            return manager.getPackageInfo(packageName, 0);
        } catch (NameNotFoundException e) {
            // Should never happen.
            throw new AssertionError("Failed to retrieve own package info");
        }
    }

    /**
     * Retrieves the PackageInfo object for this application.
     *
     * @param context Any context.
     * @return The PackageInfo object for this application.
     */
    public static String getOwnApplicationLabele(Context context) {
        PackageManager manager = context.getPackageManager();
        try {
            return manager.getApplicationLabel(context.getApplicationInfo()).toString();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Retrieves the PackageInfo object of the file.
     *
     * @param context Any context.
     * @param file archive file
     * @return The PackageInfo object for the file.
     */
    public static PackageInfo getPackageInfo(Context context, File file) {
        if (file.exists() && file.isFile()) {
            PackageManager manager = context.getPackageManager();
            return manager.getPackageArchiveInfo(file.getAbsolutePath(), PackageManager.GET_ACTIVITIES);
        } else {
            return null;
        }
    }

    /**
     * Retrieves the version of the given package installed on the device.
     *
     * @param context Any context.
     * @param packageName Name of the package to find.
     * @return The package's version code if found, -1 otherwise.
     */
    public static int getPackageVersion(Context context, String packageName) {
        int versionCode = -1;
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);
            if (packageInfo != null) versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // Do nothing, versionCode stays -1
        }
        return versionCode;
    }

}
