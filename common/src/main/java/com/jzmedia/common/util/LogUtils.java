package com.jzmedia.common.util;
/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.util.Log;

/**
 * <p>打印.</p>
 */
public class LogUtils {
    /** 打印前缀. */
    private static final String LOG_PREFIX = "jzcm_";
    /** 前缀长度. */
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    /** 打印标签最长长度,超过此长度需要截断,android的打印的标签最长长度就是这么长. */
    private static final int MAX_LOG_TAG_LENGTH = 23;
    /** 打印开关. */
    private static boolean LOGGING_ENABLED = true;

    /**
     * <p>隐藏构造方法</p>
     */
    private LogUtils() {
    }

    /**
     * <p>打开打印</p>
     */
    public static void enable() {
        LOGGING_ENABLED = true;
    }

    /**
     * <p>关闭打印.</p>
     */
    public static void disable() {
        //LOGGING_ENABLED = false;
    }

    /**
     * <p>生成打印标签</p>
     * @param str 自定义标签
     * @return 生成的标签
     */
    public static String makeLogTag(String str) {
        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            return LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1);
        }

        return LOG_PREFIX + str;
    }

    /**
     * <p>生成打印标签</p>
     * <p>混淆代码的类最好不要用这个方法,否则会出现打印标签混乱(Don't use this when obfuscating class names!)</p>
     * @param cls class
     * @return 生成的标签
     */
    public static String makeLogTag(Class cls) {
        return makeLogTag(cls.getSimpleName());
    }

    /**
     * Send a log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void LOGD(final String tag, String msg) {
        if (LOGGING_ENABLED){
            Log.d(tag, msg);
        }
    }

    /**
     * Send a log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static void LOGD(final String tag, String msg, Throwable tr) {
        if (LOGGING_ENABLED){
            Log.d(tag, msg, tr);
        }
    }

    /**
     * Send a log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void LOGV(final String tag, String msg) {
        if (LOGGING_ENABLED) {
            Log.v(tag, msg);
        }
    }

    /**
     * Send a log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static void LOGV(final String tag, String msg, Throwable tr) {
        if (LOGGING_ENABLED) {
            Log.v(tag, msg, tr);
        }
    }

    /**
     * Send an log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void LOGI(final String tag, String msg) {
        if (LOGGING_ENABLED) {
            Log.i(tag, msg);
        }
    }

    /**
     * Send a log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static void LOGI(final String tag, String msg, Throwable tr) {
        if (LOGGING_ENABLED) {
            Log.i(tag, msg, tr);
        }
    }

    /**
     * Send a log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void LOGW(final String tag, String msg) {
        if (LOGGING_ENABLED) {
            Log.w(tag, msg);
        }
    }

    /**
     * Send a log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static void LOGW(final String tag, String msg, Throwable tr) {
        if (LOGGING_ENABLED) {
            Log.w(tag, msg, tr);
        }
    }

    /**
     * Send an log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void LOGE(final String tag, String msg) {
        if (LOGGING_ENABLED){
            Log.e(tag, msg);
        }
    }

    /**
     * Send a log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static void LOGE(final String tag, String msg, Throwable tr) {
        if (LOGGING_ENABLED) {
            Log.e(tag, msg, tr);
        }
    }

}
