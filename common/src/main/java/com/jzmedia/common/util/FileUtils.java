package com.jzmedia.common.util;

import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import okhttp3.Response;

import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.makeLogTag;

/**
 * Created by ChristieIn on 2018/3/15.
 * file utils.
 */
public class FileUtils {
    /** 打印标签. */
    private static final String TAG = makeLogTag("FileUtils");

    /**
     * <p>隐藏构造方法</p>
     */
    private FileUtils() {
    }

    /**
     * <p>获取文件的大小</p>
     * @param file file
     * @return file size
     */
    public static long fileSize(File file) {
        LOGD(TAG, "fileSize() called with: file = [" + file + "]");
        if (file != null && file.exists()) {
            return file.length();
        }
        return 0;
    }

    /**
     * <p>获取文件的大小</p>
     * @param path file path
     * @return file size
     */
    public static long fileSize(String path) {
        LOGD(TAG, "fileSize() called with: path = [" + path + "]");
        return fileSize(new File(path));
    }

    /**
     * <p>删除文件</p>
     * @param path file path
     */
    public static void deleteFile(String path) {
        LOGD(TAG, "deleteFile() called with: path = [" + path + "]");
        File file = new File(path);
        if (!file.exists()) {
            LOGD(TAG, "file not exists!" );
        } else if (file.delete()) {
            LOGD(TAG, "delete success.");
        } else {
            LOGD(TAG, "delete failure.");
        }
    }

    /**
     * <p>删除文件夹</p>
     * @param dir 文件夹
     * @return 删除结果
     */
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            File[] children = dir.listFiles();
            if (children != null) {
                //递归删除目录中的子目录下
                for (File child : children) {
                    boolean success = deleteDir(child);
                    if (!success) {
                        return false;
                    }
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }

    /**
     * 保存配置文件到本地
     *
     * @param response
     */
    public static void saveCfgFile(final String response, final String path) {
        File cfgFile = new File(path);
        //判斷該配置文件是否已经存在
        if (cfgFile.exists()) {
            cfgFile.delete();
            //return;
        }
        FileOutputStream fos = null;//FileOutputStream会自动调用底层的close()方法，不用关闭
        BufferedWriter bw = null;
        try {
            fos = new FileOutputStream(path, false);//这里的第二个参数代表追加还是覆盖，true为追加，flase为覆盖
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            bw.write(response);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();//关闭缓冲流
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Get MD5 from file.
     * @param file
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public static String sumFileMd5(File file) throws NoSuchAlgorithmException, IOException  {
        String md5 = "";
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            MessageDigest digester = MessageDigest.getInstance("MD5");
            byte[] bytes = new byte[8192];
            int byteCount;
            while ((byteCount = fis.read(bytes)) > 0) {
                digester.update(bytes, 0, byteCount);
            }
            byte[] digest = digester.digest();
            for (byte b : digest) {
                md5 = md5 + Integer.toString(256 + (0xFF & b), 16).substring(1);
            }
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return md5;
    }

    /**
     * 判断文件的MD5是否一致.
     *
     * @param filePath
     * @param md5
     * @return
     */
    public boolean checkFileMD5(String filePath, String md5) {
        boolean isFileComplete = false;
        File file = new File(filePath);
        if (!file.exists()) {
            LOGD(TAG, "firm file is not exist: ");
            return false;
        }
        if(TextUtils.isEmpty(md5)){
            return false;
        }
        try {
            String fileMd5 = sumFileMd5(file);
            LOGD(TAG, "local file md5: " + fileMd5);
            LOGD(TAG, "service file md5: " + md5);
            if (md5.equals(fileMd5)) {
                isFileComplete = true;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            isFileComplete = false;
        } catch (IOException e) {
            e.printStackTrace();
            isFileComplete = false;
        }
        return isFileComplete;
    }
    /**
     * 截取最后一部分：“xxx/tset.apk” -->  ""tset.apk
     *
     * @param path
     * @return
     */
    public String getFileNameFromPath(String path) {
        String[] res = path.split("/");
        return res[res.length - 1];
    }

    /**
     * 去掉后缀：“test.apk”-->"test"
     *
     * @param file
     * @return
     */
    public String removeSuffix(String file) {
        String[] res = file.split("\\.");
        return res[0];
    }

}
