package com.jzmedia.common.weiget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
//import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.jzmedia.common.R;

/**
 * Created by dell on 2017/9/3.
 * 圆角图片.
 */

public class RoundAngleImageView extends AppCompatImageView {
    private Paint paint;
    /**
     * 个人理解是
     *
     * 这两个都是画圆的半径
     */
    private int roundWidth = 18;
    private int roundHeight = 18;
    private boolean roundLeftTop, roundRightTop, roundLeftBottom, roundRightBottom;
    private Paint paint2;

    public RoundAngleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public RoundAngleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RoundAngleImageView(Context context) {
        super(context);
        init(context, null);
    }

    private void init(Context context, AttributeSet attrs) {
        float density = context.getResources().getDisplayMetrics().density;
        roundWidth = (int) (roundWidth * density);
        roundHeight = (int) (roundHeight * density);
        roundLeftTop = true;
        roundRightTop = true;
        roundLeftBottom = true;
        roundRightBottom = true;
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RoundAngleImageView);
            roundWidth = a.getDimensionPixelSize(R.styleable.RoundAngleImageView_roundWidth, roundWidth);
            roundHeight = a.getDimensionPixelSize(R.styleable.RoundAngleImageView_roundHeight, roundHeight);
            roundLeftTop = a.getBoolean(R.styleable.RoundAngleImageView_roundLeftTop, true);
            roundRightTop = a.getBoolean(R.styleable.RoundAngleImageView_roundRightTop, true);
            roundLeftBottom = a.getBoolean(R.styleable.RoundAngleImageView_roundLeftBottom, true);
            roundRightBottom = a.getBoolean(R.styleable.RoundAngleImageView_roundRightBottom, true);
            a.recycle();
        }

        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));

        paint2 = new Paint();
        paint2.setXfermode(null);
    }

    @Override
    public void draw(Canvas canvas) {
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Config.ARGB_8888);
        Canvas canvas2 = new Canvas(bitmap);
        super.draw(canvas2);
        if (roundLeftTop) {
            drawLiftUp(canvas2);
        }
        if (roundLeftBottom) {
            drawLiftDown(canvas2);
        }
        if (roundRightTop) {
            drawRightUp(canvas2);
        }
        if (roundRightBottom) {
            drawRightDown(canvas2);
        }
        canvas.drawBitmap(bitmap, 0, 0, paint2);
        bitmap.recycle();
    }

    private void drawLiftUp(Canvas canvas) {
        Path path = new Path();
        path.moveTo(0, roundHeight);
        path.lineTo(0, 0);
        path.lineTo(roundWidth, 0);
        path.arcTo(new RectF(0, 0, roundWidth * 2, roundHeight * 2), -90, -90);
        path.close();
        canvas.drawPath(path, paint);
    }

    private void drawLiftDown(Canvas canvas) {
        Path path = new Path();
        path.moveTo(0, getHeight() - roundHeight);
        path.lineTo(0, getHeight());
        path.lineTo(roundWidth, getHeight());
        path.arcTo(new RectF(0, getHeight() - roundHeight * 2, roundWidth * 2, getHeight()), 90, 90);
        path.close();
        canvas.drawPath(path, paint);
    }

    private void drawRightDown(Canvas canvas) {
        Path path = new Path();
        path.moveTo(getWidth() - roundWidth, getHeight());
        path.lineTo(getWidth(), getHeight());
        path.lineTo(getWidth(), getHeight() - roundHeight);
        path.arcTo(new RectF(getWidth() - roundWidth * 2, getHeight() - roundHeight * 2, getWidth(), getHeight()), -0, 90);
        path.close();
        canvas.drawPath(path, paint);
    }

    private void drawRightUp(Canvas canvas) {
        Path path = new Path();
        path.moveTo(getWidth(), roundHeight);
        path.lineTo(getWidth(), 0);
        path.lineTo(getWidth() - roundWidth, 0);
        path.arcTo(new RectF(getWidth() - roundWidth * 2, 0, getWidth(), roundHeight * 2), -90, 90);
        path.close();
        canvas.drawPath(path, paint);
    }
}
