package com.jzmedia.common.ok.body;

import android.os.Handler;
import android.os.Looper;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import okio.ForwardingSource;
import okio.Okio;
import okio.Source;

/**
 * Created by ChristieIn on 2018/3/15.
 * <p>处理进度的响应体</p>
 */
public class ProgressResponseBody extends ResponseBody {
    /** 原始的响应体. */
    private final ResponseBody mResponseBody;
    /** 进度回调接口. */
    private final ProgressResponseListener mProgressListener;
    /** 包装完成的BufferedSourde. */
    private BufferedSource mBufferedSource;
    /** main thread handler. */
    private Handler mHandler;

    /**
     * <p>构造函数</p>
     * @param responseBody 原始的响应体
     * @param progressListener 进度回调接口
     */
    public ProgressResponseBody(ResponseBody responseBody, ProgressResponseListener progressListener) {
        mResponseBody = responseBody;
        mProgressListener = progressListener;
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public MediaType contentType() {
        return mResponseBody.contentType();
    }

    @Override
    public long contentLength() {
        return mResponseBody.contentLength();
    }

    @Override
    public BufferedSource source() {
        if (mBufferedSource == null) {
            mBufferedSource = Okio.buffer(source(mResponseBody.source()));
        }
        return mBufferedSource;
    }

    /**
     * <p>读取，回调进度接口</p>
     * @param source source
     * @return source
     */
    private Source source(Source source) {
        return new ForwardingSource(source) {
            long totalBytesRead = 0L;

            @Override
            public long read(Buffer sink, long byteCount) throws IOException {
                long bytesRead = super.read(sink, byteCount);
                // read() returns the number of bytes read, or -1 if this source is exhausted.
                totalBytesRead += bytesRead != -1 ? bytesRead : 0;
                notifyResponseProgress(totalBytesRead, contentLength(), bytesRead == -1);
                return bytesRead;
            }
        };
    }

    /**
     * <p>通知main thread响应进度</p>
     * @param bytesRead 已读取的数据长度
     * @param contentLength 总数据长度
     * @param done 进度完成
     */
    private void notifyResponseProgress(final long bytesRead, final long contentLength, final boolean done) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mProgressListener.onResponseProgress(bytesRead, contentLength, done);
            }
        });
    }

    /**
     * <p>响应体进度回调接口</p>
     */
    public interface ProgressResponseListener {
        /**
         * <p>响应进度调用的方法</p>
         * @param bytesRead 已读取的数据长度
         * @param contentLength 总数据长度
         * @param done 进度完成
         */
        void onResponseProgress(long bytesRead, long contentLength, boolean done);
    }
}
