package com.jzmedia.common.ok.dl;

import com.jzmedia.common.ok.body.ProgressResponseBody;
import com.jzmedia.common.util.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.jzmedia.common.util.LogUtils.LOGD;
import static com.jzmedia.common.util.LogUtils.makeLogTag;

/**
 * Created by ChristieIn on 2018/3/15.
 * the file downloader witch based on okhttp.
 */
public class FileDownloader {
    /** 打印标签. */
    private static final String TAG = makeLogTag("FileDownloader");

    FileDownloader() {
    }

    /**
     * <p>下载文件</p>
     * @param url 下载地址
     * @param target 保存的文件
     * @param resume 是否使用断点续传
     * @param downloadListener 下载监听器
     */
    public static Call dl(String url, final File target, boolean resume, final DownloadListener downloadListener) {
        LOGD(TAG, "dl() called with: url = [" + url + "], target = [" + target + "], resume = [" + resume + "], downloadListener = [" + downloadListener + "]");
        //如果不是断点续传,则删除已存在的目标文件
        if (!resume) {
            if (target.exists()) {
                boolean ret = target.delete();
                if (!ret) {
                    downloadListener.onFailure("target file cannot delete.");
                    return null;
                }
            }
        }

        long completed = 0;
        Request request = null;
        if (resume) {
            completed = FileUtils.fileSize(target);
            request = new Request.Builder()
                    .url(url)
                    .addHeader("RANGE", "bytes=" + completed + "-")
                    .build();
        } else {
            request = new Request.Builder()
                    .url(url)
                    .build();
        }
        final long fileCompleted = completed;

        if (downloadListener != null) {
            downloadListener.onStart();
        }

        final ProgressResponseBody.ProgressResponseListener progressListener =
                new ProgressResponseBody.ProgressResponseListener() {
                    @Override
                    public void onResponseProgress(long bytesRead, long contentLength, boolean done) {
                        if (downloadListener != null) {
                            if (done) {
                                downloadListener.onSuccess(target);
                            } else {
                                if (contentLength != -1) {
                                    downloadListener.onProgress(bytesRead + fileCompleted, contentLength + fileCompleted);
                                }
                            }
                        }
                    }
                };

        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override public Response intercept(Interceptor.Chain chain) throws IOException {
                        Response originalResponse = chain.proceed(chain.request());
                        return originalResponse.newBuilder()
                                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                                .build();
                    }
                })
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (downloadListener != null) {
                    downloadListener.onFailure("error");
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                LOGD(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
                if (!response.isSuccessful()) {
                    if (downloadListener != null) {
                        downloadListener.onFailure("response is null.");
                    }
                } else {
                    long comp = fileCompleted;
                    if(response.header("Content-Range")==null || response.header("Content-Range").length() == 0){
                        comp = 0;
                    }
                    saveFile(response, target, comp);
                }
            }
        });
        return call;
    }

    /**
     * <p>保存文件</p>
     * @param response response
     * @param target target
     * @param completedBytes completed bytes
     */
    public static void saveFile(Response response, File target, long completedBytes)  {
        byte[] buf = new byte[1024 * 32];//每次读32kb
        int len;

        InputStream is = null;
        FileOutputStream fos = null;
        try {
            is = response.body().byteStream();
            fos = new FileOutputStream(target, true);
            while ((len = is.read(buf)) != -1) {
                fos.write(buf,0,len);
                fos.flush();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(is!=null){
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if(fos!=null){
                    LOGD(TAG, "saveFile: close file.");
                    fos.flush();
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * <p>文件下载回调接口</p>
     */
    public interface DownloadListener {

        /**
         * <p>开始下载</p>
         */
        void onStart();

        /**
         * <p>下载进度</p>
         * @param current 已下载字节数
         * @param total 总字节数
         */
        void onProgress(long current, long total);

        /**
         * <p>下载完成</p>
         * @param file 下载的文件
         */
        void onSuccess(File file);

        /**
         * <p>下载失败</p>
         * @param error 错误信息
         */
        void onFailure(String error);
    }

}
