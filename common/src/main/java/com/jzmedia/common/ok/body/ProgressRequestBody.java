package com.jzmedia.common.ok.body;

import android.os.Handler;
import android.os.Looper;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

/**
 * Created by ChristieIn on 2018/3/15.
 * <p>处理进度的请求体</p>
 */
public class ProgressRequestBody extends RequestBody {
    /** 原始的请求体. */
    private final RequestBody mRequestBody;
    /** 进度回调接口. */
    private final ProgressRequestListener mProgressListener;
    /** 包装完成的BufferedSink. */
    private BufferedSink mBufferedSink;
    /** main thread handler. */
    private Handler mHandler;

    /**
     * <p>构造函数</p>
     * @param requestBody 原始的请求体
     * @param progressListener 进度回调接口
     */
    public ProgressRequestBody(RequestBody requestBody, ProgressRequestListener progressListener) {
        mRequestBody = requestBody;
        mProgressListener = progressListener;
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public MediaType contentType() {
        return mRequestBody.contentType();
    }

    @Override
    public long contentLength() throws IOException {
        return mRequestBody.contentLength();
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        if (mBufferedSink == null) {
            mBufferedSink = Okio.buffer(sink(sink));
        }
        //写入
        mRequestBody.writeTo(mBufferedSink);
        //关闭数据
        mBufferedSink.close();
    }

    /**
     * <p>写入，回调进度接口</p>
     * @param sink sink
     * @return sink
     */
    private Sink sink(Sink sink) {
        return new ForwardingSink(sink) {
            long totalBytesWritten = 0L;

            @Override
            public void write(Buffer source, long byteCount) throws IOException {
                super.write(source, byteCount);
                totalBytesWritten += byteCount;
                notifyRequestProgress(totalBytesWritten, contentLength(),
                        totalBytesWritten == contentLength());
            }

        };
    }

    /**
     * <p>通知main thread请求进度</p>
     * @param bytesWritten 已写入的数据长度
     * @param contentLength 总数据长度
     * @param done 进度完成
     */
    private void notifyRequestProgress(final long bytesWritten, final long contentLength, final boolean done) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mProgressListener.onRequestProgress(bytesWritten, contentLength, done);
            }
        });
    }

    /**
     * <p>请求体进度回调接口</p>
     */
    public interface ProgressRequestListener {
        /**
         * <p>请求进度调用的方法</p>
         * @param bytesWritten 已写入的数据长度
         * @param contentLength 总数据长度
         * @param done 进度完成
         */
        void onRequestProgress(long bytesWritten, long contentLength, boolean done);
    }
}
