package com.jzmedia.common.okhttp.callback;

import android.util.Log;

import com.jzmedia.common.okhttp.OkHttpUtil;
import com.jzmedia.common.okhttp.response.IResponseHandler;

import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MyCallback implements Callback {

    private IResponseHandler mResponseHandler;

    public MyCallback(IResponseHandler mResponseHandler) {
        this.mResponseHandler = mResponseHandler;
    }

    @Override
    public void onFailure(final Call call, final IOException e) {
        if(OkHttpUtil.isDebug){
            Log.e(OkHttpUtil.debugTag,e.getMessage());
            e.printStackTrace();
        }
        OkHttpUtil.mHandler.post(new Runnable() {
            @Override
            public void run() {
                mResponseHandler.onFailed(0,e.toString());
            }
        });
    }

    @Override
    public void onResponse(Call call, final Response response) throws IOException {
        if(OkHttpUtil.isDebug){
            Log.i(OkHttpUtil.debugTag,"response status:"+response.code()+",msg:"+response.message()+",response body:"+response.body());
        }
        if(response.isSuccessful()){
            mResponseHandler.onSuccess(response);
        }else{
            OkHttpUtil.mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mResponseHandler.onFailed(response.code(),"response msg"+response.message());
                }
            });
        }
    }
}
