package com.jzmedia.common.okhttp.response;


import com.jzmedia.common.okhttp.OkHttpUtil;

import java.io.IOException;
import okhttp3.Response;
import okhttp3.ResponseBody;

public abstract class RawResHandler implements IResponseHandler {
    @Override
    public final void onSuccess(final Response response) {
        ResponseBody responseBody = response.body();
        String resBodyStr = "";
        try {
            resBodyStr = responseBody.string();
        } catch (IOException e) {
            e.printStackTrace();
            OkHttpUtil.mHandler.post(new Runnable() {
                @Override
                public void run() {
                    onFailed(response.code(),"failed read response body");
                }
            });
        }finally {
            responseBody.close();
        }
        final String finalResBodyStr = resBodyStr;
        OkHttpUtil.mHandler.post(new Runnable() {
            @Override
            public void run() {
                onSuccess(response.code(), finalResBodyStr);
            }
        });
    }

    public abstract void onSuccess(int statusCode,String response);

    @Override
    public void onProgress(long progress, long total) {

    }
}
