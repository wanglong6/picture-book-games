package com.jzmedia.common.okhttp.callback;

import android.util.Log;

import com.jzmedia.common.okhttp.response.IResponseDownloadHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MyDownloadCallback implements Callback {
    private static final String TAG = "DownloadCallback";
    public static final int  SAVE_FILE_BYTE = 16*1024;//32kb
    private IResponseDownloadHandler mResponseDownloadHandler;
    private String mFilePath;
    private long mCompleteBytes;

    public MyDownloadCallback(IResponseDownloadHandler mResponseDownloadHandler, String mFilePath, long mCompleteBytes) {
        this.mResponseDownloadHandler = mResponseDownloadHandler;
        this.mFilePath = mFilePath;
        this.mCompleteBytes = mCompleteBytes;
    }

    @Override
    public void onFailure(Call call, final IOException e) {
        mResponseDownloadHandler.onFailed(e.toString());
    }

    @Override
    public void onResponse(Call call, final Response response) throws IOException {
        ResponseBody body = response.body();
        try {
            if(response.isSuccessful()){
                if(mResponseDownloadHandler!=null){
                    mResponseDownloadHandler.onStart(response.body().contentLength());
                }

                try {
                    if(response.header("Content-Range")==null || response.header("Content-Range").length() == 0){
                        mCompleteBytes = 0;
                    }
                    saveFile(response,mFilePath,mCompleteBytes);

                    if(mResponseDownloadHandler != null){
                        mResponseDownloadHandler.onFinish(mFilePath);
                    }

                } catch (IOException e) {
                    if(mResponseDownloadHandler!=null){
                        mResponseDownloadHandler.onCancel();
                    }
                }
            }else{
                if(mResponseDownloadHandler!=null){
                    mResponseDownloadHandler.onFailed("failed status "+response.code());
                }
            }
        } finally {
            if(body!=null){
                body.close();
            }
        }
    }

    /**
     * 保存文件
     * @param response
     * @param filePath
     * @param completeBytes
     */
    private void saveFile(Response response, String filePath, long completeBytes) throws IOException {
        InputStream is = null;
        byte[] buf = new byte[SAVE_FILE_BYTE];//每次读32kb
        int len;

        RandomAccessFile file = null;
        try {
            Log.d(TAG, "saveFile: filePath->" + filePath + ", completeBytes = " + completeBytes);
            //long startTime = System.currentTimeMillis();
            is = response.body().byteStream();
            file = new RandomAccessFile(filePath,"rw");
            long completeLen = 0;
            if(completeBytes > 0){
                file.seek(completeBytes);
                completeLen = completeBytes;
            }
            final long totalLen = response.body().contentLength() + completeLen;
            int rate = 0;
            int tmpRate;
            while ((len = is.read(buf)) != -1) {
                file.write(buf,0,len);
                completeLen += len;

                tmpRate = (int)(((float)completeLen/totalLen)*100);
                if (rate == tmpRate) {
                    continue;
                }
                rate = tmpRate;
                mResponseDownloadHandler.onProgress(rate);
            }
            //Log.d(TAG, "saveFile: Cost time: " + (System.currentTimeMillis() - startTime));
        } finally {
            try {
                if(is!=null){
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if(file!=null){
                    file.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
