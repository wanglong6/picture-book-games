package com.jzmedia.common.okhttp;

import android.os.Handler;
import android.os.Looper;

import com.jzmedia.common.okhttp.builder.DownloadBuilder;
import com.jzmedia.common.okhttp.builder.GetBuilder;
import com.jzmedia.common.okhttp.builder.PostBuilder;
import com.jzmedia.common.okhttp.builder.UploadBuilder;

import java.util.concurrent.TimeUnit;
import okhttp3.Call;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;

/**
 * Created by huangfen on 2017/12/20.
 */

public class OkHttpUtil {
    public static Handler mHandler = new Handler(Looper.getMainLooper());
    private static OkHttpClient mOkHttpClient;
    public static boolean isDebug = false;
    public static final String debugTag = "OkDroid";

    public OkHttpUtil(){
        this(null);
    }

    public OkHttpUtil(OkHttpClient okHttpClient){
        if(this.mOkHttpClient == null){
            synchronized (OkHttpUtil.class){
                if(this.mOkHttpClient == null){
                    if(okHttpClient == null){
                        this.mOkHttpClient = new OkHttpClient();
                    }else{
                        this.mOkHttpClient = okHttpClient;
                    }
                    mOkHttpClient.newBuilder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(5,TimeUnit.MINUTES)
                            .build();

                }
            }
        }
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    /**
     * 是否开启log,默认不开启
     * @param debug
     */
    public void setDebug(boolean debug) {
        isDebug = debug;
    }

    public GetBuilder get(){
        return new GetBuilder(this);
    }

    public PostBuilder post(){
        return new PostBuilder(this);
    }

    public UploadBuilder upload(){
        return new UploadBuilder(this);
    }

    public DownloadBuilder download(){
        return new DownloadBuilder(this);
    }

    /**
     * 根据tag取消请求
     * @param tag
     */
    public void cancel(Object tag){
        Dispatcher dispatcher = mOkHttpClient.dispatcher();
        for (Call call : dispatcher.queuedCalls()) {
            if(tag.equals(call.request().tag())){
                call.cancel();
            }
        }

        for (Call call : dispatcher.runningCalls()){
            if(tag.equals(call.request().tag())){
                call.cancel();
            }
        }
    }
}
