package com.jzmedia.common.okhttp.response;

import okhttp3.Response;


public interface IResponseHandler {

    void onSuccess(Response response);

    void onFailed(int statusCode, String errMsg);

    void onProgress(long progress, long total);
}
