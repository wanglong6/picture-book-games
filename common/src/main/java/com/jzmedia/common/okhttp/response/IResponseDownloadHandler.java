package com.jzmedia.common.okhttp.response;

public abstract class IResponseDownloadHandler {
    public abstract void onFinish(String path);
    public abstract void onProgress(int rate);
    public abstract void onFailed(String errMsg);

    public void onStart(long total){

    }
    public void onCancel(){

    }
}
